<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //

    protected $table='contacts';

    protected $fillable = array('name','email','subject','message');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}
