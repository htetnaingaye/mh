<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class Stock extends Model
{
    //

    // use SearchableTrait;

	
    protected $table='stocks';

    protected $fillable = array('name','link','created_by');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}
