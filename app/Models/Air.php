<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Air extends Model
{
    //

    protected $table='airs';

    protected $fillable = array('name','file_name','file_path','website','created_by','sort_order');


    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }
}
