<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class StockMarketPhoto extends Model
{
    //

    // use SearchableTrait;

	
    protected $table='stock_market_photos';

    protected $fillable = array('date','created_by','left_file_name','left_file_path','right_file_name','right_file_path');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}