<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //

    protected $table='clients';

    protected $fillable = array('name','url','description','file_path','file_name','sort_order','created_by','file_path','file_name','website','created_by');
     							
    							

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];

}
