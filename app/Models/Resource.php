<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Resource extends Model
{
    //

    use SearchableTrait;

    protected $table='resources';

    protected $fillable = array('type','category_id','sub_category','created_by');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'title'=>255
            
        ]
    ];

}
