<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class StockMarket extends Model
{
    //

    // use SearchableTrait;

	
    protected $table='stock_markets';

    protected $fillable = array('date','stock_id','eleven_unit','eleven_price','one_unit','one_price','different','status','created_by','left_file_name','left_file_path','right_file_name','right_file_path');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function stock(){
    
        return $this->hasOne('App\Models\Stock','id','stock_id');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}