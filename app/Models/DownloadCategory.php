<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class DownloadCategory extends Model
{
    //

    // use SearchableTrait;

	
    protected $table='download_categories';

    protected $fillable = array('name','type','created_by');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function downloadsub(){
    
        return $this->hasMany('App\Models\Resource','category_id','id');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}
