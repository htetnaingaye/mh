<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxProcedure extends Model
{
    //

    protected $table='tax_procedures';

    protected $fillable = array('title','description','status','created_by');						

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];

}
