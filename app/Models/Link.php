<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    //

    protected $table='links';

    protected $fillable = array('title','category_id','url','file_path','file_name','created_by');
     							
    							

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];

}
