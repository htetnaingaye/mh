<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Download extends Model
{
    //

    use SearchableTrait;

    protected $table='downloads';

    protected $fillable = array('title','category','cover_file_path','cover_file_name','description','type','file_path','file_name','created_by');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'title'=>255
            
        ]
    ];

}
