<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //

    protected $table='teams';

    protected $fillable = array('name','position','type','description','sort_order','created_by','file_path','file_name','created_by');
     							
    							

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];

}
