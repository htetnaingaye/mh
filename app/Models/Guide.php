<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    //

    protected $table='guides';

    protected $fillable = array('name','short_description','description','file_name','file_path','created_by','sort_order','status');


    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }
}
