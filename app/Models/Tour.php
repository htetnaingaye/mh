<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    //

    protected $table='tours';

    protected $fillable = array('name','destination_id','short_description','featured_status','description','sort_order','featured_sort_order','step_count','created_by');


    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }


    public function destination(){

        return $this->hasOne('App\Models\destination','id','destination_id');
    }

    public function images(){
    
        return $this->hasMany('App\Models\Images','parent_id','id')->where('parent_type',TOUR)->orderBy('sort_order');
    }


    public function active_images(){
    
        return $this->hasMany('App\Models\Images','parent_id','id')->where(array('parent_type'=>TOUR,'status'=>ACTIVE))->orderBy('sort_order');
    }

}
