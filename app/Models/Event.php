<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Event extends Model
{
    
	use SearchableTrait;

	
    protected $table='events';

    protected $fillable = array('title','mb_description','time','category_id','url','description','featured_status','featured_order_status','view_count','author_id','created_by','file_name','file_path','organizer_name','organizer_email','paid_status','fee','start_date','end_date','location'
     							
    							);

   

    public function category(){
    
        return $this->hasOne('App\Models\EventCategory','id','category_id');
    }

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function author(){
    
        return $this->hasOne('App\Models\Author','id','author_id');
    }


    protected $searchable=[

        'columns'=>[

            'title'=>255,
            'description'=>255
            
        ]
    ];

}
