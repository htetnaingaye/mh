<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fav extends Model
{
    //
    protected $table='favs';

    protected $fillable = array('image_id','user_id');


    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }
}
