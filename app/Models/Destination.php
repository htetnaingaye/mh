<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    //

    protected $table='destinations';

    protected $fillable = array('name','short_description','featured_status','description','sort_order','featured_sort_order','step_count','created_by');


    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    public function images(){
    
        return $this->hasMany('App\Models\Images','parent_id','id')->where('parent_type',DEST)->orderBy('sort_order');
    }

    public function active_images(){
    
        return $this->hasMany('App\Models\Images','parent_id','id')->where(array('parent_type'=>DEST,'status'=>ACTIVE))->orderBy('sort_order');
    }

}
