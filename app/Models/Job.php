<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Job extends Model
{
    
    use SearchableTrait;

    
    protected $table='jobs';

    protected $fillable = array('title','category_id','location','company','position', 'responsibility','requirements','apply','salary','created_by'
                                
                                );

   

    public function category(){
    
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function author(){
    
        return $this->hasOne('App\Models\Author','id','author_id');
    }


    protected $searchable=[

        'columns'=>[

            'title'=>255,
            'description'=>255
            
        ]
    ];

}

