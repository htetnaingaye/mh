<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class EventCategory extends Model
{
    //

    // use SearchableTrait;

	
    protected $table='event_categories';

    protected $fillable = array('name','created_by');
     							
    							

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    protected $searchable=[

        'columns'=>[

            'name'=>255
            
        ]
    ];
}
