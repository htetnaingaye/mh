<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Images extends Model
{
    //

    protected $table='images';

    protected $fillable=array('parent_id','name','title','sources','view_count','parent_type','file_path','file_name','sort_order','status','created_by');

    public function fav(){
    
        return $this->hasMany('App\Models\Fav','image_id','id')->where(array('favs.user_id'=>Auth::user()->id));
    }

    public function user(){

    	 return $this->hasOne('App\User','id','created_by');
    }


}
