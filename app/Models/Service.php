<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Service extends Model
{
    
	use SearchableTrait;

	
    protected $table='services';

    protected $fillable = array('title','mb_description','category_id', 'description','featured_status','featured_order_status','view_count','author_id','created_by','file_name','file_path'
     							
    							);

   

    public function category(){
    
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function author(){
    
        return $this->hasOne('App\Models\Author','id','author_id');
    }


    protected $searchable=[

        'columns'=>[

            'title'=>255,
            'description'=>255
            
        ]
    ];

}
