<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class School extends Model
{
    
	use SearchableTrait;

	
    protected $table='schools';

    protected $fillable = array('name','description','start_date', 'duration','price','file_path','file_name','created_by','price','status','course','township','map_place','lat','lng'
     							
    							);

   

    public function category(){
    
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function user(){
    
        return $this->hasOne('App\User','id','created_by');
    }

    public function author(){
    
        return $this->hasOne('App\Models\Author','id','author_id');
    }


    protected $searchable=[

        'columns'=>[

            'name'=>255,
            
        ]
    ];

}
