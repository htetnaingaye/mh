<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Link;
use App\User;
use App\Models\Category;
use App\Models\Demand;
use App\Models\Supply;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;
use Cloudder;
use Storage;

class LinkController extends Controller
{
    //

    public function search(Request $request){

        $active='link';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Link::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.link.index',compact('list','data','search','keyword','active'));
    }

    

    public function index(){

        $active='link';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Link::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

    	return view('backend.link.index',compact('list','data','active'));
    }

    public function basic($id=0){

        $active='author';

        $info=Link::find($id); 

	    return view('backend.link.basic',compact('info','id','active'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;


            $inputs['status']=INACTIVE;

            if ($request->hasFile('photo')) {

                $upload_path =ARTICLE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();
                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('photo')->move($upload_path, $fileName);

                /* Creating Normal */

                $img = Image::make($upload_path.$fileName);

                $img->fit(NORMAL_WIDTH,NORMAL_HIGHT)->save($upload_path.NORMAL_PREFIX.$fileName);

                $img->fit(THUMB_WIDTH,THUMB_HIGHT)->save($upload_path.THUMB_PREFIX.$fileName);

                

                $inputs['file_name']=$fileName;
                $inputs['file_path']=$upload_path;
                
            }


            $info=Link::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/link');     
            } 

        }else{

            $info=Link::find($id);

            $info->title=$request->title;
            $info->url=$request->url;
            $info->category_id=$request->category_id;

            if ($request->hasFile('photo')) {

                
                if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                    File::delete($info->file_path.$info->file_name);

                    if(File::exists($info->file_path.NORMAL_PREFIX.$info->file_name)){

                        File::delete($info->file_path.NORMAL_PREFIX.$info->file_name);

                    }

                    if(File::exists($info->file_path.THUMB_PREFIX.$info->file_name)){

                        File::delete($info->file_path.THUMB_PREFIX.$info->file_name);

                    }
            
                }  
                
                $upload_path =ARTICLE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();

                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('photo')->move($upload_path, $fileName);

                 /* Creating Normal */

                $img = Image::make($upload_path.$fileName);

                $img->fit(NORMAL_WIDTH,NORMAL_HIGHT)->save($upload_path.NORMAL_PREFIX.$fileName);

                $img->fit(THUMB_WIDTH,THUMB_HIGHT)->save($upload_path.THUMB_PREFIX.$fileName);


                $info->file_name=$fileName;
                $info->file_path=$upload_path;

                // optimize 
            }

            
            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/link');      

        }

    }

    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Link::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Link::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Link::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredArticles(){

        $active='featured-article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Link::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.link.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =Link::find($row);

            $info->featured_sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

    public function getMember(){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->paginate(DEFAULT_PAGE_COUNT);


        return view('backend.link.member',compact('list','data','active'));
    }

    public function memberSearch(Request $request){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.link.member',compact('list','data','search','keyword','active'));
    }

    public function getDemand(){

       
        $active='demand';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Demand::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.link.demand',compact('list','data','active','author'));
    }

    public function getSupply(){

       
        $active='supply';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Supply::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.link.supply',compact('list','data','active','author'));
    }

    public function toggleSupplyPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Supply::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroySupply(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Supply::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleDemandPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Demand::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroyDemand(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Demand::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


}
