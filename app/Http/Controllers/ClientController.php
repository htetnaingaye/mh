<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Client;
use Flash;
use Input;
use Response;
use Auth;
use File;

class ClientController extends Controller
{
    //

    public function search(Request $request){

        
        $parent='client';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Client::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.client.index',compact('list','data','search','keyword','parent'));
    }

    public function index(){

        
        $parent='client';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Client::orderBy('created_at')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.client.index',compact('list','data','parent'));
    }

    public function basic($id=0){

        $active='category';
        $parent='article';

        $info=Client::find($id); 
        
	    return view('backend.client.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;


            if ($request->hasFile('photo')) {

                $upload_path =DOWNLOAD_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();
                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('photo')->move($upload_path, $fileName);


                $inputs['file_name']=$fileName;
                $inputs['file_path']=$upload_path;
                
            }



            $info=Client::create($inputs);


            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/client');     
            } 

        }else{

            $info=Client::find($id);

            $info->name=$request->name;
            $info->website=$request->website;


            if ($request->hasFile('photo')) {

                
                if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                    File::delete($info->file_path.$info->file_name);

                    if(File::exists($info->file_path.$info->file_name)){

                        File::delete($info->file_path.$info->file_name);

                    }
            
                }  
                
                $upload_path =DOWNLOAD_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();

                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('photo')->move($upload_path, $fileName);


                $info->file_name=$fileName;
                $info->file_path=$upload_path;

                // optimize 
            }

            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/client');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Client::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
