<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Team;
use App\User;
use App\Models\Category;
use App\Models\Demand;
use App\Models\Supply;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;
use Cloudder;
use Storage;

class TeamController extends Controller
{
    //

    public function search(Request $request){

        $active='team';
        $parent='about_us';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Team::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.team.index',compact('list','data','search','keyword','active','parent'));
    }


    public function index(){

        $active='team';
        $parent='about_us';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Team::orderBy('created_at','DESC')->orderBy('sort_order','ASC')->paginate(DEFAULT_PAGE_COUNT);
        
    	$category=Category::pluck('name','id');
    	
    	return view('backend.team.featured_list',compact('list','data','active','category','parent'));
    }

    public function basic($id=0){

        $active='team';
        $parent='about_us';

        $info=Team::find($id); 

        $category=Category::pluck('name','id');

	    return view('backend.team.basic',compact('info','id','active','category','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;
            
            if ($request->hasFile('photo')) {

                $upload_path =SERVICE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();
                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('photo')->move($upload_path, $fileName);

                /* Creating Normal */

                $img = Image::make($upload_path.$fileName);

                $img->fit(SERVICE_NORMAL_WIDTH,SERVICE_NORMAL_HIGHT)->save($upload_path.NORMAL_PREFIX.$fileName);

                $img->fit(THUMB_WIDTH,THUMB_HIGHT)->save($upload_path.THUMB_PREFIX.$fileName);

                $inputs['file_name']=$fileName;
                $inputs['file_path']=$upload_path;
                
            }

            $inputs['status']=INACTIVE;

            

            $info=Team::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/team');     
            } 

        }else{

            $info=Team::find($id);

            $info->name=$request->name;
            $info->position=$request->position;
            $info->type=$request->type;
            $info->description=$request->description;


            if ($request->hasFile('photo')) {

                
                if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                    File::delete($info->file_path.$info->file_name);

                    if(File::exists($info->file_path.NORMAL_PREFIX.$info->file_name)){

                        File::delete($info->file_path.NORMAL_PREFIX.$info->file_name);

                    }

                    if(File::exists($info->file_path.THUMB_PREFIX.$info->file_name)){

                        File::delete($info->file_path.THUMB_PREFIX.$info->file_name);

                    }
            
                }  
                
                $upload_path =SERVICE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();

                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('photo')->move($upload_path, $fileName);

                 /* Creating Normal */

                $img = Image::make($upload_path.$fileName);

                $img->fit(SERVICE_NORMAL_WIDTH,SERVICE_NORMAL_HIGHT)->save($upload_path.NORMAL_PREFIX.$fileName);

                $img->fit(THUMB_WIDTH,THUMB_HIGHT)->save($upload_path.THUMB_PREFIX.$fileName);


                $info->file_name=$fileName;
                $info->file_path=$upload_path;

                // optimize 
            }
            
            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/team');      

        }

    }

    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Team::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Team::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Team::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredServices(){

        $active='featured-article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Team::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.team.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =Team::find($row);

            $info->sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

    

}
