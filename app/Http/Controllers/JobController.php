<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Job;
use App\Models\JobCategory;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;

use Storage;

class JobController extends Controller
{
    //

    public function search(Request $request){

        $active='job';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Job::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.job.index',compact('list','data','search','keyword','active'));
    }

    public function index(){

       
        $active='job';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Job::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
    	
    	
    	return view('backend.job.index',compact('list','data','active'));
    }

    public function basic($id=0){

        $active='job';

        $info=Job::find($id); 
        $category=JobCategory::pluck('name','id');

	    return view('backend.job.basic',compact('info','id','active','category'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        

        if($id==0){

            $inputs['created_by']=Auth::user()->id;
            
         
            $info=Job::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/job');     
            } 

        }else{

            $info=Job::find($id);

            $info->title=$request->title;
            $info->category_id=$request->category_id;
            $info->responsibility=$request->responsibility;
            $info->requirements=$request->requirements;
            $info->apply=$request->apply;
            $info->company=$request->company;
            $info->location=$request->location;
            $info->salary=$request->salary;

            
            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/job');      

        }

    }

    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Job::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Job::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Job::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredJobs(){

        $active='featured-job';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Job::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.job.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =Job::find($row);

            $info->featured_sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

}
