<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Contact;

class SettingController extends Controller
{
    //


	public function getContactMessage(){

		$active='contact';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Contact::paginate(DEFAULT_PAGE_COUNT);


        return view('backend.setting.contact',compact('list','data','search','keyword','active'));

	}
}
