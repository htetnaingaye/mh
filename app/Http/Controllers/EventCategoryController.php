<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\EventCategory;
use Flash;
use Input;
use Response;
use Auth;
use File;

class EventCategoryController extends Controller
{
    //

    public function search(Request $request){

        $active='event_category';
        $parent='event';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=EventCategory::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.event_category.index',compact('list','data','search','keyword','active','parent'));
    }

    public function index(){

        $active='event_category';
        $parent='event';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=EventCategory::orderBy('created_at')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.event_category.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='event_category';
        $parent='event';

        $info=EventCategory::find($id); 
        
	    return view('backend.event_category.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            $info=EventCategory::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/event_category');     
            } 

        }else{

            $info=EventCategory::find($id);

            $info->name=$request->name;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/event_category');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=EventCategory::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
