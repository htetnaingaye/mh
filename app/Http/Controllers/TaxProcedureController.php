<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\TaxProcedure;
use App\User;
use App\Models\Category;
use App\Models\Demand;
use App\Models\Supply;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;
use Cloudder;
use Storage;

class TaxProcedureController extends Controller
{
    //

    public function search(Request $request){

        $active='procedure';
        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=TaxProcedure::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.taxprocedure.index',compact('list','data','search','keyword','active','parent'));
    }

    

    public function index(){

        $active='procedure';
        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=TaxProcedure::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

    	return view('backend.taxprocedure.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='procedure';
        $parent='stock';

        $info=TaxProcedure::find($id); 

	    return view('backend.taxprocedure.basic',compact('info','id','active','stock'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;


            $inputs['status']=INACTIVE;
            $info=TaxProcedure::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/procedure');     
            } 

        }else{

            $info=TaxProcedure::find($id);

            $info->title=$request->title;
            $info->description=$request->description;
            

            
            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/procedure');      

        }

    }

    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=TaxProcedure::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=TaxProcedure::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=TaxProcedure::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredArticles(){

        $active='featured-article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=TaxProcedure::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.taxprocedure.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =TaxProcedure::find($row);

            $info->featured_sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }
    
}
