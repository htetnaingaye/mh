<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\DownloadCategory;
use Flash;
use Input;
use Response;
use Auth;
use File;

class DownloadCategoryController extends Controller
{
    //

    public function search(Request $request){

        $active='download_category';
        $parent='download';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=DownloadCategory::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.download_category.index',compact('list','data','search','keyword','active','parent'));
    }

    public function index(){

        $active='download_category';
        $parent='download';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=DownloadCategory::orderBy('created_at')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.download_category.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='download_category';
        $parent='download';

        $info=DownloadCategory::find($id); 
        
	    return view('backend.download_category.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            $info=DownloadCategory::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/download_category');     
            } 

        }else{

            $info=DownloadCategory::find($id);

            $info->name=$request->name;
            $info->type=$request->type;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/download_category');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=DownloadCategory::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
