<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Destination;
use App\Models\Hotel;
use App\Models\Air;
use App\Models\Images;
use Image;
use Input;
use Auth;
use Flash;
use DB;
use Response;
use File;


class AirController extends Controller
{
    //

    public function getIndex(){

        $active='air';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');


        $list=Air::orderBy('sort_order')->paginate(DEFAULT_PAGE_COUNT);

        return view('backend.air.index',compact('list','data','active'));

    }

    public function getBasic($id=0){

        $active='air';

        $info=Air::find($id);

        $data['csstoload']=array('bootstrap-fileinput');
        $data['jstoload']=array('bootstrap-fileinput');

        return view('backend.air.basic',compact('id','info','active','data'));

    }

    public function postBasic(Request $request,$id=0){

        $info=$request->id==0?new Air:Air::find($id);

        

        $info->name=$request->name;
        $info->website=$request->website;

        if ($request->hasFile('photo')) {


                if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                    File::delete($info->file_path.$info->file_name);
            
                } 
                $upload_path =AIR_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();

                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Ymd-His')."-".$imageName;

                Input::file('photo')->move($upload_path, $fileName);

                $info->file_name=$fileName;
                $info->file_path=$upload_path;
                
            }

        $info->created_by=1;

        $info->save();

        $id=$info->id;


        Flash::success('Successfully saved.');
        return redirect(ADMIN_PREFIX.'/air');

    }

    

    

    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            /* Checking Used transaction */
          
            $info=Air::find($id);
            /* need to delete related photos and files */

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);

            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
            
        }
    }

    

     public function sortPhoto($id=0,Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info = Images::find($row);

            $info->sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =Air::find($row);

            $info->sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }
     
}
