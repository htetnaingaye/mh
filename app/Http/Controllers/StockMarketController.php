<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\StockMarket;
use App\Models\StockMarketPhoto;
use App\User;
use App\Models\Stock;
use App\Models\Demand;
use App\Models\Supply;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;
use Cloudder;
use Storage;

class StockMarketController extends Controller
{
    //

    public function search(Request $request){

        $active='stock_market';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=StockMarket::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.stock_market.index',compact('list','data','search','keyword','active'));
    }

    

    public function index(){

        $active='stock_market';
        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=StockMarket::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

    	return view('backend.stock_market.index',compact('list','data','active','parent'));
    }

    public function getPhotoIndex(){

        $active='stock_photo';
        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=StockMarketPhoto::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

        return view('backend.stock_market.photo_index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='stock_market';

        $info=StockMarket::find($id); 

        $stock=Stock::pluck('name','id');

        $parent='stock';

	    return view('backend.stock_market.basic',compact('info','id','active','stock','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;


            $inputs['status']=INACTIVE;

            $info=StockMarket::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/stock_market');     
            } 

        }else{

            $info=StockMarket::find($id);

            $info->stock_id=$request->stock_id;
            $info->eleven_price=$request->eleven_price;
            $info->eleven_unit=$request->eleven_unit;
            $info->one_price=$request->one_price;
            $info->one_unit=$request->one_unit;
            $info->different=$request->different;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/stock_market');      

        }

    }

    public function getStockPhotoList(){

        $active='stock_photo';

        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=StockMarketPhoto::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

        $stock=Stock::pluck('name','id');

        $parent='stock';

        return view('backend.stock_market.photo_list',compact('list','active','stock','parent','data'));

    }

    public function getStockPhoto($id=0){

        $active='stock_photo';

        $parent='stock';

        $info=StockMarketPhoto::find($id); 

        $stock=Stock::pluck('name','id');

        $parent='stock';

        return view('backend.stock_market.photo',compact('info','id','active','stock','parent'));

    }

    public function postStockPhoto(Request $request,$id=0){

        $active='stock_photo';

        $parent='stock';

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            if ($request->hasFile('right_photo')) {

                    $upload_path =ARTICLE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                    if (!is_dir($upload_path)) {

                        mkdir($upload_path, 0755, true);
                        file_put_contents($upload_path . "/index.html", "");
                    }

                    $ext = $request->file('right_photo')->getClientOriginalExtension();
                    $imageName = $request->file('right_photo')->getClientOriginalName();

                    $fileName = Date('Y-md-His')."-".$imageName;

                    $fileName = preg_replace('/\s+/', '-', $fileName);


                    Input::file('right_photo')->move($upload_path, $fileName);

                    /* Creating Normal */



                    $inputs['right_file_name']=$fileName;
                    $inputs['right_file_path']=$upload_path;
                    
                }

                if ($request->hasFile('left_photo')) {

                    $upload_path =ARTICLE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                    if (!is_dir($upload_path)) {

                        mkdir($upload_path, 0755, true);
                        file_put_contents($upload_path . "/index.html", "");
                    }

                    $ext = $request->file('left_photo')->getClientOriginalExtension();
                    $imageName = $request->file('left_photo')->getClientOriginalName();

                    $fileName = Date('Y-md-His')."-".$imageName;

                    $fileName = preg_replace('/\s+/', '-', $fileName);


                    Input::file('left_photo')->move($upload_path, $fileName);

                    /* Creating Normal */



                    $inputs['left_file_name']=$fileName;
                    $inputs['left_file_path']=$upload_path;
                    
                }



                $info=StockMarketPhoto::create($inputs);

                Flash::success("Successfully saved.");
                    
                return redirect(ADMIN_PREFIX.'/stock_photo'); 

            }else{

                 $info=StockMarketPhoto::find($id);


                if ($request->hasFile('right_photo')) {

                    
                    if($info->file_name!='' && File::exists($info->right_file_path.$info->right_file_name)){

                        File::delete($info->right_file_path.$info->right_file_name);
                
                    }  
                    
                    $upload_path =ARTICLE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                    if (!is_dir($upload_path)) {

                        mkdir($upload_path, 0755, true);
                        file_put_contents($upload_path . "/index.html", "");
                    }

                    $ext = $request->file('right_photo')->getClientOriginalExtension();

                    $imageName = $request->file('right_photo')->getClientOriginalName();

                    $fileName = Date('Y-md-His')."-".$imageName;

                    $fileName = preg_replace('/\s+/', '-', $fileName);

                    Input::file('right_photo')->move($upload_path, $fileName);

                    /* Creating Normal */


                    $info->right_file_name=$fileName;
                    $info->right_file_path=$upload_path;

                    // optimize 
                }

                if ($request->hasFile('left_photo')) {

                    if($info->file_name!='' && File::exists($info->left_file_path.$info->left_file_name)){

                        File::delete($info->left_file_path.$info->left_file_name);
                
                    }  
                    
                    $upload_path =ARTICLE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                    if (!is_dir($upload_path)) {

                        mkdir($upload_path, 0755, true);
                        file_put_contents($upload_path . "/index.html", "");
                    }

                    $ext = $request->file('right_photo')->getClientOriginalExtension();

                    $imageName = $request->file('right_photo')->getClientOriginalName();


                    $info->left_file_name=$fileName;
                    $info->left_photo_file_path=$upload_path;

                    $fileName = Date('Y-md-His')."-".$imageName;


                    $fileName = preg_replace('/\s+/', '-', $fileName);

                    Input::file('right_photo')->move($upload_path, $fileName);

                    /* Creating Normal */


                    $info->right_file_name=$fileName;
                    $info->right_file_path=$upload_path;

                    // optimize 
                }
                
                $info->save();

                Flash::success("Successfully updated.");

                return redirect(ADMIN_PREFIX.'/stock_photo');
            }

        

    }


    public function getPhoto($id=0){

        $active='stock_market';

        $info=StockMarket::find($id); 

        $stock=Stock::pluck('name','id');

        return view('backend.stock_market.photo',compact('info','id','active','stock'));

    }

    public function postPhoto(Request $request,$id=0){

        $inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;


            $inputs['status']=INACTIVE;

             if ($request->hasFile('right_photo')) {

                $upload_path =ARTICLE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('right_photo')->getClientOriginalExtension();
                $imageName = $request->file('right_photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('right_photo')->move($upload_path, $fileName);

                /* Creating Normal */



                $inputs['right_file_name']=$fileName;
                $inputs['right_file_path']=$upload_path;
                
            }

            if ($request->hasFile('left_photo')) {

                $upload_path =ARTICLE_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('left_photo')->getClientOriginalExtension();
                $imageName = $request->file('left_photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('left_photo')->move($upload_path, $fileName);

                /* Creating Normal */



                $inputs['left_file_name']=$fileName;
                $inputs['left_file_path']=$upload_path;
                
            }



            $info=StockMarketPhoto::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/stock_market');     
            } 

        }else{

            

            $info=StockMarketPhoto::find($id);


            $info->created_by=Auth::user()->id;

            if ($request->hasFile('right_photo')) {

                
                if($info->file_name!='' && File::exists($info->right_file_path.$info->right_file_name)){

                    File::delete($info->right_file_path.$info->right_file_name);
            
                }  
                
                $upload_path =ARTICLE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('right_photo')->getClientOriginalExtension();

                $imageName = $request->file('right_photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('right_photo')->move($upload_path, $fileName);

                /* Creating Normal */


                $info->right_file_name=$fileName;
                $info->right_file_path=$upload_path;

                // optimize 
            }

            if ($request->hasFile('left_photo')) {

                if($info->file_name!='' && File::exists($info->left_file_path.$info->left_file_name)){

                    File::delete($info->left_file_path.$info->left_file_name);
            
                }  
                
                $upload_path =ARTICLE_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('right_photo')->getClientOriginalExtension();

                $imageName = $request->file('right_photo')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('right_photo')->move($upload_path, $fileName);

                /* Creating Normal */


                $info->right_file_name=$fileName;
                $info->right_file_path=$upload_path;

                // optimize 
            }
            
            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/stock_market');      

        }

    }


    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=StockMarket::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function togglePhotoPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=StockMarketPhoto::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }



    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=StockMarket::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=StockMarket::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroyPhoto(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=StockMarketPhoto::find($id);

            if($info->right_file_name!='' && File::exists($info->right_file_path.$info->right_file_name)){

                File::delete($info->right_file_path.$info->right_file_name);
            
            } 

            if($info->left_file_name!='' && File::exists($info->left_file_path.$info->left_file_name)){

                File::delete($info->left_file_path.$info->left_file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredArticles(){

        $active='featured-article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=StockMarket::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.stock_market.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =StockMarket::find($row);

            $info->featured_sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

    public function getMember(){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->paginate(DEFAULT_PAGE_COUNT);


        return view('backend.stock_market.member',compact('list','data','active'));
    }

    public function memberSearch(Request $request){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.stock_market.member',compact('list','data','search','keyword','active'));
    }

    public function getDemand(){

       
        $active='demand';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Demand::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.stock_market.demand',compact('list','data','active','author'));
    }

    public function getSupply(){

       
        $active='supply';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Supply::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.stock_market.supply',compact('list','data','active','author'));
    }

    public function toggleSupplyPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Supply::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroySupply(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Supply::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleDemandPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Demand::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroyDemand(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Demand::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


}
