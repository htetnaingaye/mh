<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Stock;
use Flash;
use Input;
use Response;
use Auth;
use File;

class StockController extends Controller
{
    //

    public function search(Request $request){

        $active='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Stock::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.stock.index',compact('list','data','search','keyword','active'));
    }

    public function index(){

        $active='stock';
        $parent='stock';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Stock::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.stock.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='stock';
        $parent='stock';

        $info=Stock::find($id); 
        
	    return view('backend.stock.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            $info=Stock::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/stock');     
            } 

        }else{

            $info=Stock::find($id);

            $info->name=$request->name;
            $info->link=$request->link;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/stock');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Stock::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
