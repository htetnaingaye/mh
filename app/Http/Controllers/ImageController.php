<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Models\Images;
use File;
use Response;
use Auth;
use Flash;
use Input;
use Image;


class ImageController extends Controller
{
    //

    public function index(){


    	return view('backend.dashboard');
    }

    public function getIndex(){

        $active='image';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');



    	$list=Images::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

    	return view('backend.image.index',compact('list','active','data'));
    }

    public function getBasic($id=0){

        $info=Images::find($id);

        return view('backend.image.basic',compact('info','id'));
    }

    public function postBasic(Request $request,$id=0){



        $info=$id==0?new Images:Images::find($id);

        $info->title=$request->title;
        $info->source=$request->source;
        $info->name=$request->name;
        $info->view_count=$request->view_count;
        $info->created_by=Auth::user()->id;

        if ($request->hasFile('photo')) {

                $upload_path =PHOTO_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }


                $ext = $request->file('photo')->getClientOriginalExtension();
                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('Ymd-H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('photo')->move($upload_path, $fileName);

                /* Creating Normal */

                $img = Image::make($upload_path.$fileName);

                $img->save($upload_path.$fileName,30);

                

                $info->file_name=$fileName;
                $info->file_path=$upload_path;

                
            }

            $info->save();

            Flash::success('Successfully saved');
            return redirect(ADMIN_PREFIX.'/image');
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            /* Checking Used transaction */
            $info=Images::find($id);

            
            if($info!=null){

                if(File::exists($info->file_path.$info->file_name)){

                     File::delete($info->file_path.$info->file_name);
                }
            }
         

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
            
        }
    }

   

    
}
