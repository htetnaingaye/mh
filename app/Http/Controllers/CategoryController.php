<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Category;
use Flash;
use Input;
use Response;
use Auth;
use File;

class CategoryController extends Controller
{
    //

    public function search(Request $request){

        $active='category';
        $parent='article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Category::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.category.index',compact('list','data','search','keyword','active','parent'));
    }

    public function index(){

        $active='category';
        $parent='article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=Category::orderBy('created_at')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.category.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

        $active='category';
        $parent='article';

        $info=Category::find($id); 
        
	    return view('backend.category.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            $info=Category::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/category');     
            } 

        }else{

            $info=Category::find($id);

            $info->name=$request->name;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/category');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Category::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
