<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class AdminController extends Controller
{
    //

    public function index(){

    	return view('backend.dashboard');
    }

    public function getUser(){

    	$list=User::paginate(DEFAULT_PAGE_COUNT);

    	return view('backend.user.index',compact('list'));
    }

    public function getUserBasic(){


    	return view('backend.user.index',compact('list'));

    }

    
}
