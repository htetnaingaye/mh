<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Download;
use App\User;
use App\Models\Category;
use App\Models\Resource;
use App\Models\DownloadCategory;
use App\Models\Supply;
use Flash;
use Input;
use Response;
use Auth;
use File;
use Image;
use Cloudder;

use Storage;

class ResourcesController extends Controller
{
    //

    public function search(Request $request){

        $active='download';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        //$list=Download::search($request->keyword,0)->get();
        $list=Download::get();

        $keyword=$request->keyword;

        $search=true;

        return view('backend.download.index',compact('list','data','search','keyword','active'));
    }

    public function getFree($id=0){

        $active='resources';
        $parent='download';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Resource::where('type',10)->orderBy('category_id','DESC')->get();

        $info=Resource::find($id);

        $category_list=DownloadCategory::pluck('name','id');

    	
    	return view('backend.resources.free',compact('list','category_list','data','active','info','category','parent'));
    }

    public function postFree(Request $request){

        $inputs=\Input::except("_token");

        $info=$request->id==0?new Resource:Resource::find($request->id);
        $info->type=10;
        $info->category_id=$request->category_id;
        $info->sub_category=$request->sub_category;
        $info->created_by=Auth::user()->id;

        $info->save();

        if($request->id==0){
            
            Flash::success("Successfully saved.");

        }else{

            Flash::success("Successfully Updated.");
        }

        return redirect(ADMIN_PREFIX."/download_subcategory");

    }

    public function basic($id=0){

        $active='download';

        $info=Download::find($id); 

	    return view('backend.download.basic',compact('info','id','active','category'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;
            


            if ($request->hasFile('cover_photo')) {

                $upload_path =DOWNLOAD_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('cover_photo')->getClientOriginalExtension();
                $imageName = $request->file('cover_photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('cover_photo')->move($upload_path, $fileName);


                $inputs['cover_file_name']=$fileName;
                $inputs['cover_file_path']=$upload_path;
                
            }

            
            if ($request->hasFile('photo')) {

                $upload_path =DOWNLOAD_PATH. "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();
                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);


                Input::file('photo')->move($upload_path, $fileName);


                $inputs['file_name']=$fileName;
                $inputs['file_path']=$upload_path;
                
            }

            $inputs['status']=INACTIVE;

            

            $info=Download::create($inputs);



            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect('admin/download_subcategory');     
            } 

        }else{

            $info=Download::find($id);

            $info->title=$request->title;
            $info->description=$request->description;
            $info->type=$request->type!=null?$request->type:1;
            $info->category=$request->category;

            if ($request->hasFile('photo')) {

                
                if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                    File::delete($info->file_path.$info->file_name);

                    if(File::exists($info->file_path.$info->file_name)){

                        File::delete($info->file_path.$info->file_name);

                    }
            
                }  
                
                $upload_path =DOWNLOAD_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('photo')->getClientOriginalExtension();

                $imageName = $request->file('photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('photo')->move($upload_path, $fileName);


                $info->file_name=$fileName;
                $info->file_path=$upload_path;

                // optimize 
            }
            

            if ($request->hasFile('cover_photo')) {

                
                if($info->file_name!='' && File::exists($info->cover_file_path.$info->cover_file_name)){

                    File::delete($info->cover_file_path.$info->cover_file_name);

                    if(File::exists($info->cover_file_path.$info->cover_file_name)){

                        File::delete($info->cover_file_path.$info->cover_file_name);

                    }
            
                }  
                
                $upload_path =DOWNLOAD_PATH . "/" . date("Y") . "/" . date("m") . "/";

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('cover_photo')->getClientOriginalExtension();

                $imageName = $request->file('cover_photo')->getClientOriginalName();

                $fileName = Date('H-i-s')."-".$imageName;

                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('cover_photo')->move($upload_path, $fileName);


                $info->cover_file_name=$fileName;
                $info->cover_file_path=$upload_path;

                // optimize 
            }


            $info->save();

            Flash::success("Successfully updated.");

            return redirect('admin/download');      

        }

    }

    public function togglePublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Download::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleFeatured(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Download::find($id);
            $info->featured_status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Set.":"Successfully Unset.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Download::find($id);

            if($info->file_name!='' && File::exists($info->file_path.$info->file_name)){

                File::delete($info->file_path.$info->file_name);
            
            } 

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function featuredArticles(){

        $active='featured-article';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Download::where('featured_status',ACTIVE)->orderBy('featured_sort_order')->paginate(DEFAULT_PAGE_COUNT);

        
        return view('backend.download.featured_list',compact('list','data','active'));
    }

    public function sortFeatured(Request $request)
    {

        $sortOrder = $request->get('sortOrder');
        $index = 1;

        foreach ($sortOrder as $row) {

            $info =Download::find($row);

            $info->featured_sort_order = $index;
            $info->save();
            $index++;
        }

        return Response::json(array('status' => true));
    }

    public function getMember(){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->paginate(DEFAULT_PAGE_COUNT);


        return view('backend.download.member',compact('list','data','active'));
    }

    public function memberSearch(Request $request){

        $active='member';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=User::where('type',MEMBER)->search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.download.member',compact('list','data','search','keyword','active'));
    }

    public function getDemand(){

       
        $active='demand';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Demand::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.download.demand',compact('list','data','active','author'));
    }

    public function getSupply(){

       
        $active='supply';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=Supply::orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        return view('backend.download.supply',compact('list','data','active','author'));
    }

    public function toggleSupplyPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Supply::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroySupply(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Supply::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function toggleDemandPublish(Request $request,$id=0,$status=0){

    if($request->ajax() && $id > 0){

            $info=Demand::find($id);
            $info->status=$status;
            $info->save();
            $message=$status==ACTIVE? "Successfully Published.":"Successfully Unpublished.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

    public function destroyDemand(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=Demand::find($id);

            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }


}
