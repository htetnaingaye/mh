<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Images;
use App\Models\Article;
use App\Models\Event;
use App\Models\Category;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Link;
use App\Models\Service;
use App\Models\School;
use App\Models\Contact;
use App\Models\Download;
use App\Models\Resource;
use App\Models\Job;
use App\Models\Team;
use App\Models\TaxProcedure;
use App\Models\Client;
use App\Models\EventCategory;
use App\Models\DownloadCategory;
use App\Models\StockMarket;
use App\Models\StockMarketPhoto;
use App\Models\Stock;
use App\User;
use Image;
use Flash;
use Input;
use Response;
use Auth;
use Validator;
use Hash;
use GuzzleHttp;
use Mail;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function getIndex(){

        return view('frontend.index');
    }

    public function __construct()
    {
        
        $this->event=EventCategory::pluck('name','id');

        $this->service=Service::where('status',ACTIVE)->get();
        
    }

    public function autocomplete(){


            $keyword = Input::get('term');
            
            $results = array();

            
            $queries=DB::select( DB::raw('select * from ( select distinct township   from schools where township!="" union select distinct name from schools ) s where township like "%'.$keyword.'%"') );

            
            $queries=json_encode($queries);
            $queries=json_decode($queries);





            
            foreach ($queries as $key=>$value)
            {

                


                $results[] = [ 'id' => $value->township, 'value' =>$value->township];
            }

            return Response::json($results);
    }


    public function getLanding(){

        return  view('frontend.landing');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function getHome(){

        $latest=Article::where('status',ACTIVE)->orderBy('created_at','DESC')->take(4)->get();
        $event=$this->event;

        $service_list=Service::where('status',ACTIVE)->where('featured_status',ACTIVE)->orderBy('featured_sort_order','ASC')->take(4)->get();

        $active='home';

        $s_list=$this->service;

        $client=Client::get();

        return view('frontend.home',compact('latest','event','service_list','s_list','active','client'));
    }

     public function getArticle(){

        $event=$this->event;

        $s_list=$this->service;

        $list=Article::where('status',ACTIVE)->orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        $category=Category::pluck('name','id');

        $popular=Article::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='article';

        return view('frontend.article',compact('list','category','popular','event','active','s_list'));
    }

    public function getArticleDetail($id=0){

        $active='article';

        $event=$this->event;

        $s_list=$this->service;


        $info=Article::find($id);

        if($info==null){

            abort(404);
        }


        $category=Category::pluck('name','id');

        $popular=Article::where('status',ACTIVE)
            ->where('category_id',$info->category_id)
            ->where('id','!=',$info->id)->orderBy('view_count','DESC')
            ->take(5)->paginate(DEFAULT_PAGE_COUNT);


        $active='article';

        $og=array();

        $og['title']=$info->title;
        $og['description']=trim(strip_tags(str_limit($info->description,150,'...')));
        $og['url']=url('detail-detail/'.$id."/");
        $og['image']=asset($info->file_path.$info->file_name);

        return view('frontend.article-detail',compact('info','active','category','popular','event','active','s_list','og'));



    }


    public function getFbArticleShare($id=0){

        $active='article';

        $event=$this->event;

        $s_list=$this->service;


        $info=Article::find($id);

        if($info==null){

            abort(404);
        }


        $category=Category::pluck('name','id');

        $popular=Article::where('status',ACTIVE)
            ->where('category_id',$info->category_id)
            ->where('id','!=',$info->id)->orderBy('view_count','DESC')
            ->take(5)->paginate(DEFAULT_PAGE_COUNT);


        $active='article';

        $og=array();

        $og['title']=$info->title;
        $og['description']=trim(strip_tags(str_limit($info->description,150,'...')));
        $og['url']=url('detail-detail/'.$id."/");
        $og['image']=asset($info->file_path.$info->file_name);

        return view('frontend.article-detail',compact('info','active','category','popular','event','active','s_list','og'));


    }

    public function getNew(){

        $event=$this->event;

        $s_list=$this->service;

        $list=News::where('status',ACTIVE)->orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);
        
        $category=NewsCategory::pluck('name','id');

        $popular=News::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='article';

        return view('frontend.news',compact('list','category','popular','event','active','s_list'));
    }

    public function getNewDetail($id=0){

        $active='article';

        $event=$this->event;

        $s_list=$this->service;


        $info=News::find($id);

        if($info==null){

            abort(404);
        }


        $category=NewsCategory::pluck('name','id');

        $popular=News::where('status',ACTIVE)
            ->where('category_id',$info->category_id)
            ->where('id','!=',$info->id)->orderBy('view_count','DESC')
            ->take(5)->paginate(DEFAULT_PAGE_COUNT);


        $active='article';

        $og=array();

        $og['title']=$info->title;
        $og['description']=trim(strip_tags(str_limit($info->description,150,'...')));
        $og['url']=url('detail-detail/'.$id."/");
        $og['image']=asset($info->file_path.$info->file_name);

        return view('frontend.new_detail',compact('info','active','category','popular','event','active','s_list','og'));



    }

    public function getFbNewShare($id=0){

        $active='article';

        $event=$this->event;

        $s_list=$this->service;


        $info=News::find($id);

        if($info==null){

            abort(404);
        }


        $category=NewsCategory::pluck('name','id');

        $popular=News::where('status',ACTIVE)
            ->where('category_id',$info->category_id)
            ->where('id','!=',$info->id)->orderBy('view_count','DESC')
            ->take(5)->paginate(DEFAULT_PAGE_COUNT);


        $active='article';

        $og=array();

        $og['title']=$info->title;
        $og['description']=trim(strip_tags(str_limit($info->description,150,'...')));
        $og['url']=url('detail-detail/'.$id."/");
        $og['image']=asset($info->file_path.$info->file_name);

        return view('frontend.new_detail',compact('info','active','category','popular','event','active','s_list','og'));



    }

    public function getArticleCategory($id=0){

        $event=$this->event;

        $list=Article::where('status',ACTIVE)->where('category_id',$id)->paginate(DEFAULT_PAGE_COUNT);
        
        $category=Category::pluck('name','id');

        $popular=Article::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='article';

        $s_list=$this->service;

        return view('frontend.article',compact('list','category','popular','event','active','s_list'));
    }

    public function getNewCategory($id=0){

        $event=$this->event;

        $list=News::where('status',ACTIVE)->where('category_id',$id)->paginate(DEFAULT_PAGE_COUNT);
        
        $category=NewsCategory::pluck('name','id');

        $popular=News::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='article';

        $s_list=$this->service;

        return view('frontend.news',compact('list','category','popular','event','active','s_list'));
    }


    public function getSchool(){

        $s_list=$this->service;
        $event=$this->event;

        $active='link';

        $list=School::where('status',ACTIVE)->paginate(DEFAULT_PAGE_COUNT);

        return view('frontend.school',compact('list','event','active','s_list'));

    }

    public function postSchool(Request $request){

        $s_list=$this->service;
        $event=$this->event;

        $keyword=$request->term;

        $active='link';

        $query=School::where('status',ACTIVE);

        $query=$query->where(function($q)use($keyword){

                return $q->where('name','like', '%' .$keyword. '%')
                        ->orwhere('township','like','%'.$keyword.'%')
                        ->orWhere('course','like','%'.$keyword.'%');
        });

        $query=$query->orderBy('name','ASC');

        $list=$query->get();

        return view('frontend.school',compact('list','event','active','s_list','keyword'));

    }

    public function getSchoolDetail($id){

        $s_list=$this->service;
        $event=$this->event;

        $active='link';

        $info=School::where('status',ACTIVE)->find($id);

        $popular=School::where('status',ACTIVE)->where('id','!=',$id)->take(7)->get();

        return view('frontend.school-detail',compact('info','event','active','s_list','popular'));

    }

    public function getShop(){

        return view('frontend.shop');
    }

      public function getFaq(){

        $event=$this->event;

        $active='faq';

        $s_list=$this->service;

        return view('frontend.faq',compact('event','active','s_list'));
    }

    public function getContact(){

        $event=$this->event;

        $active='contact';

        $s_list=$this->service;

        return view('frontend.contact',compact('event','active','s_list'));
    }

    public function postContact(Request $request){

        $event=$this->event;

        $active='contact';

        $info=new Contact;

        $info->name=$request->name;
        $info->email=$request->email;
        $info->subject=$request->subject;
        $info->message=$request->message;

        /* Need to email ot the admin */

        $info->save();

        $data=array(
            'name'=>$request->name,
            'email'=>$request->email,
            'subject'=>$request->subject,
            'content'=>$request->content

        );

        $admin_email=ADMIN_EMAIL;

        Mail::send('emails.contact',$data, function ($message)use($admin_email,$request) {

            $message->from($request->email,$request->email);

            $message->to($admin_email)->subject('Contact Message');

        });


        Flash::success("Successfully message was successfully sent.Our team will contact you soon.");

        return redirect()->back();
    }

      public function getFinancetools(){

        $event=$this->event;

        $s_list=$this->service;

        return view('frontend.financetools',compact('event','s_list'));
    }

     public function getService(){

        $event=$this->event;

        $active='service';

        $list=Service::where('status',ACTIVE)->orderBy('featured_status','DESC')->orderBy('featured_sort_order','ASC')->get();

        $s_list=$this->service;

        return view('frontend.service',compact('list','event','active','s_list'));
    }

    public function getServiceDetail($id=0){

        $event=$this->event;

        $active='service';

        $list=Service::where('status',ACTIVE)->orderBy('featured_status','DESC')->orderBy('featured_sort_order','ASC')->get();

        $s_list=$this->service;

        return view('frontend.service-detail',compact('list','event','active','s_list','id'));


    }

     public function getEvent(){

        $event=$this->event;

        $active='event';

        $list=Event::where('status',ACTIVE)->paginate(DEFAULT_PAGE_COUNT);

        $category=EventCategory::pluck('name','id');

        $popular=Event::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $s_list=$this->service;

        return view('frontend.event',compact('list','category','popular','event','active','s_list'));
    }

    public function getEventCategory($id=0){

        $active='event';

        $event=$this->event;

        $list=Event::where('status',ACTIVE)->where('category_id',$id)->paginate(DEFAULT_PAGE_COUNT);

        $category=EventCategory::pluck('name','id');

        $popular=Event::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='event';


        $s_list=$this->service;

        return view('frontend.event',compact('list','category','popular','event','active','s_list'));
    }


    public function getEventDetail($id=0){

        $active='article';

        $event=$this->event;


        $info=Event::find($id);

        if($info==null){

            abort(404);
        }


        $category=EventCategory::pluck('name','id');

        $popular=Event::where('status',ACTIVE)->orderBy('view_count','DESC')->take(5)->paginate(DEFAULT_PAGE_COUNT);

        $active='event';

        $s_list=$this->service;


        return view('frontend.event-detail',compact('info','active','category','popular','event','active','s_list'));



    }

     public function getSchools(){

        return view('frontend.schools');
    }

     public function getSeminar(){


        return view('frontend.seminar');
    }

    

      public function getAboutus(){

        $active='aboutus';

        $s_list=$this->service;
        $event=$this->event;

        return view('frontend.aboutus',compact('active','s_list','event'));
    }


    public function getUsefulLink($category=''){

        $active='link';

        $event=$this->event;

        $list=Link::where('status',ACTIVE)->get();



        $s_list=$this->service;

        return view('frontend.usefullink',compact('active','list','s_list','event','category'));
    }

    public function getDownload(){


        $s_list=$this->service;
        $event=$this->event;

        $active='download';

        $category_list=Download::where('type','100')->get();

        $list=Download::where('status',ACTIVE)->paginate(DEFAULT_PAGE_COUNT);

        return view('frontend.download',compact('list','event','active','s_list'));

    }


    public function getDownloadCategory($category=''){


        $s_list=$this->service;
        $event=$this->event;

        $menu=array();

        $active='download';

        if($category!=''){

            $list=Download::where('status',ACTIVE)->where('category',$category)->paginate(DEFAULT_PAGE_COUNT);
            $menu=Resource::find($category);


    
        }else{

            $list=Download::where('status',ACTIVE)->paginate(DEFAULT_PAGE_COUNT);
        }
        
        $category_list=DownloadCategory::where('type','100')->get();


        return view('frontend.download',compact('category_list','list','event','active','s_list','category','menu'));

    }


    public function getDownloadDetail($id=0){

        $info=Download::find($id);
        
        $file=$info->file_path.$info->file_name;

        $headers = array(
              'Content-Type: application/pdf',
            );
        return Response::download($file,$info->file_name, $headers);
    }

    public function getDownloadRead($id=0){

        $info=Download::find($id);

        $s_list=$this->service;
        $event=$this->event;

        $active='download';

        $list=Download::where('status',ACTIVE)->where('id','!=',$id)->take(10)->get();


        return view('frontend.download_read',compact('list','info','event','active','s_list','category'));
        
        
    }

    public function getTaxProcedure($category=''){


        $s_list=$this->service;
        $event=$this->event;

        $active='tax';

        $list=TaxProcedure::where('status',ACTIVE)->get();

        return view('frontend.tax-procedure',compact('event','list','active','s_list'));

    }

    public function getTaxCalendar($category=''){


        $s_list=$this->service;
        $event=$this->event;

        $active='tax';

        $list=TaxProcedure::where('status',ACTIVE)->get();

        return view('frontend.tax-calendar',compact('event','list','active','s_list'));
    }

    public function getMarketData($para=''){

        $s_list=$this->service;

        $active='market';

        $event=$this->event;
        
        $category=Stock::pluck('name','id');


        if($para=='stock-market'){

            $info=StockMarket::where('date',date('Y-m-d'))->get();
            $photo=StockMarketPhoto::where('date',date('Y-m-d'))->first();

            return view('frontend.stock_market',compact('info','photo','event','active','s_list','category'));

        }else if($para=="ex-rate"){


            $client = new GuzzleHttp\Client();
            $res = $client->request('GET', 'http://forex.cbm.gov.mm/api/latest');

            $currency=$res->getBody();
            $currency=json_decode($currency,true); 

            return view('frontend.ex-rate',compact('currency','event','active','s_list'));


        }else{

            return view('frontend.market_data',compact('event','active','s_list'));
        }

        

    }

    public function getJob($para=''){

        $s_list=$this->service;
        $event=$this->event;

        if($para=='cv-submit'){

             return view('frontend.cv_submit',compact('list','event','active','s_list','list','featured_list'));

        }else{

            $featured_list=Job::where('status',ACTIVE)->where('featured_status',ACTIVE)->orderBy('featured_sort_order','ASC')->paginate(DEFAULT_PAGE_COUNT);


            $list=Job::where('status',ACTIVE)->where('featured_status','!=',ACTIVE)->orderBy('created_at','DESC')->paginate(DEFAULT_PAGE_COUNT);

            $active='job';

            

            

            return view('frontend.job',compact('list','event','active','s_list','list','featured_list'));
        }

    }

    public function getJobDetail($id=0){


        $s_list=$this->service;


        $featured_list=Job::where('status',ACTIVE)->where('featured_status',ACTIVE)->orderBy('featured_sort_order','ASC')->paginate(DEFAULT_PAGE_COUNT);


        $info=Job::find($id);

        $active='job';

        $event=$this->event;

        return view('frontend.job_detail',compact('info','event','active','s_list','list','featured_list'));

    }


    public function getTeam($para=''){

        $s_list=$this->service;
        
        $active='aboutus';

        $event=$this->event;

        $list=Team::orderBy('sort_order','ASC')->get();

        return view('frontend.team',compact('category','active','s_list','event','list'));

    }

    public function getUserLogin(){


        $s_list=$this->service;
        
        $active='home';

        $event=$this->event;

        $list=Team::orderBy('sort_order','ASC')->get();

        return view('frontend.user_login',compact('category','active','s_list','event','list'));

    }


    public function postUserLogin(Request $request){



        if (Auth::attempt(['email' =>$request->email, 'password' =>$request->password])) {
            
            Flash::overlay('Welcome Back', 'Welcome');

            return redirect('home');

        }

    }

    public function getUserRegister(Request $request){


        $s_list=$this->service;
        
        $active='home';

        $event=$this->event;

        $list=Team::orderBy('sort_order','ASC')->get();

        return view('frontend.user_register',compact('category','active','s_list','event','list'));

    }

    public function postUserRegister(Request $request){


        $rules = [

                'name' => 'required|max:50',
                'email' => 'required|email|unique:users|max:50',
                'password' => 'required|min:6|max:50',
                'confirmation_password' => 'required|min:6|max:50|same:password',
               
            ];

            $confirmation_code = str_random(30);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                
                return redirect()->back()->withErrors($validator)
                        ->withInput();
                
            }else{

                $info=new User;
                
                $info->email=$request->email;
                $info->password=Hash::make($request->password);
                $info->name=$request->name;
                $info->last_login=date("Y-m-d");
            
                $info->type=MEMBER;
                $info->status=ACTIVE;

                $info->save();

                /* Welcome email to the registered User */

                if (Auth::attempt(['email' =>$request->email, 'password' =>$request->password,'type'=>MEMBER,'status'=>ACTIVE])) {
                
                    Flash::overlay('Welcome Back', 'Welcome');

                    return redirect('home');

                }



                return redirect('home');
            } 

    }

    public function getUserDashboard(){

        if(Auth::check() && Auth::user()->type==MEMBER){

            $s_list=$this->service;
        
            $active='home';

            $event=$this->event;

            $list=Team::orderBy('sort_order','ASC')->get();

            return view('frontend.user.dashboard',compact('category','active','s_list','event','list'));

        }
    }

    public function getTaxCalculatar(){


        $s_list=$this->service;
        $event=$this->event;

        $active='tax';

        $list=TaxProcedure::where('status',ACTIVE)->get();

        return view('frontend.tax_calculatar',compact('event','list','active','s_list'));


    }

    public function getPremium(){

        $s_list=$this->service;
        $event=$this->event;

        $active='download';

        

        return view('frontend.premium',compact('event','active','s_list'));


    }




}
