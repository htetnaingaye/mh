<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\NewsCategory;
use Flash;
use Input;
use Response;
use Auth;
use File;

class NewsCategoryController extends Controller
{
    //

    public function search(Request $request){

        $active='new-category';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

        $list=NewsCategory::search($request->keyword,0)->get();

        $keyword=$request->keyword;
        $search=true;

        return view('backend.new-category.index',compact('list','data','search','keyword','active'));
    }

    public function index(){

        $active='new-category';
        $parent='news';

        $data['csstoload']=array('bootstrap-dialog');
        $data['jstoload']=array('bootstrap-dialog');

    	$list=NewsCategory::orderBy('created_at')->paginate(DEFAULT_PAGE_COUNT);
    	
    	return view('backend.new-category.index',compact('list','data','active','parent'));
    }

    public function basic($id=0){

         $active='new-category';
         $parent='news';

        $info=NewsCategory::find($id); 
        
	    return view('backend.new-category.basic',compact('info','id','active','parent'));

    }

    public function storeBasic(Request $request,$id=0){

    	$inputs=\Input::except("_token");

        $id=$request->id;

        if($id==0){

            $inputs['created_by']=Auth::user()->id;

            $info=NewsCategory::create($inputs);

            if($info->id > 0){

                Flash::success("Successfully saved.");
                
                return redirect(ADMIN_PREFIX.'/new-category');     
            } 

        }else{

            $info=NewsCategory::find($id);

            $info->name=$request->name;

            $info->save();

            Flash::success("Successfully updated.");

            return redirect(ADMIN_PREFIX.'/new-category');      

        }

    }


    public function destroy(Request $request,$id=0){

        if($request->ajax() && $id > 0){

            $info=NewsCategory::find($id);
            $info->delete();
            
            $message="Successfully Deleted.";
            
            return Response::json(array('success' => true,'message'=>$message));
        }
    }

   
}
