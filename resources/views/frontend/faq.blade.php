@extends('frontend.layout.template')
@section('content')

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Faqs</h1>
						<ul>
							<li><a href="{!!url('home')!!}">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Faqs</li>
						</ul>
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ================= /Banner ================ -->
<!-- ================== Faqs ================ -->
		<section class="faqs_sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 pull-right right_side"> <!-- Right Side -->
						<h2>Frequently Ask Questions</h2>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur, adipisci velit, sed quia non numquam eius modi .</p>
						
						
						<div class="tab_option">

							<div class="panel-group" id="accordion_two_two">
							  	<div class="panel panel-default">
								    <div class="panel-heading">
								      <h4 class="panel-title">
								        <a data-toggle="collapse" data-parent="#accordion_two" href="#collapse10">
								        	Reparing of boiler and heating system price
								        	<img src="{!!asset('public/frontend/images/icon-bg.png')!!}" alt="icon" class="active">
								        	<img src="{!!asset('public/frontend/images/icon-bg-hover.png')!!}" alt="icon" class="hover">
								    	</a>
								      </h4>
								    </div>
								    <div id="collapse10" class="panel-collapse collapse">
								      <div class="panel-body">
								      	<h3>Duis aute irure dolor in reprehenderit in voluptate</h3>
								      	<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur, adipisci velit, sed quia non numquam.</p>
								      	<p><img src="{!!asset('public/frontend/images/16.jpg')!!}" alt="images">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
								      </div>
								  </div>
								</div>
							  	<div class="panel panel-default">
								    <div class="panel-heading">
								      <h4 class="panel-title">
								        <a data-toggle="collapse" data-parent="#accordion_two" href="#collapse20">
								        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur.
								        <img src="{!!asset('public/frontend/images/icon-bg.png')!!}" alt="icon" class="active">
								        <img src="{!!asset('public/frontend/images/icon-bg-hover.png')!!}" alt="icon" class="hover">
								    	</a>
								      </h4>
								    </div>
								    <div id="collapse20" class="panel-collapse collapse">
								      <div class="panel-body">
								      	<h3>Duis aute irure dolor in reprehenderit in voluptate</h3>
								      	<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur, adipisci velit, sed quia non numquam.</p>
								      	<p><img src="{!!asset('public/frontend/images/16.jpg')!!}" alt="images">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
								      </div>
								    </div>
							 	</div>
							  	<div class="panel panel-default">
								    <div class="panel-heading">
								      <h4 class="panel-title">
								        <a data-toggle="collapse" data-parent="#accordion_two" href="#collapse30">
									        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur.
									        <img src="{!!asset('public/frontend/images/icon-bg.png')!!}" alt="icon" class="active">
								        	<img src="{!!asset('public/frontend/images/icon-bg-hover.png')!!}" alt="icon" class="hover">
								    	</a>
								      </h4>
								    </div>
								    <div id="collapse30" class="panel-collapse collapse">
								      <div class="panel-body">
								      	<h3>Duis aute irure dolor in reprehenderit in voluptate</h3>
								      	<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur, adipisci velit, sed quia non numquam.</p>
								      	<p><img src="{!!asset('public/frontend/images/16.jpg')!!}" alt="images">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
								      </div>
								    </div>
							  	</div>
							  	<div class="panel panel-default">
								    <div class="panel-heading">
								      <h4 class="panel-title">
								        <a data-toggle="collapse" data-parent="#accordion_two" href="#collapse40">
									        Reparing of boiler and heating system price.
									        <img src="{!!asset('public/frontend/images/icon-bg.png')!!}" alt="icon" class="active">
								        	<img src="{!!asset('public/frontend/images/icon-bg-hover.png')!!}" alt="icon" class="hover">
								    	</a>
								      </h4>
								    </div>
								    <div id="collapse40" class="panel-collapse collapse">
								      <div class="panel-body">
								      	<h3>Duis aute irure dolor in reprehenderit in voluptate</h3>
								      	<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consect etur, adipisci velit, sed quia non numquam.</p>
								      	<p><img src="{!!asset('public/frontend/images/16.jpg')!!}" alt="images">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
								      </div>
								    </div>
							 	</div>
							</div>
						</div> <!-- End tab_option -->
					</div> <!-- End Right Side -->
					<div class="col-lg-4 col-md-4 col-sm-12 pull-left left_side"> <!-- Left Side -->
						<h4>Search</h4>
						<form action="#">
							<input type="text" placeholder="Enter Search Keywords">
							<button type="submit"><span class="icon icon-Search"></span></button>
						</form>
						
						<h4>Categories</h4>
						<ul class="p0 category_item">
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial Investment</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Company Growth</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Taxes and Accounting</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial modeling and planning</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Profits & Wealth</a></li>
							<li><a href="" class="bottom_item"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Investments in Bonds</a></li>
						</ul>
						<h4>Popular Posts</h4>
						<ul class="p0 post_item">
							<li>AUG 12,2015<a href="">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
							<li>AUG 12,2015<a href="">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
							<li>AUG 12,2015<a href="" class="bottom_item">Making Cents Investments in Start-ups become profitable for Companies ...</a></li>
						</ul>
						
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div> <!-- End container -->
		</section>
<!-- ================== /Faqs ================ -->

@stop