@extends('frontend.layout.template')
@section('content')
<!-- =============== blog container ============== -->
		<article class="blog-container login-wrapper"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-md-6 col-md-offset-3">
						<?php
						
							$rates=$currency['rates'];
							$timestamp=$currency['timestamp'];
							
							$left=array("USD"=>"BND","EUR"=>"ILS","THB"=>"RUB","SGD"=>'SGD',
								'JPY'=>'JPY','EUR'=>'EUR',"CHF"=>"CHF");


						?>
						<h4 class="highlight-me ed-header">Exchange Rates ( {!!date('Y-m-d',$timestamp)!!} )</h4>
						
						<ul class="ex-rate">
							@foreach($rates as $key=>$value)	
								@if(array_key_exists($key,$left))
									<li>1 {!!$key!!} <span class="pull-right">{!!$value!!} MMK<span></li>
								@endif

							@endforeach
							<li class="lt-label">Lastest exchange rates from Central Bank of Myanmar</li>
							
						</ul>
					</div> <!-- End right-side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->
@stop