@extends('frontend.layout.template')
@section('content')
<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container mb15">
				<div class="row">

					<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12  right-side">
						<h1 class="text-center header">BOD  team</h1>
						
					</div> <!-- End right-side -->
				</div> <!-- End row -->

				<div class="row">
					<div class="col-md-12 text-center">
					@if(isset($list) && count($list))
							<?php $index=1;?>
						@foreach($list as $row)
							@if($row->type=='bod')
								
									<div class="t-box">
										<img src="{!!asset($row->file_path.$row->file_name)!!}" class="img-responsive text-center img-team">

										<h2 class="text-center">{!!$row->name!!}</h2>
										<h4 class="text-center">{!!$row->position!!}</h4>
										<div class="desc">
											{!!$row->description!!}
										</div>
									</div>
								
								@if($index%3==0)
									<div class="clearfix"></div>
								@endif
								<?php $index++;?>
							@endif
							
						@endforeach
					@endif
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">

					<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12  right-side">
						<h1 class="text-center header">Operation  team</h1>
						
					</div> <!-- End right-side -->
				</div> <!-- End row -->

				<div class="row">
					@if(isset($list) && count($list))
							<?php $i=1;?>
						<div class="col-md-12 text-center">
						@foreach($list as $row)
							@if($row->type=='operation')
								
									<div class="t-box">
										<img src="{!!asset($row->file_path.$row->file_name)!!}" class="img-responsive text-center img-team">

										<h2 class="text-center">{!!$row->name!!}</h2>
										<h4 class="text-center">{!!$row->position!!}</h4>
										<div class="desc">
											{!!$row->description!!}
										</div>
									</div>
								
								@if($i%3==0)
									<div class="clearfix"></div>
								@endif
								<?php $i++;?>
							@endif
							
						@endforeach
						</div>
					@endif
				</div>
			</div>
		</article>

<!-- =============== /blog container ============== -->
@stop