@extends('frontend.layout.template')
@section('content')

<!-- ======= revolution slider section ======= -->
	<section class="rev_slider_wrapper me-fin-banner" style="height:736px !important">
		<div id="slider1" class="rev_slider"  data-version="5.0">
			<ul>
				<li data-transition="fade">
					<img src="{!!asset('frontend/images/slides/1.jpg')!!}"  alt="">
					<div class="tp-caption sfb tp-resizeme banner-h1" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="290" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="500">
						Finance – Heartbeat of the Business<br>
						With a healthy heart... The Beat goes on…<br>
						With a healthy finance… The Business goes on…<br>
						We do with our heart…
						
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-border" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="400" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="800">
						
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-text" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="435" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1100">

				    </div>
					<div class="tp-caption sfl tp-resizeme " 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="510" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1400">
						<a class="banner-button" href="{!!url('aboutus')!!}">Readmore<i class="fa fa-arrow-circle-right"></i></a>
				    </div>
					
				</li>
				<li data-transition="fade">
					<img src="{!!asset('frontend/images/slides/2.jpg')!!}"  alt="">
					<div class="tp-caption sfb tp-resizeme banner-h1" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="290" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="500">
						Close your professional gap! 
						<br>Outsource your Finance and Accounting
						<br>Just keep the focus on your core business,
						<br>Close your professional gap with us…
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-border" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="400" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="800">
						
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-text" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="435" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1100">
						
				    </div>
					<div class="tp-caption sfb tp-resizeme " 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="510" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1400">
						<a class="banner-button" href="http://finance4sme.com/service-detail/7/Book-keeping-&-Accounting-Service.html">Read More <i class="fa fa-arrow-circle-right"></i></a>
				    </div>
				</li>
				<li data-transition="fade">
					<img src="{!!asset('frontend/images/slides/3.jpg')!!}"  alt="">
					<div class="tp-caption sfb tp-resizeme banner-h1" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="290" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="500">
						We find a way. 
						<br>We’ll get you there.
						<br>We understand you want an advisor . . .
						<br>We can provide more than traditional accounting solutions.
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-border" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="400" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="800">
						
				    </div>
					<div class="tp-caption sfb tp-resizeme banner-text" 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="435" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1100">
						
				    </div>
					<div class="tp-caption sfb tp-resizeme " 
				        data-x="left" data-hoffset="380" 
				        data-y="top" data-voffset="510" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1400">
						<a class="banner-button" href="{!!url('team')!!}">Read More <i class="fa fa-arrow-circle-right"></i></a>
				    </div>
				</li>
			</ul>
		</div>
	</section>
<!-- ======= /revolution slider section ======= -->


<!-- ======= Welcome section ======= -->
		<section class="welcome_sec">
			<div class="container">
				<div class="row welcome_heading">
					<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
						<h2>We offer <br>Different Services</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
						<p>We aims to provide quality Financial & Accounting study resources to students, professionals and others interested in learning the financial matters. We seek to provide comprehensive accounting and financial information site that are easy to understand.</p>
					</div>
				</div> <!-- End Row -->
				<div class="row welcome welcome_details">
					
						@if(isset($service_list) && count($service_list))
							@foreach($service_list as $row)

								<?php 
								
									$url =url('service-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
								?>


								<div class="col-lg-6 col-md-12 s-box">
									<div class="welcome_item">
										<img src="{!!asset($row->file_path.$row->file_name)!!}" alt="images" class="img-responsive s-img">
										<div class="welcome_info">
											<h3>{!!str_limit($row->title,25,'...')!!}</h3>
											<p>{!!str_limit($row->description,100,'...')!!}</p>
											<p><a href="{!!$url!!}" class="read-more pull-right">Detail <i class="fa fa-arrow-circle-o-right"></i></a></p>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						
					
					
				</div> <!-- End Row -->
			</div> <!-- End container -->
		</section> <!-- End welcome_sec -->
<!-- ======= /Welcome section ======= -->

		<div class="clearfix"></div>

<!-- ======== Small business ======== -->
		<section class="small_business_sec">
			<div class="business_opacity">
				<div class="container float_right">
					<h2>WE help planing small business</h2>
					<p>We seek to provide comprehensive accounting and financial information site that are easy to understand.</p>
					
					
					<ul class="p0 pull-left w50">
						<?php $index=1;?>
						@foreach($s_list as $row)
							@if($index<=3)
								<li><i class="fa fa-arrow-circle-right"></i><a href="{!!url('service-detail/'.$row->id)!!}">{!!$row->title!!}</a></li>
							@endif
						<?php $index++; ?>
						@endforeach
					</ul>
					
					
					<ul class="pull-left w50">
						<?php $index=1;?>
						@foreach($s_list as $row)
							@if($index<=3)
								<li><i class="fa fa-arrow-circle-right"></i><a href="">{!!$row->title!!}</a></li>
							@endif
						<?php $index++; ?>
						@endforeach
					</ul>
				</div> <!-- End container -->
			</div>
		</section> <!-- Edn small_business_sec -->
<!-- ======== /Small business ======== -->

<!-- ======== Latest News ======== -->
		<section class="client_wrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="text-center">Our Clients</h1>
						</div>
					</div>
					<div class="row">
						@if(isset($client) && count($client))
							<div class="col-md-12 text-center">
							@foreach($client as $row)
								
									<div class="cl_box text-center">
										<img src="{!!asset($row->file_path.$row->file_name)!!}" class="img-responsive">
										<h1 class="text-center">{!!$row->name!!}</h1>
									</div>
									
							@endforeach
							</div>
							
						@endif
					</div>
				</div>
		</section>
		<section class="p0 container-fluid latest_news_sec news_large">
			<div class="container">
				<h2>Latest News</h2>
			</div>
			<div class="news_highlight">

				@if(isset($latest) && count($latest))
					@foreach($latest as $row)

						<?php 
								
							$url =url('article-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
						?>


						<div class="col-md-3 col-md-6 news">
							<div class="news_img_holder">
								<img class="img-responsive" src="{!!asset($row->file_path.$row->file_name)!!}" alt="image" class="img-responsive">
								<div class="news_opacity">
								</div>
								<div class="news_details">
									<a href="{!!$url!!}">
										<span>{!!date('d F Y',strtotime($row->created_at))!!}</span>
										<h4>{!!$row->title!!}</h4>
										<p>{!!trim(strip_tags(str_limit($row->description,100,"...")))!!}</p>
									</a>
								</div>
							</div>
						</div>
					@endforeach
					<div class="clearfix"></div>
					<div class="col-md-12 text-center">
						<a href="{!!url('article')!!}" class="l-more">Read More News<i class="fa fa-arrow-circle-right"></i></a>
					</div>
				@endif
				
			</div>
		</section> <!-- End latest_news_sec -->
<!-- ======== /Latest News ======== -->




@stop