@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13 pt30">

						@if(isset($info) && $info!=null)
							
								<div class="single-blog-post anim-5-all">
									<div class="img-holder">
										<img src="{!!asset($info->file_path.$info->file_name)!!}" alt="{!!$info->title!!}">
									</div>
									<div class="post-meta">
										<div class="date-holder">
											<span>{!!date('d',strtotime($info->created_at))!!}</span> {!!date('F',strtotime($info->created_at))!!}
										</div>
										<div class="title-holder">
											<h2 class="title">{!!$info->title!!}</h2>
											<ul>
												<li><a href="#">Categories: {!!$info->category->name!!}</a></li>
												<li></li>
												<?php 
                                    				$f='https://www.facebook.com/sharer/sharer.php?u='.url('fbarticleshare/'.$info->id);
                                				?>
												<li>Share on: <a href="{!!$f!!}" class="btn_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="content">
										{!!$info->description!!}
										
									</div>
								</div>
							

						@endif

					</div> <!-- End right-side -->
					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container pt30"> <!-- Left Side -->
						
						<h4>Categories</h4>
						<ul class="p0 category_item">
							@if(isset($category) && count($category))
								@foreach($category as $key=>$value)

									<?php 
								
										$url =url('article-category/'.$key.'/'.preg_replace('/\s+/', '-', $value).".html");
									?>

									<li><a href="{!!$url!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$value!!}</a></li>
								@endforeach
							@endif
						</ul>
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop