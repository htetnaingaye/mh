@extends('frontend.layout.template')
@section('content')

		<section class="side_tab">
			<div class="container">
				<div class="row">
					<div class="white_bg pt20 right_side col-lg-9 col-md-8 col-sm-12 col-xs-12 pull-right">
						<div class="tab_details">
						    <!-- Tab panes -->
						    <div class=" right_info">

						  <!-- auto -->
						  	<?php $index=1;?>
						  	  

						  	 
						      <div class="tab-pane fade in row {!!$active!!}">
						      		@if(isset($list) && count($list))
						      			<div class="row">
						      			<?php $index=1;$found=false;?>

						      			@foreach($list as $row)
						      				

						      				<?php 
												$found=true;
												$url =url('download-read/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
											?>

						      				<div class="col-lg-4">
												<div class="s-box d-box">
													<img class="img-responsive" src="{!!asset($row->cover_file_path.$row->cover_file_name)!!}" alt="images" class="img-responsive">
													<h4>{!!$row->title!!}</h4>
													<p>{!!str_limit($row->description,100,"...")!!} </p>
													
													<a href="{!!$url!!}" class="read-more d-detail">Detail</a>

													<a href="{!!url('download-detail/'.$row->id)!!}" class="read-more">Download <i class="fa fa-download"></i></a>
												</div>
											</div>
											@if($index%3==0)
												<div class="clearfix"></div>
											@endif
											<?php $index++;?>
											
						      				
						      			@endforeach
						      			</div>

						      			@if(!$found)
						      				<div class="alert alert-info">
						      					<i class="fa fa-info-circle"></i> There is no record.
						      				</div>
						      			@endif

						      		@else
						      			<div class="alert alert-info">
						      				<i class="fa fa-info-circle"></i> There is no record.
						      			</div>
						      		@endif
						      </div>
						      <?php $index++;?>
						      
						    </div>
						</div> <!-- End tab_details -->
						
						<div class="clear_fix"></div>
						</div>
					<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 pull-left pt20"> 

						<div id="accordion" role="tablist" aria-multiselectable="true">
							<div class="card">
								    <div class="card-header" role="tab" id="headingOne">
								      
								        <a  href="{!!url('download-category')!!}" class="d-main">
								          All <i class="fa fa-list pull-right"></i>
								        </a>
								      
								    </div>
							</div>
							@if(isset($category_list) && count($category_list))
								@foreach($category_list as $row)

								<div class="card">
								    <div class="card-header" role="tab" id="headingOne">
								      
								        <a data-toggle="collapse" data-parent="#accordion" href="#{!!'collapse'.$row->id!!}" aria-expanded="true" aria-controls="collapseOne" class="d-main">
								          {!!$row->name!!} <i class="fa fa-angle-down pull-right"></i>
								        </a>
								      
								    </div>
								    <?php
								    $l=$row->downloadsub;
								    ?>
								    @if(count($l))
								    <div id="{!!'collapse'.$row->id!!}" class="collapse {!!isset($menu) && isset($menu->category_id) && $menu->category_id==$row->id?'in':''!!}" role="tabpanel" aria-labelledby="headingOne">
								      <div class="card-block">
								        	@foreach($l as $r)
								        		<a href="{!!url('download-category/'.$r->id)!!}" class="d-sub {!!isset($menu) && isset($menu->category_id) && $menu->id==$r->id?'active':''!!}">{!!$r->sub_category!!}</a>
								        	@endforeach
								      </div>
								    </div>
								    @endif
							  	</div>
								@endforeach
							@endif
						</div>  
					</div> <!-- End row -->
				<br>
			</div> <!-- End container -->
		</section>




@stop