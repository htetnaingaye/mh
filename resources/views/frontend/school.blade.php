@extends('frontend.layout.template')
@section('content')


<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two pt30"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">


						{!! Form::open(array('url' =>'school/filter','class'=>'form-horizontal','method'=>'post','autocomplete'=>'off')) !!}

						<div class="input-group loca-group">
					      <input type="text" name="term" class="form-control ui-autocomplete-input" id="location" placeholder="School name or Township name...">
					      <span class="input-group-btn">
					        <button class="btn btn-secondary" type="button"><i class="fa fa-search"></i></button>
					      </span>
					    </div>

						{!! Form::close()!!}
					</div>
				</div>
				<div class="row">
					@if(isset($list) && count($list))
						<?php $index=1;?>
						@foreach($list as $row)

							<?php 
								
								$url =url('school-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->name).".html");
							?>
							<div class="col-lg-4 col-xs-12 right-side">

								
								<div class="single-blog-post anim-5-all sch-box">
									
										<img src="{!!asset($row->file_path.$row->file_name)!!}" alt="" class="center-block img-responsive">

									
									
									<div class="title-holder">
										<h2 class="title"><a href="{!!$url!!}"> {!!$row->name!!}</a></h2>
										
									</div>
									
									<div class="content">
										{!!str_limit($row->description,200,'...')!!}
										<br>
										<br>
										<a href="{!!$url!!}" class="read-more">Read More <i class="fa fa-angle-right"></i></a>
									</div>
								</div>
									

							</div> <!-- End right-side -->
							@if($index%3==0)
								<div class="clearfix"></div>
							@endif
							<?php $index++;?>
						@endforeach
					@else

						<div class="col-md-12">
							<div class="alert alert-info">
								<i class="fa fa-info-circle"></i> There is no record.
							</div>
						</div>

					@endif
					
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->
<script src="{!! asset('frontend/js/jquery-ui.min.js')!!}"></script>
<script type="text/javascript">
	$(function(){

		$('#location').autocomplete({

	        source:"autocomplete",
	        minLength:2,
	        select: function(event, ui) {
	            var url = ui.item.id;
	            if(url != '#') {
	                
	            }

	           $(".menu-location").removeClass("show");
	        },

	    });

	});
</script>

@stop