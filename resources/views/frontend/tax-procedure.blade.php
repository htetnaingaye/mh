@extends('frontend.layout.template')
@section('content')
<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12  right-side ptb-13 pt30">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							@if(isset($list) && count($list))
								@foreach($list as $row)
							        <div class="panel panel-default">
							            <div class="panel-heading" role="tab" id="headingOne">
							                <h4 class="panel-title">
							                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{!!'collapse_'.$row->id!!}" aria-expanded="true" aria-controls="collapseOne">
							                        <i class="more-less glyphicon glyphicon-plus"></i>
							                       {!!$row->title!!}
							                    </a>
							                </h4>
							            </div>
							            <div id="{!!'collapse_'.$row->id!!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							                <div class="panel-body">
							                     {!!$row->description!!}
							                </div>
							            </div>
							        </div>
							     @endforeach

					        @endif

					    </div><!-- panel-group -->
					</div> <!-- End right-side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

<script type="text/javascript">
	function toggleIcon(e) {
	   
	    $(e.target)
	        .prev('.panel-heading')
	        .find(".more-less")
	        .toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);

	$('.panel-group').on('shown.bs.collapse', toggleIcon);

</script>

@stop