@extends('frontend.layout.template')
@section('content')

<section class="side_tab">
			<div class="container">
		<div class="row">
			<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right pt30">
				<div class="tab_details">
				    <!-- Tab panes -->
				    <div class="tab-content right_info">

				  <!-- auto -->
				  	<?php $o_list=json_decode(ORDINARY_LINK,true);$index=1;?>
				  	  @foreach($list as $row)

				  	  	<?php 
						  	  	$active='';

						  	  	if(isset($id) && $id==$row->id){

						  	  		$active='active';

						  	  	}else if(!isset($id)){

						  	  		$active=$index==1?'active':'';
						  	  	}
						?>
				      <div class="tab-pane fade in {!!$active!!}" id="{!!$row->id!!}">
				      		
				      			<div class="row">
				      				<div class="col-lg-12">
										<div class="s-box d-box">
											<img class="img-responsive" src="{!!asset($row->file_path.$row->file_name)!!}" alt="images" class="img-responsive">
											<h1>{!!$row->title!!}</h1>
											<p>{!!$row->description!!}</p>
											
										</div>
									</div>
									
				      				
				      			
				      			</div>
				      		
				      </div>
				      <?php $index++;?>
				      @endforeach
				    </div>
				</div> <!-- End tab_details -->
				
				<div class="clear_fix"></div>
				</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left left_side_bar pt30"> <!-- required for floating -->
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs tabs-left"><!-- 'tabs-right' for right tabs -->
			  	<?php $index=1;?>
			  	 @foreach($list as $row)

				  	<?php 
					  	  	$active='';

					  	  	if(isset($id) && $id==$row->id){

					  	  		$active='active';

					  	  	}else if(!isset($id)){

					  	  		$active=$index==1?'active':'';
					  	  	}
					?>
				  	  	
			    	<li class="{!!$active!!}"><a href="#{!!$row->id!!}" data-toggle="tab"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$row->title!!}</a></li>
			    <?php $index++;?>
			    @endforeach
			  </ul>
			  
		</div> <!-- End row -->
		<br>
	</div> <!-- End container -->
</section>

@stop