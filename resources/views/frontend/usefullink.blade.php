@extends('frontend.layout.template')
@section('content')

	<section class="side_tab">
			<div class="container">
				<div class="row">
					<div class="white_bg right_side col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
						<div class="tab_details">
						    <!-- Tab panes -->
						    <div class="tab-content right_info">

						  <!-- auto -->
						  	<?php $o_list=json_decode(WEBSITE_LINK,true);$index=1;?>
						  	  @foreach($o_list as $key=>$value)

						  	  <?php 
						  	  	$active='';

						  	  	if(isset($category) && $category==$key){

						  	  		$active='active';

						  	  	}else if($category==''){

						  	  		$active=$index==1?'active':'';
						  	  	}
						  	  ?>
						      <div class="tab-pane fade in  {!!$active!!}" id="{!!$key!!}">
						      		@if(isset($list) && count($list))
						      			

						      			@foreach($list as $row)
						      				@if($row->category_id==$key)
						      				<div class="row">
							      				<div class="col-md-3">	
							      					<img class="img-responsive" src="{!!asset($row->file_path.$row->file_name)!!}" alt="images" class="img-responsive">
							      				</div>
							      				<div class="col-lg-9">
													<div class="s-box d-box">
														
														<h4>{!!$row->title!!}</h4>

														<a href="{!!$row->url!!}" class="read-more" target="_blank">Visit Website <i class="fa fa-angle-right"></i></a>
													</div>
												</div>
											</div>
											<div class="mb15"></div>
											<hr>
											@endif
						      				
						      			@endforeach
						      			
						      		@else
						      			<div class="alert alert-info">There is no record yet.</div>
						      		@endif
						      </div>
						      <?php $index++;?>
						      @endforeach
						    </div>
						</div> <!-- End tab_details -->
						
						<div class="clear_fix"></div>
						</div>
					<div class="col-lg- col-md-4 col-sm-12 col-xs-12 pull-left left_side_bar"> <!-- required for floating -->
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs tabs-left"><!-- 'tabs-right' for right tabs -->
					  	<?php $o_list=json_decode(WEBSITE_LINK,true);$index=1;?>
					  	 @foreach($o_list as $key=>$value)

					  	 	 <?php 
						  	  	$active='';

						  	  	if(isset($category) && $category==$key){

						  	  		$active='active';

						  	  	}else if($category==''){

						  	  		$active=$index==1?'active':'';

						  	  	}
						  	  ?>

					    	<li class="{!!$active!!}"><a href="#{!!$key!!}" data-toggle="tab"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$value!!}</a></li>
					    <?php $index++;?>
					    @endforeach
					  </ul>
					  
				</div> <!-- End row -->
				<br>
			</div> <!-- End container -->
		</section>


	

<!-- =============== /blog container ============== -->

@stop