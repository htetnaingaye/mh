@extends('frontend.layout.template')
@section('content')
<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12  right-side pt30">
						<ol class="pre-list">
							<li>ဤ အင္တာနက္စာမ်က္ႏွာသည္ ဘ႑ာေရးနယ္ပယ္တြင္ အသက္ေမြး၀မ္းေက်ာင္းျပဳလုပ္ေနသူမ်ားအတြက္ ျပည္တြင္း၊ ျပည္ပမွ ပညာရပ္ဆိုင္ရာ စာအုပ္၊စာေပမ်ား၊ ACCA, CIMA, အစရိွေသာ Text book မ်ား ကို Online မွ တစ္ဆင့္ အလြယ္တစ္ကူ ၀ယ္ယူရရိွႏိုင္ရန္ ရည္ရြယ္လႊင့္တင္ထားေသာ အင္တာနက္စာမ်က္ႏွာ တစ္ခုျဖစ္ပါသည္။</li>

							<li>
								ယခုအခ်ိန္တြင္ စမ္းသပ္ဆဲကာလျဖစ္ေသာေၾကာင့္ <a href="download-category/law">Free download</a> စာမ်က္ႏွာမွ ပညာရပ္ဆိုင္ရာ စာအုပ္မ်ားကို သာလွ်င္ အခမဲ့ရယူႏိုင္ၿပီး မၾကာမွီကာလအတြင္း Premium Books မ်ားကို ၀ယ္ယူရရိွႏိုင္ ေတာ့မည္ျဖစ္ပါသည္။ 
							</li>
							<li>
								Free Download မွ စာအုပ္မ်ားအား Hard Copy အား ၀ယ္ယူလိုပါက <a href="mailto:info.mesc2016@gmail.com">info@finance4sme.com</a> ထံသို႔ ဆက္သြယ္၀ယ္ယူႏိုင္ပါသည္။
							</li>
							<li>
								အျခားသိရိွလုိသည္မ်ားကို <a href="mailto:info@finance4sme.com" target="_blank">info@finance4sme.com</a> ထံသို႔လည္း <a href="mailto:info.mesc2016@gmail.com" target="_blank">info.mesc2016@gmail.com</a>သို႔လည္းေကာင္း ဆက္သြယ္စံုစမ္းေမးျမန္းႏိုင္ပါသည္။ 
							</li>
							<li>
								ယခုကဲ့သို႔ အခ်ိန္ေပး ၀င္ေရာက္ၾကည့္ရႈျခင္းအတြက္ လူႀကီးမင္းအား အထူးပင္ေက်းဇူးတင္ရိွပါသည္။ 

							</li>
						</ol>
						<p>
							Admin Team<br>
							www.finance4sme.com
						</p>
					</div> <!-- End right-side -->

				</div> <!-- End row -->
			</div>
			<div class="pt30"></div>
		</article>


<!-- =============== /blog container ============== -->
@stop