@extends('frontend.layout.template')
@section('content')

<!-- ======== Service facts ========= -->
		<section class="service_facts">
			<div class="container">
				<div class="row">
					@if(isset($list) && count($list))
						<?php $index=0;?>
						@foreach($list as $row)
							<?php 
								
								$url =url('event-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
							?>


							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="s-box">
									<img class="img-responsive" src="{!!asset($row->file_path.$row->file_name)!!}" alt="images" class="img-responsive">
									<a href="{!!$url!!}">
										<h3>{!!$row->title!!}</h3>
									</a>
									<p>{!!str_limit($row->description,200,"...")!!}</p>
									<a href="{!!$url!!}" class="read-more">Read More <i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						<?php $index++;?>
							@if($index%3==0)
								<div class="clearfix"></div>
							@endif
						@endforeach
					@else
						<div class="col-md-12">
							<div class="alert alert-info">
								<p>There is no record found.</p>
							</div>
						</div>
					@endif
					
				</div>
				
			</div>
		</section>
<!-- ======== /Service facts ========= -->



@stop