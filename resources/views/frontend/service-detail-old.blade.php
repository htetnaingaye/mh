@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13">

						@if(isset($info) && $info!=null)
							
								<div class="single-blog-post anim-5-all">
									<div class="img-holder">
										<img src="{!!asset($info->file_path.$info->file_name)!!}" alt="">
										<div class="overlay"><a href=""><i class="fa fa-link"></i></a></div>
									</div>
									<div class="post-meta">
										<h2 class="title">{!!$info->title!!}</h2>
									</div>
									<div class="content">
										{!!$info->description!!}
										
									</div>
								</div>
							

						@endif

						

						

					</div> <!-- End right-side -->
					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container"> <!-- Left Side -->

						<h4>Other Services</h4>
						<ul class="p0 post_item">
							@if(isset($popular) && count($popular))
								@foreach($popular as $row)
									<li><a href="{!!url('article-detail/'.$row->id)!!}">
										{!!$row->title!!}
									</a></li>
								@endforeach
							@endif
						</ul>
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop