@extends('frontend.layout.template')
@section('content')
<!-- =================== Contact us container ============== -->
		<section class="contact_us_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;"> <!-- section title -->
						<h2>Get In Touch With Us</h2>
						<p>You can talk to our online representative at any time. Please leave a message, our team will response you as soon as possible.</p>
					</div> <!-- End section title -->
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 form_holder"> <!-- form holder -->
						{!!Form::open(array('url' => 'contact','method'=>'post','class'=>'')) !!}


							<input class="form-control name" type="text" name="name" required="true" placeholder="Your Name">
							<input class="form-control email" type="email" name="email" required="true" placeholder="Your Email">
						    <input class="form-control" type="text" name="subject" required="true" placeholder="Subject">
						    <textarea name="message" placeholder="Message" required="true"></textarea>
						    <button type="submit" class="submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
						{!!Form::close()!!}
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right address">
						<address>
							<div class="icon_holder float_left"><span class="icon icon-Pointer"></span></div>
							<div class="address_holder float_left">	B (5) 2 Malika Housing,Yadanar Street  <br> Thingangyun, Yangon <br>Myanmar</div>
						</address>
						<address class="clear_fix">
							<div class="icon_holder float_left"><span class="icon icon-Plaine"></span></div>
							<div class="address_holder float_left">info@finance4sme.com</div>
						</address>
						<address class="clear_fix">
							<div class="icon_holder float_left"><span class="icon icon-Phone2"></span></div>
							<div class="address_holder float_left"><a href="callto:+959253262626">+959 253 262 626</a></div>
						</address>
					</div>
				</div>
			</div>
		</section>
		<br>

<!-- =================== /Contact us container ============== -->


@stop