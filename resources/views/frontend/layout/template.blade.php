<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">

		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Finance4SME</title>

		<meta property="og:image:width" content="450"/>
    	<meta property="og:image:height" content="298"/>

	    <meta property="og:type" content="image" />
    	<meta property="og:title" content="{!!isset($og)?$og['title']:'BeHope'!!}" /> 
    	<meta property="og:description" content="{!!isset($og)?$og['description']:''!!}" />
    	<meta property="og:image" content="{!!isset($og)?$og['image']:asset('/frontend/images/logo.png')!!}" />

    	<meta name="google-site-verification" content="OXeGQe-A82UW_YkzDmxm7QQLUmg3EdSUWsPucbeYRPw" />
		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="{!!asset('/frontend/images/fav-icon/apple-icon-57x57.png')!!}">
		<link rel="apple-touch-icon" sizes="60x60" href="{!!asset('/frontend/images/fav-icon/apple-icon-60x60.png')!!}">
		<link rel="apple-touch-icon" sizes="72x72" href="{!!asset('/frontend/images/fav-icon/apple-icon-72x72.png')!!}">
		<link rel="apple-touch-icon" sizes="76x76" href="{!!asset('/frontend/images/fav-icon/apple-icon-76x76.png')!!}">
		<link rel="apple-touch-icon" sizes="114x114" href="{!!asset('/frontend/images/fav-icon/apple-icon-114x114.png')!!}">
		<link rel="apple-touch-icon" sizes="120x120" href="{!!asset('/frontend/images/fav-icon/apple-icon-120x120.png')!!}">
		<link rel="apple-touch-icon" sizes="144x144" href="{!!asset('/frontend/images/fav-icon/apple-icon-144x144.png')!!}">
		<link rel="apple-touch-icon" sizes="152x152" href="{!!asset('/frontend/images/fav-icon/apple-icon-152x152.png')!!}">
		<link rel="apple-touch-icon" sizes="180x180" href="{!!asset('/frontend/images/fav-icon/apple-icon-180x180.png')!!}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{!!asset('/frontend/images/fav-icon/android-icon-192x192.png')!!}">
		<link rel="icon" type="image/png" sizes="32x32" href="{!!asset('/frontend/images/fav-icon/favicon-32x32.png')!!}">
		<link rel="icon" type="image/png" sizes="96x96" href="{!!asset('/frontend/images/fav-icon/favicon-96x96.png')!!}">
		<link rel="icon" type="image/png" sizes="16x16" href="{!!asset('/frontend/images/fav-icon/favicon-16x16.png')!!}">


		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="{!!asset('/frontend/css/bootstrap/bootstrap.css')!!}" media="screen">
		<link rel="stylesheet" type="text/css" href="{!!asset('/frontend/css/bootstrap.vertical-tabs.css')!!}">


		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="{!!asset('/frontend/fonts/font-awesome/css/font-awesome.min.css')!!}">
		<!-- Stroke Gap Icon -->
		<link rel="stylesheet" href="{!!asset('/frontend/fonts/stroke-gap/style.css')!!}">
		<!-- owl-carousel css -->
		<link rel="stylesheet" href="{!!asset('/frontend/css/owl.carousel.css')!!}">
		<link rel="stylesheet" href="{!!asset('/frontend/css/owl.theme.css')!!}">
		
		<!-- owl-carousel css -->
		<link rel="stylesheet" href="{!!asset('/frontend/css/owl.carousel.css')!!}">
		<link rel="stylesheet" href="{!!asset('/frontend/css/owl.theme.css')!!}">
		<!-- Custom Css -->
		<link rel="stylesheet" type="text/css" href="{!!asset('/frontend/css/custom/style.css')!!}">
		<link rel="stylesheet" type="text/css" href="{!!asset('/frontend/css/responsive/responsive.css')!!}">
		
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery-2.1.4.js')!!}"></script>

		<!--[if lt IE 9]>
	   		<script src="js/html5shiv.js"></script>
		<![endif]-->



	</head>
	<body class="home">
<!-- =======Header ======= -->
		<header>
			<div class="container-fluid top_header">
				<div class="container">
					<p class="float_left">Welcome to Finance4SME Services, we have over 12 years of expertise</p>
					<div class="float_right">

						<ul class="social-icon">
							@if(Auth::check())

								<li><a href="{!!url('dashboard')!!}"><i class="fa fa-dashboard"> My Dashoard</i></a></li>

								<li><a href="{!!url('logout')!!}"><i class="fa fa-power-off"> Logout</i></a></li>


								
							@else
								
								<li><a href="{!!url('user-login')!!}"><i class="fa fa-lock"> Login </i> </a></li>
								<li><a href="{!!url('user-register')!!}"><i class="fa fa-user"> Register</i></a></li>


							@endif

						</ul>
					</div>
				</div> <!-- end container -->
			</div><!-- end top_header -->
			<div class="bottom_header top-bar-gradient">
				<div class="container clear_fix">
					<div class="float_left logo">
						<a href="{!!url('home')!!}">
							<img src="{!!asset('/frontend/images/logo.png')!!}" alt="Me Finance">
						</a>
					</div>
					<div class="float_right address">
						<div class="top-info">
							<div class="icon-box">
								<span class=" icon icon-Pointer"></span>							
							</div>
							<div class="content-box">
								<p> B (5) 2 Malika Housing,Yadanar Street <br><span>Thingangyun, Yangon</span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Phone2"></span>							
							</div>
							<div class="content-box">
								<p><a href="callto:+959253262626">+959 253 26 26 26 </a><br><span><a href="mailto:info@finance4sme.com">info@finance4sme.com</a></span></p>
							</div>
						</div>
						<div class="top-info">
							<div class="icon-box">
								<span class="separator icon icon-Timer"></span>
							</div>
							<div class="content-box">
								<p>24/7<br><span>Support</span></p>
							</div>
						</div>
					</div>
				</div> <!-- end container -->
			</div> <!-- end bottom_header -->
		</header> <!-- end header -->
<!-- ======= /Header ======= -->
<!-- ======= mainmenu-area section ======= -->
		<section class="mainmenu-area stricky">
			<div class="container">
				<nav class="clearfix">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header clearfix">
				      <button type="button" class="navbar-toggle collapsed">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="fa fa-th fa-2x"></span>
				      </button>
				    </div>

					<div class="nav_main_list custom-scroll-bar pull-left" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav" id="hover_slip">
							<li><a href="{!!url('/')!!}" class="{!!isset($active) && $active=='home'?'active':''!!}">Home</a></li>
							
							<li class="arrow_down">
								<a href="{!!url('aboutus')!!}" class="{!!isset($active) && $active=='service'?'active':''!!}">
								About Us</a>
								<div class="sub-menu">
									<ul>	
										<li><a href="{!!url('aboutus')!!}">About Us</a></li>
										<li><a href="{!!url('service')!!}">Service</a></li>
										<li><a href="{!!url('team')!!}">Our Team</a></li>
									</ul>
								</div>
							</li>

							<li class="arrow_down" >
								<a href="{!!url('news')!!}" class="{!!isset($active) && $active=='article'?'active':''!!}">News</a>

								<div class="sub-menu">
									<ul>	
										<li><a href="{!!url('news')!!}">News</a></li>
										<li><a href="{!!url('article')!!}">Articles</a></li>
										
									</ul>
								</div>
							</li>

							<li class="arrow_down" >
								<a href="{!!url('market-data')!!}" class="{!!isset($active) && $active=='market'?'active':''!!}">Market Data</a>

								<div class="sub-menu">
									<?php $m_data=json_decode(MARKET_DATA,true);?>
									<ul>	
										@foreach($m_data as $key=>$value)
											<li><a href="{!!url('market-data/'.$key)!!}">{!!$value!!}</a></li>
										@endforeach
										
									</ul>
								</div>
							</li>

							<li class="arrow_down" >
								<a href="{!!url('tax-procedure')!!}" class="{!!isset($active) && $active=='tax'?'active':''!!}">Tax</a>

								<div class="sub-menu">
									<ul>	
										<li><a href="{!!url('tax-procedure')!!}">Tax Prodedure</a></li>
										<li><a href="{!!url('tax-calculatar')!!}">Tax Calculatar</a></li>
										<li><a href="{!!url('tax-calendar')!!}">Tax Calendar</a></li>
									</ul>
								</div>
							</li>

							<li class="arrow_down" >
								<a href="{!!url('download')!!}" class="{!!isset($active) && $active=='download'?'active':''!!}">Resources</a>

								<div class="sub-menu">
									<ul>	

										<?php $premium=json_decode(PREMIUM_LINK,true);?>
										<li class="t-parent">
											<a href="{!!url('resources/premium')!!}" class="t-par" class="t-par">Premium</a>
										</li>


										<li class="t-parent">
											<a href="{!!url('download-category')!!}" class="t-par">Free</a>
											<?php $ordinary=json_decode(ORDINARY_LINK,true);?>
											
										</li>

										
									</ul>
								</div>
							</li>

							<li class="arrow_down" ><a href="{!!url('event')!!}" class="{!!isset($active) && $active=='event'?'active':''!!}">Event</a>
								<div class="sub-menu">
									<ul>	

										@foreach($event as $key=>$value)
											<li>
												<a href="{!!url('event-category/'.$key)!!}" class="t-normal">{!!$value!!}</a>
											</li>
										@endforeach

									</ul>
								</div>
							</li>




							<li class="arrow_down" >
								<?php $j_category=json_decode(JOB_CATEGORY,true);?>
								<a href="{!!url('job')!!}" class="{!!isset($active) && $active=='job'?'active':''!!}">Job</a>

								<div class="sub-menu">
									<ul>		
										@foreach($j_category as $key=>$value)
											<li><a href="{!!url('job/'.$key)!!}">{!!$value!!}</a></li>
										@endforeach
										
									</ul>
								</div>

							</li>

							<li>
								<a href="{!!url('/useful-links')!!}" class="{!!isset($active) && $active=='link'?'active':''!!}">Links</a>

								<?php $w_link=json_decode(WEBSITE_LINK,true);?>
								<div class="sub-menu">
									<ul>		

										<li>
											<a href="{!!url('school')!!}">Training Schools</a>
										</li>

										<li class="t-parent">
											<a href="{!!url('useful-links')!!}" class="t-par">Website Link</a>
											
											<ul class="t-menu">
											@foreach($w_link as $key=>$value)
												<li><a href="{!!url('useful-links/'.$key)!!}" class="t-normal">{!!$value!!}</a></li>
											@endforeach
											</ul>
										</li>
									</ul>
								</div>
							</li>

						</ul>						
					</div>
					<div class="find-advisor pull-right">
						<a href="{!!url('financetools')!!}" class="advisor ">Financial Tools</a>
					</div>
				</nav> <!-- End Nav -->
			</div> <!-- End Container -->
		</section>
<!-- ======= /mainmenu-area section ======= -->

	
	<div class="wrapper">
		@yield('content')
	</div>

	<!-- ==================shop ================ -->
<!-- ============ free consultation ================ -->
		<section class="container-fluid consultation">
			<div class="container">
				<p>If you have any querry for related investment  ... We are available</p>
				<a href="{!!url('contact')!!}">Contact us <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</section> <!-- End consultation -->
<!-- ============ /free consultation ================ -->

<!-- ============= Footer ================ -->
		<footer>
			<div class="top_footer container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-md-3 part2">

							<h5>About Us</h5>
							<ul class="p0">
								<li><a href="{!!url('home')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Home</a></li>
								<li><a href="{!!url('aboutus')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;About Us</a></li>

								<li><a href="{!!url('news')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;News</a></li>
								<li><a href="{!!url('article')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Articles</a></li>

								
								
								
							</ul>
							
						</div>
						<div class="col-lg-3 col-md-3 part2">
							<h5>Our Services</h5>
							<ul class="p0">
								@foreach($s_list as $row)


								<?php 
								
									$url =url('service-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
								?>
								<li><a href="{!!$url!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;{!!$row->title!!}</a></li>
								@endforeach
								
							</ul>
						</div>
						<div class="col-md-3 part2">
							<h5>Market Data</h5>
							<ul class="p0">
								@foreach($m_data as $key=>$value)
								<li><a href="{!!url('market-data',$key)!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;{!!$value!!}</a></li>
								@endforeach
								
							</ul>	

							<h5>Tax</h5>
							<ul class="p0">
								
								<li><a href="{!!url('tax-procedure')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Tax Procedure</a></li>
								<li><a href="{!!url('tax-calculatar')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Tax Calculatar</a></li>
								<li><a href="{!!url('tax-calendar')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Tax Calendar</a></li>

								
								
							</ul>		
						</div>	

						<div class="col-md-3 part2">
							<h5>Event</h5>
							<ul class="p0">
								<li><a href="{!!url('school')!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Training School</a></li>
								@foreach($event as $key=>$value)
								<li><a href="{!!url('event',$key)!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;{!!$value!!}</a></li>
								@endforeach
								
							</ul>	

							<h5>Job</h5>
							<ul class="p0">
								@foreach($j_category as $key=>$value)
								<li><a href="{!!url('event',$key)!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;{!!$value!!}</a></li>
								@endforeach
							</ul>	
						</div>
						
					</div> <!-- End row -->

					<div class="row">
						<div class="col-md-4 col-md-offset-4 part1 text-center">
							<h5 class="text-center">Find Us On</h5>
							<div class="text-center">
								<ul class="p0">
									<li><a href="{!!FACEBOOK_LINK!!}" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="{!!TWITTER_LINK!!}" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="{!!GOOGLE_LINK!!}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="{!!LINKIN_LINK!!}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="callto:{!!SKYPE_NAME!!}"><i class="fa fa-skype"></i></a></li>
								</ul>
							</div>	
						</div>
					</div>
				</div>
			</div> <!-- End top_footer -->
			<div class="bottom_footer container-fluid">
				<div class="container">
					<p class="float_left">Copyright &copy; finance4sme {!!date('Y')!!}. All rights reserved. </p>
					<p class="float_right">Created by: MM Tech</p>
				</div>
			</div> <!-- End bottom_footer -->
		</footer>

<!-- ============= /Footer =============== -->

		<!-- Js File -->

		<!-- j Query -->
		
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.mCustomScrollbar.concat.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.mCustomScrollbar.concat.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.bxslider.min.js')!!}"></script>
		<script src="{!!asset('/frontend/js/revolution-slider/jquery.themepunch.tools.min.js')!!}"></script> <!-- Revolution Slider Tools -->
		<script src="{!!asset('/frontend/js/revolution-slider/jquery.themepunch.revolution.min.js')!!}"></script> <!-- Revolution Slider -->
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.actions.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.carousel.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.kenburn.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.layeranimation.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.migration.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.navigation.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.parallax.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.slideanims.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/revolution-slider/extensions/revolution.extension.video.min.js')!!}"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="{!!asset('/frontend/js/bootstrap.min.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.appear.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.countTo.js')!!}"></script>
		<script type="text/javascript" src="{!!asset('/frontend/js/jquery.fancybox.pack.js')!!}"></script>
		<!-- owl-carousel -->
		<script type="text/javascript" src="{!!asset('/frontend/js/owl.carousel.js')!!}"></script>
		
		<!-- Custom & Vendor js -->
		<script type="text/javascript" src="{!!asset('/frontend/js/custom.js')!!}"></script>

		<script>
		    $('#flash-overlay-modal').modal();
		</script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-89705800-1', 'auto');
		  ga('send', 'pageview');

		</script>
		

	</body>
</html>