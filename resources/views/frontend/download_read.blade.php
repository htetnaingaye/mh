@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13 pt20">

						@if(isset($info) && $info!=null)
							
								<div class="single-blog-post anim-5-all">
									<div class="img-holder">
										<img src="{!!asset($info->cover_file_path.$info->cover_file_name)!!}" alt="" class="img-download">
										
									</div>
									<div class="post-meta">
										

										<h2 class="title p20">{!!$info->title!!}</h2>
											
										
									</div>
									<div class="content pt20">
										{!!$info->description!!}
										<a href="{!!url('download-detail/'.$info->id)!!}" class="read-more">Download <i class="fa fa-download"></i></a>
									</div>
								</div>
							

						@endif

						

						

					</div> <!-- End right-side -->
					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container pt20"> <!-- Left Side -->
						
						<h4>Related Books</h4>
						<ul class="p0 category_item">
							@if(isset($list) && count($list))
								@foreach($list as $row)
									<?php 
								
										$url =url('download-read/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
									?>

									<li><a href="{!!$url!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$row->title!!}</a></li>
								@endforeach
							@endif
						</ul>
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop