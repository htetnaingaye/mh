@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13 pt30">

						@if(isset($info) && $info!=null)
							
								<div class="single-blog-post anim-5-all">
									<div class="img-holder">
										<img src="{!!asset($info->file_path.$info->file_name)!!}" alt="{!!$info->name!!}">
									</div>
									<div class="post-meta">
										<h2 class="title">{!!$info->name!!}</h2>
										
									</div>
									<div class="content">

										<ul class="nav nav-tabs">
										  	<li class="active"><a href="#tab1default" data-toggle="tab">Program Description</a></li>
				                            <li><a href="#tab2default" data-toggle="tab">Duration & Price</a></li>

				                            <li><a href="#tab3" data-toggle="tab">Courses</a></li>
				                            <li><a href="#tab4" data-toggle="tab" id="map">Location on Map</a></li>
										  
										</ul>


										<div class="tab-content">
					                        <div class="tab-pane fade in active" id="tab1default">
					                        	<br>
					                        	{!!$info->description!!}
					                        </div>
					                        <div class="tab-pane fade" id="tab2default">
					                        	<div class="row">
					                        		<div class="col-md-4">
					                        			<div class="sc-box text-center">
					                        				<i class="fa fa-calendar"></i>
					                        				<h3>Start Date</h3>
					                        				<p>{!!$info->start_date!!}</p>
					                        			</div>

					                        		</div>
					                        		<div class="col-md-4">
					                        			<div class="sc-box text-center">
					                        				<i class="fa fa-clock-o"></i>
					                        				<h3>Duration</h3>
					                        				<p>{!!$info->duration!!}</p>
					                        			</div>
					                        		</div>
					                        		<div class="col-md-4">
					                        			<div class="sc-box text-center">
					                        				<i class="fa fa-money"></i>
					                        				<h3>Price</h3>
					                        				<p>{!!$info->price!!}</p>
					                        			</div>
					                        		</div>
					                        		
					                        	</div>
					                        </div>

					                        <div class="tab-pane" id="tab3">
					                        	@if($info->course!='')
					                        		<?php 
					                        		$tmp=explode(',',$info->course);
					                        		?>
					                        		

					                        		@if(count($tmp))
					                        			<ul class="c-list">
					                        				@foreach($tmp as $row)	
					                        					<li><i class="fa fa-graduation-cap"></i> {!!$row!!}</li>
					                        				@endforeach
					                        			</ul>
					                        		@endif
					                        	@endif
					                        </div>
					                        <div class="tab-pane" id="tab4">
					                        	<div class="map-panel">
													<div id="googleMap"></div>
												</div>
					                        	<style type="text/css">
					                        		.map-panel{
					                        			height:400px;
					                        			width:100%;
					                        		}
					                        		#googleMap{
					                        			width:100%;
					                        			height:100%;
					                        		}

					                        		#googleMap img {
													    max-width: none;
													}
					                        	</style>
					                        </div>
					                        
					                    </div>
									</div>
								</div>
						@endif
					</div> <!-- End right-side -->

					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container pt30"> <!-- Left Side -->
						
						<h4>Related Schools</h4>
						<ul class="p0 post_item">
							@if(isset($popular) && count($popular))
								@foreach($popular as $row)

									<?php 
								
										$url =url('school-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->name).".html");
									?>

									<li><a href="{!!$url!!}">
										{!!$row->name!!}
									</a></li>
								@endforeach
							@endif
						</ul>
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@if($info->lat!='' && $info->lng)
		<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB1orum3qYQkkbAgpmbLV4wPvIREHSDb14"></script>

		<script>
		$(function(){


			var lat="{!!$info->lat!!}";
			var lng="{!!$info->lng!!}";

			var myCenter=new google.maps.LatLng(lat,lng);
			var marker=new google.maps.Marker({
			    position:myCenter
			});

			function initialize() {
			  var mapProp = {
			      center:myCenter,
			      zoom: 14,
			      draggable: false,
			      scrollwheel: false,
			      mapTypeId:google.maps.MapTypeId.ROADMAP
			  };
			  
			  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			  marker.setMap(map);
			    
			  google.maps.event.addListener(marker, 'click', function() {
			      
			    infowindow.setContent(contentString);
			    infowindow.open(map, marker);
			    
			  }); 
			};

			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			  	initialize();
			});

			

		});
		</script>
	@endif

@stop