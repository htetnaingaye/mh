@extends('frontend.layout.template')
@section('content')


<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec login-wrapper"> <!-- faqs_sec use for style side content -->
			<div class="container">
				@if(isset($featured_list) && count($featured_list))
					<div class="row">
						<div class="col-md-offset-1 col-md-9">
							<h3>Featured Jobs</h3>
							<hr>
						</div>
					</div>
					@foreach($featured_list as $row)
						<div class="row">

							<div class="col-md-4 col-md-offset-1">
								<h4><a href="{!!url('job-detail/'.$row->id)!!}"> {!!$row->title!!}</a></h4>
								<p>{!!$row->company!!}</p>
							</div>

							<div class="col-md-2">
								{!!$row->location!!}
							</div>
							<div class="col-md-2">
								<a href="">{!!$row->position!!}</a>
							</div>
							<div class="col-md-1">
								<button class="btn btn-xs btn-success">Full Time</button>
							</div>
						</div> <!-- End row -->
						<div class="row">	
							<div class="col-md-9 col-md-offset-1">
								<hr>
							</div>
						</div>
					@endforeach
				@endif
				<br><br><br>
				@if(isset($list) && count($list))
					<div class="row">
						<div class="col-md-offset-1 col-md-9">
							<h3>Latest Jobs</h3>
							<hr>
						</div>
					</div>
					@foreach($list as $row)
						<div class="row">

							<div class="col-md-4 col-md-offset-1">
								<h4><a href="{!!url('job-detail/'.$row->id)!!}"> {!!$row->title!!}</a></h4>
								<p>{!!$row->company!!}</p>
							</div>

							<div class="col-md-2">
								{!!$row->location!!}
							</div>
							<div class="col-md-2">
								<a href="">{!!$row->position!!}</a>
							</div>
							<div class="col-md-1">
								<button class="btn btn-xs btn-success">Full Time</button>
							</div>
						</div> <!-- End row -->
						<div class="row">	
							<div class="col-md-9 col-md-offset-1">
								<hr>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop