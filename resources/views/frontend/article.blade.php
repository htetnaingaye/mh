@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13 pt30">

						@if(isset($list) && count($list))
							@foreach($list as $row)
								<?php 
								
									$url =url('article-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
								?>
								<div class="row">
									<div class="col-md-4">
										<img src="{!!asset($row->file_path.$row->file_name)!!}" alt="" class="img-responsive">
									</div>
									<div class="col-md-8">
										<h2 class="a-title"><a href="{!!$url!!}"> {!!$row->title!!}</a></h2>
										<ul class="a-category">
											<li>Categories:<a href="#"> {!!$row->category->name!!}</a></li>
										</ul>
										<div class="content">
											{!!str_limit($row->description,300,'...')!!}
											<a href="{!!$url!!}" class="read-more a-more">Read More <i class="fa fa-angle-right"></i></a>
										</div>
									</div>
								</div>
								<hr>
							@endforeach
							<div class="clearfix"></div>
							<div class="pull-right">
								{!!$list->render()!!}
							</div>
						@else

							<div class="alert alert-info">
								<p>There is no record yet.</p>
							</div>

						@endif

						

						

					</div> <!-- End right-side -->
					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container pt30"> <!-- Left Side -->
						
						
						<h4>Categories</h4>
						<ul class="p0 category_item">
							@if(isset($category) && count($category))
								@foreach($category as $key=>$value)

									<?php 
								
										$url =url('article-category/'.$key.'/'.preg_replace('/\s+/', '-', $value).".html");
									?>


									<li><a href="{!!$url!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$value!!}</a></li>
								@endforeach
							@endif
						</ul>

						<h4>Popular Posts</h4>
						<ul class="p0 post_item">
							@if(isset($popular) && count($popular))
								@foreach($popular as $row)
									<li>{!!date('F d, Y',strtotime($row->created_at))!!}
									<?php 
								
										$url =url('article-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
									?>
									<a href="{!!$url!!}">
										{!!$row->title!!}
									</a></li>
								@endforeach
							@endif
						</ul>
						


					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop