@extends('frontend.layout.template')
@section('content')

<!-- ======= Banner ======= -->
		<section class="p0 container-fluid banner about_banner">
			<div class="about_banner_opacity">
				<div class="container">
					<div class="banner_info_about">
						<h1>Shop</h1>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><i class="fa fa-angle-right"></i></li>
							<li>Shop</li>
						</ul>
						
					</div> <!-- End Banner Info -->
				</div> <!-- End Container -->
			</div> <!-- End Banner_opacity -->
		</section> <!-- End Banner -->
<!-- ======= /Banner ======= -->
<!-- ==================shop ================ -->
		<section class="shop_container faqs_sec"> <!-- faqs_sec use for style left side content -->
			<div class="container">
				<div class="row">
					<!-- .shop-page-content -->
					<div class="col-lg-8 col-md-8 pull-right shop-page-content">

						<div class="row best-seller">
							<div class="section-title-style-2">
								<h2>Best Seller</h2>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/7.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/8.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/9.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>

						<div class="section-title-style-2">
							<h2>Featured Products</h2>
						</div>				
						<div class="row">

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/1.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/2.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/3.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>	

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/4.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/5.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 single-shop-item">
								<img class="img-responsive" src="{!!asset('public/frontend/images/shop/6.jpg')!!}" alt="">
								<div class="meta">
									<h4>Invest Bonds</h4>
									<p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit,</p>
									<span>Price: <b>$24.5</b></span>
									<a href="cart-page.html">Add to Cart <i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
						
						<ul class="pagination">
						  <li><a href="#"> << </a></li>
						  <li><a href="#">1</a></li>
						  <li><a href="#">2</a></li>
						  <li><a href="#">3</a></li>
						  <li><a href="#">4</a></li>
						  <li><a href="#">5</a></li>
						  <li><a href="#">6</a></li>
						  <li><a href="#"> >> </a></li>
						</ul>

					</div> <!-- /.shop-page-content -->
					<div class="col-lg-4 col-md-4 col-sm-12 pull-left left_side pdr5"> 
						<h4>Search</h4>
						<form action="#">
							<input type="text" placeholder="Enter Search Keywords">
							<button type="submit"><span class="icon icon-Search"></span></button>
						</form>
						<h4>Categories</h4>
						<ul class="p0 category_item">
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial Investment</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Company Growth</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Taxes and Accounting</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Financial modeling and planning</a></li>
							<li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Profits & Wealth</a></li>
							<li><a href="" class="bottom_item"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;Investments in Bonds</a></li>
						</ul>
						<h4>New Products</h4>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="{!!asset('public/frontend/images/product-sidebar/1.jpg')!!}" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="{!!asset('public/frontend/images/product-sidebar/2.jpg')!!}" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<div class="row single_product_item">
							<div class="col-lg-5">
								<img src="{!!asset('public/frontend/images/product-sidebar/3.jpg')!!}" alt="images">
							</div>
							<div class="col-lg-7">
								<p>Neque porro quisqua mest qui dolorem.</p>
								<a href="">more info</a>
							</div>
						</div>
						<a href="" class="brochure"><img src="{!!asset('public/frontend/images/product/2.jpg')!!}" alt="image"></a>
						<a href="" class="free_shipping"><img src="{!!asset('public/frontend/images/product/3.jpg')!!}" alt="image"></a>
					</div> <!-- End left side -->
				</div>  <!-- End row -->
			</div> <!-- End Container -->
		</section>

<!-- ==================shop ================ -->
@stop