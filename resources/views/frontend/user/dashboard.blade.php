@extends('frontend.layout.template')
@section('content')

<div class="login-wrapper contact_us_container">	
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<h3>User's Dashboard</h3>
				<div class="panel panel-default">
					<div class="panel-body form_holder pr0">
							<div class="content">

										<ul class="nav nav-tabs">
										  	<li class="active"><a href="#tab1default" data-toggle="tab">Profile</a></li>
				                            <li><a href="#tab2default" data-toggle="tab">Change Password</a></li>
										  
										</ul>


										<div class="tab-content">
					                        <div class="tab-pane fade in active" id="tab1default">
					                        	<br>
					                        	<p>Name: {!!Auth::user()->name!!}</p>
					                        	<p>Email: {!!Auth::user()->email!!}</p>
					                        </div>

					                        <div class="tab-pane fade" id="tab2default">
					                        	<br>
					                        	<br>
					                        	{!! Form::open(array('url' =>'user-login','class'=>'form-horizontal frmBasic','files'=>true,'method'=>'post')) !!}


					                        		@if (count($errors) > 0)
													    <div class="alert alert-danger">
													        <ul>
													            @foreach ($errors->all() as $error)
													                <li>{{ $error }}</li>
													            @endforeach
													        </ul>
													    </div>
													@endif

													<div class="form-group">
														<div class="col-md-12">
															<label>Current Password</label>
															<input type="password" name="password" class="form-control" placeholder="Current Password" required="true">
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-12">
															<label>New Password</label>
															<input type="password" name="new_password" class="form-control" placeholder="New Password " required="true">
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-12">
															<label>Confirm Password</label>
															<input type="password" name="confirmation_password" class="form-control" placeholder="Confirm Password" required="true">
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-12">	
															<div class="pull-right">
																<button type="submit" class="submit mt0">Submit <i class="fa fa-arrow-circle-right"></i></button>
															</div>
														</div>
													</div>

													
												{!!Form::close()!!}
					                        </div>
					                        
					                    </div>
															
										
									</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop