@extends('frontend.layout.template')
@section('content')
<!-- =============== blog container ============== -->
		<article class="blog-container login-wrapper"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-md-4">
						@if(isset($photo) && $photo!=null)
							@if($photo->left_file_name!='')
								<img src="{!!asset($photo->left_file_path.$photo->left_file_name)!!}" class="img-responsive center-block">
							@endif
						@endif
					</div>

					<div class="col-md-4">
						<p>Date: <strong>{!!date("Y-m-d")!!}</strong></p>
						<table class="table table-bordered stock-table">
							<tr>

								<th rowspan="2" class="text-center">
									<span class="pt15">Stock Name</span>
								</th>
								<th colspan="2" class="text-center">
									11 AM
								</th>

								<th colspan="2" class="text-center">
									1:30 PM

								</th>
								<th rowspan="2" class="text-center">
									<span class="pt15">+/-</span>
								</th>
							</tr>
							<tr>
								
								<th>
									Price
								</th>
								<th>
									Unit
								</th>

								<th>
									Price

								</th>
								<th>
									Unit

								</th>
								
								
							</tr>

							@if(isset($info) && count($info))
								
									@foreach($info as $row)
									<tr>
										<td><a href="{!!$row->stock->link!!}" target="_blank"> {!!$category[$row->stock_id]!!}</a></td>
										<td>{!!$row->eleven_price!!}</td>
										<td>
											{!!$row->eleven_unit!!}
										</td>
										<td>
											{!!$row->one_price!!}
										</td>

										<td>
											{!!$row->one_unit!!}

										</td>
										<td>
											{!!$row->different!!}

										</td>
										
										
									</tr>
									@endforeach
								
							@endif
						</table>
					</div> <!-- End right-side -->
					<div class="col-md-4">
						@if(isset($photo) && $photo!=null)
							@if($photo->right_file_name!='')
								<img src="{!!asset($photo->right_file_path.$photo->right_file_name)!!}" class="img-responsive center-block">
							@endif
						@endif
					</div> <!-- End right-side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->
@stop