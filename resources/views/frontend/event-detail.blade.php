@extends('frontend.layout.template')
@section('content')

<!-- ================= /Banner ================ -->
<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two"> <!-- faqs_sec use for style side content -->
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-left right-side ptb-13 pt30">

						@if(isset($info) && $info!=null)
							
								<div class="single-blog-post anim-5-all">
									<div class="img-holder">
										<img src="{!!asset($info->file_path.$info->file_name)!!}" alt="{!!$info->title!!}">
										
									</div>
									<div class="post-meta">
										<h2 class="title">{!!$info->title!!}
											<span class="pull-right paid-status">
												@if($info->paid_status==2)
													Paid <i class="fa fa-money"></i> - {!!$info->fee!!}
												@elseif($info->paid_status==1)
													Free
												@endif
											</span>
										</h2>
										<ul class="p0">
											<li><a href="#">Categories: {!!$info->category->name!!}</a></li>

										</ul>

											<script type="text/javascript">(function () {
										            if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
										            if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
										                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
										                s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
										                s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
										                var h = d[g]('body')[0];h.appendChild(s); }})();
										    </script>

										    <!-- 3. Place event data -->
										    <span class="addtocalendar atc-style-blue">
										        <var class="atc_event">
										            <var class="atc_date_start">{!!$info->start_date!!}</var>
										            <var class="atc_date_end">{!!$info->end_date!!}</var>
										            <var class="atc_timezone">Europe/London</var>
										            <var class="atc_title">{!!$info->title!!}</var>
										            <var class="atc_description">{!!str_limit($info->description,80,'...')!!}</var>
										            <var class="atc_location">{!!$info->location!!}</var>
										            <var class="atc_organizer">{!!$info->organizer!!}</var>
										            <var class="atc_organizer_email">{!!$info->organizer_email!!}</var>
										        </var>
										    </span>

										
										
									</div>
									 <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
										 
									<p>
										<span class="paid-status"><i class="fa fa-map-marker"></i> Location:</span> {!!$info->location!!}
									</p>
									<p>
										<span class="paid-status"><i class="fa fa-calendar"></i> Date:</span> {!!$info->start_date!!} -  {!!$info->end_date!!}
									</p>
									<div class="content">
										{!!$info->description!!}
									</div>
								</div>
							

						@endif

						

						

					</div> <!-- End right-side -->
					<div class="col-lg-4 col-md-4 col-sm-12 left_side blog_right_container pt30"> <!-- Left Side -->
						
						<h4>Categories</h4>
						<ul class="p0 category_item">
							@if(isset($category) && count($category))
								@foreach($category as $key=>$value)

								<?php 
								
									$url =url('event-category/'.$key.'/'.preg_replace('/\s+/', '-', $value).".html");
								?>

									<li><a href="{!!$url!!}"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!$value!!}</a></li>

								@endforeach
							@endif
						</ul>

						<h4>Related Events</h4>
						<ul class="p0 post_item">
							@if(isset($popular) && count($popular))
								@foreach($popular as $row)

								<?php 
								
									$url =url('event-detail/'.$row->id.'/'.preg_replace('/\s+/', '-', $row->title).".html");
								?>

									<li>{!!date('F d, Y',strtotime($row->created_at))!!}<a href="{!!$url!!}">
										{!!$row->title!!}
									</a></li>
								@endforeach
							@endif
						</ul>
					</div> <!-- End left side -->
				</div> <!-- End row -->
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop