@extends('frontend.layout.template')
@section('content')

<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec blog-two about-wrapper"> <!-- faqs_sec use for style side content -->
			<div class="container text-justify">
				<div class="row">
					<br><br>
					<div class="col-md-6">

					<h1>About MES</h1>

	<p>Myanmar Enterprise Solutions is about bringing our clients a positive outsourcing experience for their accounting and corporate financial planning needs.</p>
<p>MES’s talented partners and associates specialize in the areas of accounting setup, assurance, financial planning, and business advisory who really love what we do and we strongly believe that small and mid-size businesses gain far more benefits from having experienced financial, human resources and virtual assistant expertise than larger companies who actually can afford to have this experience on staff.  </p>
<p>At MES, we understand you want an advisor who can provide more than traditional accounting solutions. Our senior partners offer specialized skills and expertise, as well as the winning attitude, character and communication skills necessary to help clients achieve their objectives. 
For more information about working with Myanmar Enterprise Solutions, we invite you to contact us for a free consultation. </p>
					
					
					<h1>Our Mission</h1>
					<p>To promote the Myanmar SME Owners feel confident about their financial future. </p>

					
					<h1>Our Philosophy</h1>
					<p>Finance – Heartbeat of the Business. We do with our heart.</p>

					<h1>Our Growth</h1>
					<p>We work hard to continually improve our businesses and to ensure sustainable future growth for the group. Our growth is dependent upon providing clients financial fuel to drive their ideas, inspirations, and goals into profitable results.</p>

					</div>

					<div class="col-md-6">
						<h1>About www.finance4sme.com</h1>
						<p>This project is part of our CSR Program to promote the society of Myanmar CPA, Accounting, and Financial Expert whoever working in the Financial World.</p>
						<p>We aims to provide quality Financial & Accounting study resources to students, professionals and others interested in learning the financial matters. We seek to provide comprehensive accounting and financial information site that are easy to understand. In this website include:</p> 
						<p>Articles:<br>
						Update News:<br>
						Job Connection:<br>
						Financial Tools:<br>
						Tax Calendar & Tax Calculator:</p>
					</div>
					
				</div> <!-- End row -->
			</div>
			<br><br>
		</article>


<!-- =============== /blog container ============== -->

@stop