@extends('frontend.layout.template')
@section('content')

	
	<div class="login-wrapper contact_us_container">	
		<div class="container">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-body form_holder pr0">
						<a href="{!!url('redirect')!!}" class="btn btn-facebook btn-block">Login with Facebook</a>
						<p class="text-center">OR</p>
						<hr>

						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif

						{!! Form::open(array('url' =>'user-register','class'=>'form-horizontal frmBasic','files'=>true,'method'=>'post')) !!}

							<div class="form-group">
								<div class="col-md-12">
									<label>Name</label>
									{!! Form::text('name',null, ['class'=>'form-control','placeholder'=>'Enter your Your', 'required']) !!}
								</div>
							</div>


							<div class="form-group">
								<div class="col-md-12">
									<label>Email</label>
									{!! Form::email('email',null, ['class'=>'form-control','placeholder'=>'Enter your email', 'required']) !!}
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<label>Password</label>
									<input type="password" name="password" class="form-control" placeholder="Password" required="true">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<label>Confirm Password</label>
									<input type="password" name="confirmation_password" class="form-control" placeholder="Confirm Password" required="true">
								</div>
							</div>


								<div class="col-md-12">	
									<div class="pull-right">
										<button type="submit" class="submit mt0">Submit <i class="fa fa-arrow-circle-right"></i></button>
									</div>
								</div>
							</div>

							
						{!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop