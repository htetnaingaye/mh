@extends('frontend.layout.template')
@section('content')


<!-- =============== blog container ============== -->
		<article class="blog-container faqs_sec login-wrapper"> <!-- faqs_sec use for style side content -->
			<div class="container">
				@if(isset($info) && $info!=null)
					<div class="row">
						<div class="col-md-11 col-md-offset-1">
							<h4><a href="#"> {!!$info->title!!}</a></h4>
							<p>{!!$info->company!!}</p>
							<hr>
							<h5>Responsibility</h5>
				             {!!$info->responsibility!!}

				             <h5>Requirements</h5>
				             {!!$info->requirements!!}

				            <h5>Salary</h5>
				            <p>{!!$info->salary!!} </p>

				            <h5>Apply</h5>
				           {!!$info->apply!!}
						</div>
						
					</div>
				@endif
				<hr>
				@if(isset($featured_list) && count($featured_list))
					<div class="row">
						<div class="col-md-offset-1 col-md-9">
							<h3>Featured Jobs</h3>
							<hr>
						</div>
					</div>
					@foreach($featured_list as $row)
						<div class="row">

							<div class="col-md-4 col-md-offset-1">
								<h4><a href="{!!url('job-detail/'.$row->id)!!}"> {!!$row->title!!}</a></h4>
								<p>{!!$row->company!!}</p>
							</div>

							<div class="col-md-2">
								{!!$row->location!!}
							</div>
							<div class="col-md-2">
								<a href="">{!!$row->position!!}</a>
							</div>
							<div class="col-md-1">
								<button class="btn btn-xs btn-success">Full Time</button>
							</div>


						</div> <!-- End row -->
						<div class="row">	
							<div class="col-md-9 col-md-offset-1">
								<hr>
							</div>
						</div>
						
					@endforeach
				@endif
			</div>
		</article>

<!-- =============== /blog container ============== -->

@stop