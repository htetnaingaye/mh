@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Procedure <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->title:'New Procedure'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Procedure - ".$info->title:"New Procedure"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/procedure/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Tax Procedure</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/procedure/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">

                            

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('title',isset($info)?$info->title:null, ['class'=>'form-control','placeholder'=>'Title', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Description<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('description',isset($info)?$info->description:null, ['class' => 'form-control','id'=>'editor']) !!}
                                    
                                </div>
                            </div>



                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>


    <script>
        $(function(){

           CKEDITOR.replace('editor',{             
                                                                
            });


        });
    </script>
    <script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>

   
    
@stop
