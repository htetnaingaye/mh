@extends('backend.layout.template')
@section('content')

<section class="content-header">
    <h1>
        Featured Services
    </h1>
    
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@include('flash::message')
		</div>

	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box-header">
                    <i class="fa fa fa-th"></i>
                    <div class="pull-right box-tools">
                        <a href="{!!url('admin/article/basic/0')!!}" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Add New Article</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="panel-body">
                    @if(isset($list) && count($list))

                    <div class="sortable-wrapper" id="sortable-wrapper">
                    <table class="table table-striped">
                        <ul class="table">
                            <li>
                                <div class="col-xs-1">#</div>
                                <div class="col-xs-2">Image</div>
                                <div class="col-xs-2">Title</div>
                                
                                <div class="col-xs-3">Description</div>
                                
                                <div class="col-xs-1">Created Date</div>
                               
                                <div class="col-xs-2">Action</div>
                            </li>
                            <?php $index=1;?>
                            @foreach($list as $row)
                            <li id="sortOrder_{!! $row->id !!}" class="sortable">
                                <?php 
                                    $file_path=$row->file_name!=''?asset($row->file_path.$row->file_name):asset(DEFAULT_THUMBNAIL);
                                ?>
                                <div class="col-xs-1"><span id="order-no">{!!$index!!}</span></div>
                                <div class="col-xs-2"><img src="{!!$file_path!!}" class="img-thumbnail" style="max-width:120px !important;height:60px;"></div>
                                <div class="col-xs-2">{!!$row->title!!}</div>
                                <div class="col-xs-3">{!!str_limit($row->description,20,"...")!!}</div>
                                
                               
                                <div class="col-xs-1">{!!date('Y-m-d',strtotime($row->created_at))!!}</div>
                                
                                <div class="col-xs-2">
                                   	<a href="{!!url(ADMIN_PREFIX.'/article/basic/'.$row->id)!!}" class="btn btn-xs btn-info">
                                    	<i class="fa fa-pencil"></i>
                                	</a>

                                	<a href="{!!url(ADMIN_PREFIX.'/article/destroy/'.$row->id)!!}" class="btn btn-xs btn-danger dialog" id="delete" onclick="return false;">
                                    	<i class="fa fa-trash-o"></i>
                                	</a>
                                    <br>


                                    @if($row->status==INACTIVE)
                                        <a href="{!!url(ADMIN_PREFIX.'/article/togglepublish/'.$row->id.'/'.ACTIVE)!!}" class="btn btn-xs btn-success dialog" id="publish" onclick="return false;">
                                            <i class="fa fa-upload"></i> Publish
                                        </a>
                                    @else
                                        <a href="{!!url(ADMIN_PREFIX.'/article/togglepublish/'.$row->id.'/'.INACTIVE)!!}" class="btn btn-xs btn-danger dialog" id="unpublish" onclick="return false;">
                                            <i class="fa fa-download"></i> UnPublish
                                        </a>
                                    @endif
                                    <br>


                                    @if($row->featured_status==ACTIVE)
                                        <a href="{!!url(ADMIN_PREFIX.'/article/toggleFeatured/'.$row->id."/".INACTIVE)!!}" class="btn btn-xs btn-warning dialog" id="remove_feature" onclick="return false;">
                                            <i class="fa fa-trash-o"></i> Remove Featured
                                        </a>
                                    @else
                                        <a href="{!!url(ADMIN_PREFIX.'/article/toggleFeatured/'.
                                        $row->id."/".ACTIVE)!!}" class="btn btn-xs btn-success dialog" id="set_feature" onclick="return false;">
                                        <i class="fa fa-check-circle-o"></i> Set Feature
                                        </a>
                                    @endif


                                </div>
                            </li>
                            <?php $index++;?>
                            @endforeach
                        </ul>
                    </table>
                    </div>
                    <div class="col-md-12 pull-right">
                        @if(isset($search) && $search)
                            &nbsp;
                        @else
                            {!!$list->render()!!}
                        @endif
                    </div>
                    @else
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> There is no Article yet.
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>

<script src="{!!asset('/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js')!!}"></script>

<link href="{!!asset('/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.css')!!}" rel="stylesheet">

	<script>
	$(document).on('click', '.dialog', function(e) {

	    var id = this.id;
	    var href = this.href;
	    var $this=$(this).parent().parent();

	    var message,colorCode,button,btnLabel;
	    var modalType;
	            // add dialog tab
	       if(id=="delete"){

	            message = "Are you sure? you want to remove this Article.?";
	            button = "btn btn-danger";
	            btnLabel = "Confirm";
	            modalType = 'type-danger';
	        
	        }else if(id=="publish"){

                message = "Are you sure? you want to publish this article?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="unpublish"){

                message = "Are you sure? you want to unpublish this article?";
                button = "btn btn-danger";
                btnLabel = "Confirm";
                modalType = 'type-danger';
            
            }else if(id=="set_feature"){

                message = "Are you sure? you want to set this article as featured ?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="remove_feature"){

                message = "Are you sure? you want to remove this article from featured ?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }

	        var dialog = new BootstrapDialog({

	            cssClass: modalType,
	            title: '<div id="dia-title">Confirmation</div>',
	            message: '<div id="dia-message">' + message + '</div>',
	            buttons: [{
	                label: 'Cancel',
	                cssClass: 'btn btn-default',
	                action: function(dialog) {
	                    dialog.close();
	                }
	            }, {
	                    label: btnLabel,
	                    cssClass: button,
	                    action: function(dialog) {
	                        $("#dia-message").text("Process is doing now.Please wait for a second");
	                        $('#dia-message').css('color', '#00');
	                        $('#dia-title').text("Wait..");
	                        $.ajax({
	                            type: 'GET',
	                            url: href,
	                            context: this,
	                            dataType: 'json',
	                            success: function(data) {
	                                if (data.success) {
	                                    dialog.close();

	                                    messagebar_toggle(data.message,'success',3000);

	                                    window.setTimeout(function(){location.reload()},2000);

	                                }else{
	                                    dialog.close();
	                                    messagebar_toggle(data.message,'warning',4000);
	                                }
	                            }
	                        });

	                    }
	                }
	                ],
	            closable: true
	        });
	        dialog.realize();
	        dialog.open();
	});


    $(function () {
                
            $_token = "{!! csrf_token() !!}";
            var $id =50;
            $("#sortable-wrapper ul").sortable({

                opacity: 0.6, cursor: 'move', update: function () {

                    var order = $(this).sortable("serialize") + '&_token=' + $_token;
                    $.post("article/sortFeatured", order, function (data) {

                        if (data.status) {

                            $index=1;

                            $("span#order-no").each(function(){

                                $(this).text($index);
                                $index++;

                            });

                            messagebar_toggle("Successfully ordered.",'success',3000);
                           
                        } else {

                            messagebar_toggle("Error! Please try again.",'warning',3000);
                           
                        }
                    });
                }
            });
        });

	</script>
@stop
