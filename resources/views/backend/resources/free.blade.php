@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Download <i class="fa fa-arrow-circle-right route-link"></i> Sub Category
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-6">
            <div class="panel">
                    
                    
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/download_subcategory/','class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">
                            

                            <div class="form-group">
                                <label  class="col-sm-4 control-label">Category<span class="mandatory">*</span></label>
                                <?php 
                                    $category=json_decode(ORDINARY_LINK,true);
                                ?>
                                <div class="col-sm-8">

                                     {!! Form::select('category_id',$category_list,isset($info)?$info->category_id:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Sub Category<span class="mandatory">*</span></label>
                                <div class="col-sm-8">

                                    {!! Form::text('sub_category',isset($info)?$info->sub_category:null, ['class'=>'form-control','placeholder'=>'Sub Category', 'required']) !!}
                                   
                                </div>  
                            </div>
                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <div class="panel">
                 @if(isset($list) && count($list))
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Main</th>
                                <th>Sub Category</th>
                                <th>Action</th>
                            </tr>
                            <?php 
                                $index=1;
                                
                            ?>
                            @foreach($list as $row)
                            <tr>    
                               
                                <td>{!!$index!!}</td>
                                
                                <td>{!!$category_list[$row->category_id]!!}</td>
                                <td>{!!$row->sub_category!!}</td>

                               

                                <td>
                                    <a href="{!!url(ADMIN_PREFIX.'/download_subcategory/'.$row->id)!!}" class="btn btn-xs btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <a href="{!!url(ADMIN_PREFIX.'/download_subcategory/destroy/'.$row->id)!!}" class="btn btn-xs btn-danger dialog" id="delete" onclick="return false;">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    
                                   

                                </td>
                            </tr>
                            <?php $index++;?>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    </section>

    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>
   
	<script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>
@stop
