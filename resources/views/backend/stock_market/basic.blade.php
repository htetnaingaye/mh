@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Stock Market <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->title:'New Stock Market'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Stock Market - ".$info->title:"New Stock Market"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/stock_market/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> StocK Market List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/stock_market/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">

                            

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date</label>
                                <div class="col-sm-5">

                                    {!! Form::text('date',isset($info)?$info->date:date('Y-m-d'), ['class'=>'form-control','placeholder'=>'3500','readonly']) !!}
                                   
                                </div>  
                            </div>


                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Stock Name<span class="mandatory">*</span></label>
                                <div class="col-sm-5">

                                     {!! Form::select('stock_id',$stock,isset($info)?$info->stock_id:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">11 Price</label>
                                <div class="col-sm-5">

                                    {!! Form::text('eleven_price',isset($info)?$info->eleven_price:null, ['class'=>'form-control','placeholder'=>'3500',]) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">11 Units</label>
                                <div class="col-sm-5">

                                    {!! Form::text('eleven_unit',isset($info)?$info->eleven_unit:null, ['class'=>'form-control','placeholder'=>'127',]) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">1:30 Price</label>
                                <div class="col-sm-5">

                                    {!! Form::text('one_price',isset($info)?$info->one_price:null, ['class'=>'form-control','placeholder'=>'3500',]) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">1:30 Units</label>
                                <div class="col-sm-5">

                                    {!! Form::text('one_unit',isset($info)?$info->one_unit:null, ['class'=>'form-control','placeholder'=>'127',]) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Different</label>
                                <div class="col-sm-5">

                                    {!! Form::text('different',isset($info)?$info->different:null, ['class'=>'form-control','placeholder'=>'+100',]) !!}
                                   
                                </div>  
                            </div>
                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>

    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>


    
@stop
