@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        School <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->name:'New School'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit School - ".$info->name:"New School"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/school/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> School List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/school/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    
                    <input type="hidden" value="{!!isset($info)?$info->id:0!!}" name="id">
                
                    
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('school_name',isset($info)?$info->name:null, ['class'=>'form-control','placeholder'=>'School Name', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Program Description<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('description',isset($info)?$info->description:null, ['class' => 'form-control','id'=>'editor']) !!}
                                    
                                </div>
                            </div>


                            <div class="form-group">
                                <label  class="col-sm-3 control-label">School Image (354px * 204px)</label>

                                <div class="col-sm-9">
                                    <!-- file input -->
                                    @if(isset($info) && $info->file_path!='')
                                        <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                        <input type="hidden" name="file_path" value="{!!$info->file_path!!}">
                                        <input type="hidden" name="file_name" value="{!!$info->file_name!!}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img src="{!!asset($info->file_path.$info->file_name)!!}"
                                                     id="edit_image">
                                            </div>
                                            <div>
                                              <span class="btn default btn-file">
                                              <span class="btn btn-xs btn-info fileinput-new">Change image </span>
                                              <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                          {!! Form::file('photo', array('accept'=>"image/*"))!!}
                                              </span>
                                                <a href="javascript:;" class="btn btn-xs btn-danger red fileinput-exists"
                                                   data-dismiss="fileinput" id="edit_remove">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                 <img src="{!!asset('/backend/img/s-thumbnail.png')!!}" class="img-responsive img-lthumb">
                                            </div>
                                            <div>
                                                
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-info btn-xs fileinput-new">Select image </span>
                                                  <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                        {!! Form::file('photo', array('accept'=>'image/*','required'=>'true'))!!}
                                                  </span>
                                                <a href="javascript:;" class="btn btn-danger btn-xs fileinput-exists"
                                                   data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Start Date<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('start_date',isset($info)?$info->start_date:null, ['class'=>'form-control','placeholder'=>'Start Date', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Duration<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('duration',isset($info)?$info->duration:null, ['class'=>'form-control','placeholder'=>'Duration', 'required']) !!}
                                   
                                </div>  
                            </div>

                             <div class="form-group">
                                <label class="col-sm-3 control-label">Price<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('price',isset($info)?$info->price:null, ['class'=>'form-control','placeholder'=>'Price', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Course<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('course',isset($info)?$info->course:null, ['class'=>'form-control','placeholder'=>'ACCA,LCCI LEVE II,LCCI Level III', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Township<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('township',isset($info)?$info->township:null, ['class'=>'form-control','placeholder'=>'Township', 'required']) !!}
                                   
                                </div>  
                            </div>
                            <div class="form-group">
                            <label class="col-sm-3 control-label">Map</label>
                             <div class="col-sm-9">
                             <input id="geocomplete" name="map_place" class="form-control" value="{!!isset($info)?$info->map_place:''!!}" type="text" placeholder="Type in school place" size="90" />

                                

                                <input name="lat" type="hidden" value="{!!isset($info)?$info->lat:''!!}">
                                <input name="lng" type="hidden" value="{!!isset($info)?$info->lng:''!!}">

                                <div id="map_canvas" class="map_canvas mt10"></div>
                            </div>

                            </div>


                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>

    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>
    <script>
	    $(function(){

	       CKEDITOR.replace('editor',{             
                                                                 
            });

            $('.select2').select2({
               
            });

	    });
	</script>
	<script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>

    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB1orum3qYQkkbAgpmbLV4wPvIREHSDb14"></script>



<script src="{!!asset('backend/js/jquery.geocomplete.js')!!}"></script>

    <style type="text/css">
    #map_canvas{
        width:100%;
        height:300px;
    }
</style>



 <script type="text/javascript">

        $(document).ready(function() {


            
            /* map  */
            var lat ="{!!isset($info)?$info->lat:'16.798801'!!}";
            var lng = "{!!isset($info)?$info->lng:'96.148743'!!}";


            var center = new google.maps.LatLng(lat, lng);
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form ",
                location: center,
                markerOptions: {
                    draggable: true
                },
                mapOptions: {
                    zoom: 10
                }
            });

            $("#geocomplete").bind("geocode:dragged", function(event, latLng) {
                $("input[name='lat']").val(latLng.lat());
                $("input[name='lng']").val(latLng.lng());
                $("#reset").show();
                // $(".map-button-panel").fadeIn();
                $(".map-locate").addClass('dni');
                $(".map-cancel").removeClass('dn');
                $(".map-submit").removeClass('dn');

            });


            $("#reset").click(function() {
                $("#geocomplete").geocomplete("resetMarker");
                $("#reset").hide();
                return false;
            });

            $("#find").click(function() {
                $("#geocomplete").trigger("geocode");
            }).click();

            $("#geocomplete")
                .geocomplete()
                .bind("geocode:result", function(event, result) {
                    $(".map-locate").addClass('dni');
                    $(".map-cancel").removeClass('dn');
                    $(".map-submit").removeClass('dn');
                });
        });
    </script>
@stop
