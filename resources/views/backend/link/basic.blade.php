@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Link <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->title:'New Link'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Link - ".$info->title:"New Link"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/link/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Useful Link List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/link/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">

                             <div class="form-group">
                                <label  class="col-sm-3 control-label">Category<span class="mandatory">*</span></label>

                                <?php 
                                    $category=json_decode(WEBSITE_LINK,true);
                                ?>
                                <div class="col-sm-9">



                                     {!! Form::select('category_id',$category,isset($info)?$info->category_id:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('title',isset($info)?$info->title:null, ['class'=>'form-control','placeholder'=>'Link Title', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Url<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('url',isset($info)?$info->url:null, ['class'=>'form-control','placeholder'=>'http://www.google.com', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Logo</label>
                                <div class="col-sm-9">
                                    <!-- file input -->
                                    @if(isset($info) && $info->file_path!='')
                                        <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                        <input type="hidden" name="file_path" value="{!!$info->file_path!!}">
                                        <input type="hidden" name="file_name" value="{!!$info->file_name!!}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img src="{!!asset($info->file_path.$info->file_name)!!}"
                                                     id="edit_image">
                                            </div>
                                            <div>
                                              <span class="btn default btn-file">
                                              <span class="btn btn-xs btn-info fileinput-new">Change image </span>
                                              <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                          {!! Form::file('photo', array('accept'=>"image/*"))!!}
                                              </span>
                                                <a href="javascript:;" class="btn btn-xs btn-danger red fileinput-exists"
                                                   data-dismiss="fileinput" id="edit_remove">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                 <img src="{!!asset('/backend/img/s-thumbnail.png')!!}" class="img-responsive img-lthumb">
                                            </div>
                                            <div>
                                                
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-info btn-xs fileinput-new">Select image </span>
                                                  <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                        {!! Form::file('photo', array('accept'=>'image/*','required'=>'true'))!!}
                                                  </span>
                                                <a href="javascript:;" class="btn btn-danger btn-xs fileinput-exists"
                                                   data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            


                            

                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>


    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>
    <script>
        $(function(){

           CKEDITOR.replace('editor',{             
                
            });

            $('.select2').select2({
               
            });

        });
    </script>

	
@stop
