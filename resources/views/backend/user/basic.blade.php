@extends('backend.layout.template')
@section('content')
		
	 <div class="box-header">
        <i class="fa fa fa-th"></i>
        <div class="pull-right box-tools">
            <a href="{!!url(ADMIN_PREFIX.'/tour/')!!}" class="btn btn-success btn-sm"><i class="fa fa-list"></i></a>
        </div>
    </div>


	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				@include('backend.tour.tour_menu')
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-1">
				{!! Form::open(array('url' =>ADMIN_PREFIX.'/tour/basic/'.$id,'class'=>'form-horizontal frmBasic','files'=>true,'method'=>'post')) !!}
                    
                {!! Form::hidden('id',isset($info)?$info->id:0)!!}

                <div class="panel">
                	<div class="panel-body">

		                <div class="form-group">
		                    <label class="col-sm-3 control-label">Name<span class="mandatory">*</span></label>
		                    <div class="col-sm-9">

		                        {!! Form::text('name',isset($info)?$info->name:null, ['class'=>'form-control','placeholder'=>'Tour Name', 'required']) !!}
		                       
		                    </div>  
		                </div>

		                <div class="form-group">
                            <label  class="col-sm-3 control-label">Destination<span class="mandatory">*</span></label>
                            <div class="col-sm-9">
                                {!! Form::select('destination_id',$dest_list,isset($info)?$info->destination_id:null,['class' => 'form-control select2']) !!}
                            </div>
                         </div>



	                    <div class="form-group">
	                        <label  class="col-sm-3 control-label">Short Desc<span class="mandatory">*</span></label>
	                        <div class="col-sm-9">

	                            {!! Form::textarea('short_description',isset($info)?$info->short_description:null, ['class' => 'form-control']) !!}
	                            
	                        </div>
	                    </div>


	                    <div class="form-group">
	                        <label  class="col-sm-3 control-label">Description<span class="mandatory">*</span></label>
	                        <div class="col-sm-9">

	                            {!! Form::textarea('description',isset($info)?$info->description:null, ['class' => 'form-control','id'=>'editor']) !!}
	                            
	                        </div>
	                    </div>

	                    <div class="box-footer">
	                        <div class="form-group">
	                            <div class="col-sm-offset-3 col-sm-9">
	                                <a href="{!!url(ADMIN_PREFIX.'/tour')!!}" class="btn btn-default">Cancel</a>
	                                <button type="submit" class="btn btn-primary">
	                                    Save && Continue
	                                </button>
	                            </div>
	                        </div>
	                    </div>

                    </div>


                </div>

                {!!Form::close()!!}
			</div>
		</div>
	</div>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/ckeditor/ckeditor.js") }}"></script>
	<script type="text/javascript">
		$(function(){
			CKEDITOR.replace('editor');
		});
	</script>
	
@stop