@extends('backend.layout.template')
@section('content')

    <div class="row">
        <div class="col-md-12">
            @include('flash::message')

        </div>
    </div>

    <div class="box-header">
        <i class="fa fa fa-th"></i>
        <div class="pull-right box-tools">
            <a href="{!!url(ADMIN_PREFIX.'/user/basic/0')!!}" class="btn btn-success btn-sm"title="Add New Tour"><i class="fa fa-plus-circle"></i></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">


        
            <div class="panel panel-default">
                
                <div class="clearfix"></div>
                <div class="panel-body">
                    @if(isset($list) && count($list))
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Desc</th>
                                <th>Created Date</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                            <?php $index=1;?>
                            @foreach($list as $row)
                            <tr>    
                                
                                <td>{!!$index!!}</td>
                                <td>{!!$row->email!!}</td>
                                <td>{!!$row->name!!}</td>
                                <td>{!!$row->facebook_id!!}</td>
                                <td>{!!date('Y-m-d',strtotime($row->created_at))!!}</td>
                                <td></td>

                                <td>
                                    <a href="{!!url(ADMIN_PREFIX.'/user/'.$row->id)!!}" class="btn btn-xs btn-info" title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <a href="{!!url(ADMIN_PREFIX.'/user/'.$row->id)!!}" class="btn btn-xs btn-danger dialog" id="delete" onclick="return false;" title="Delete">
                                        <i class="fa fa-trash-o"></i>
                                    </a>

                                    <br>
                                    
                                    @if($row->status==INACTIVE)
                                        <a href="{!!url(ADMIN_PREFIX.'/user/togglepublish/'.$row->id.'/'.ACTIVE)!!}" class="btn btn-xs btn-success dialog" id="publish" onclick="return false;" title="Publish">
                                            <i class="fa fa-upload"></i>
                                        </a>
                                    @else
                                        <a href="{!!url(ADMIN_PREFIX.'/user/togglepublish/'.$row->id.'/'.INACTIVE)!!}" class="btn btn-xs btn-danger dialog" id="unpublish" onclick="return false;" id="UnPublish">
                                            <i class="fa fa-download"></i>
                                        </a>
                                    @endif


                                    @if($row->featured_status==ACTIVE)
                                        <a href="{!!url(ADMIN_PREFIX.'/user/toggleFeatured/'.$row->id."/".INACTIVE)!!}" class="btn btn-xs btn-warning dialog" id="remove_feature" onclick="return false;">
                                            <i class="fa fa-trash-o"></i> Remove Featured
                                        </a>
                                    @else
                                        <a href="{!!url(ADMIN_PREFIX.'/user/toggleFeatured/'.
                                        $row->id."/".ACTIVE)!!}" class="btn btn-xs btn-success dialog" id="set_feature" onclick="return false;">
                                        <i class="fa fa-check-circle-o"></i> Set Feature
                                        </a>
                                    @endif


                                </td>
                            </tr>
                            <?php $index++;?>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-md-12 pull-right">
                        @if(isset($search) && $search)
                            &nbsp;
                        @else
                            {!!$list->render()!!}
                        @endif
                    </div>
                    @else
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> There is no record yet.
                    </div>
                    @endif
                </div>
            </div>
        

    
        </div>
    </div>

    <script>
    $(document).on('click', '.dialog', function(e) {

        var id = this.id;
        var href = this.href;
        var $this=$(this).parent().parent();

        var message,colorCode,button,btnLabel;
        var modalType;
                // add dialog tab
            if(id=="publish"){

                message = "Are you sure? you want to publish this Tour?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="unpublish"){

                message = "Are you sure? you want to unpublish this Tour?";
                button = "btn btn-danger";
                btnLabel = "Confirm";
                modalType = 'type-danger';
            
            }else if(id=="set_feature"){

                message = "Are you sure? you want to set this Tour as featured?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="remove_feature"){

                message = "Are you sure? you want to remove this Tour from featured?";
                button = "btn btn-warning";
                btnLabel = "Confirm";
                modalType = 'type-warning';
            
            }else if(id=="delete"){

                message = "Are you sure? you want to remove this Tour?";
                button = "btn btn-danger";
                btnLabel = "Confirm";
                modalType = 'type-danger';
            
            }

            var dialog = new BootstrapDialog({

                cssClass: modalType,
                title: '<div id="dia-title">Confirmation</div>',
                message: '<div id="dia-message">' + message + '</div>',
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn btn-default',
                    action: function(dialog) {
                        dialog.close();
                    }
                }, {
                        label: btnLabel,
                        cssClass: button,
                        action: function(dialog) {
                            $("#dia-message").text("Process is doing now.Please wait for a second");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                context: this,
                                dataType: 'json',
                                success: function(data) {
                                    if (data.success) {
                                        dialog.close();

                                        messagebar_toggle(data.message,'success',3000);

                                        window.setTimeout(function(){location.reload()},2000);

                                    }else{
                                        dialog.close();
                                        messagebar_toggle(data.message,'warning',4000);
                                    }
                                }
                            });

                        }
                    }
                    ],
                closable: true
            });
            dialog.realize();
            dialog.open();
    });

    </script>

@stop