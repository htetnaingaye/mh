@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Event <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->name:'New Event'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Event - ".$info->name:"New Event"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url('admin/event/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Event List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/event/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Event Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('title',isset($info)?$info->title:null, ['class'=>'form-control','placeholder'=>'Event Title', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Start Date<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('start_date',isset($info)?$info->start_date:null, ['class'=>'form-control','placeholder'=>'2016-11-09 12:00:00', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">End Date<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('end_date',isset($info)?$info->end_date:null, ['class'=>'form-control','placeholder'=>'2016-11-09 12:00:00', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Time<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('time',isset($info)?$info->time:null, ['class'=>'form-control','placeholder'=>'Time', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Description<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('description',isset($info)?$info->description:null, ['class' => 'form-control','id'=>'editor']) !!}
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Category<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                     {!! Form::select('category_id',$category,isset($info)?$info->category_id:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Image</label>
                                <div class="col-sm-9">
                                    <!-- file input -->
                                    @if(isset($info) && $info->file_path!='')
                                        <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                        <input type="hidden" name="file_path" value="{!!$info->file_path!!}">
                                        <input type="hidden" name="file_name" value="{!!$info->file_name!!}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img src="{!!asset($info->file_path.$info->file_name)!!}"
                                                     id="edit_image">
                                            </div>
                                            <div>
                                              <span class="btn default btn-file">
                                              <span class="btn btn-xs btn-info fileinput-new">Change image </span>
                                              <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                          {!! Form::file('photo', array('accept'=>"image/*"))!!}
                                              </span>
                                                <a href="javascript:;" class="btn btn-xs btn-danger red fileinput-exists"
                                                   data-dismiss="fileinput" id="edit_remove">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                 <img src="{!!asset('/backend/img/s-thumbnail.png')!!}" class="img-responsive img-lthumb">
                                            </div>
                                            <div>
                                                
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-info btn-xs fileinput-new">Select image </span>
                                                  <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                        {!! Form::file('photo', array('accept'=>'image/*','required'=>'true'))!!}
                                                  </span>
                                                <a href="javascript:;" class="btn btn-danger btn-xs fileinput-exists"
                                                   data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Url</label>
                                <div class="col-sm-9">

                                    {!! Form::text('url',isset($info)?$info->url:null, ['class'=>'form-control','placeholder'=>'URL']) !!}
                                   
                                </div>  
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Location</label>
                                <div class="col-sm-9">

                                    {!! Form::text('location',isset($info)?$info->location:null, ['class'=>'form-control','placeholder'=>'Location']) !!}
                                   
                                </div>  
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Organizer Name</label>
                                <div class="col-sm-9">

                                    {!! Form::text('organizer_name',isset($info)?$info->organizer_name:null, ['class'=>'form-control','placeholder'=>'Organizer Name']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Organizer Email</label>
                                <div class="col-sm-9">

                                    {!! Form::text('organizer_email',isset($info)?$info->organizer_email:null, ['class'=>'form-control','placeholder'=>'Organizer Email']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Paid Type<span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label class="checkbox-inline"><input name="paid_status" value="1" type="radio" {!!isset($info) && $info->paid_status==1?'checked':''!!}>Free</label>
                                        <label class="checkbox-inline"><input type="radio" name="paid_status" value="2" {!!isset($info) && $info->paid_status==2    ?'checked':''!!}>Paid</label>
                                    </div>
                                    
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Fee</label>
                                <div class="col-sm-9">

                                    {!! Form::text('fee',isset($info)?$info->fee:null, ['class'=>'form-control','placeholder'=>'10000 Ks']) !!}
                                   
                                </div>  
                            </div>



                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>

    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>
    <script>
	    $(function(){

	       CKEDITOR.replace('editor',{             
                                                     
            });

            $('.select2').select2({
               
            });

	    });
	</script>
	<script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>
@stop
