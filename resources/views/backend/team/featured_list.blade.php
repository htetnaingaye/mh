@extends('backend.layout.template')
@section('content')

<section class="content-header">
    <h1>
        Team List
    </h1>
    
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@include('flash::message')
		</div>

	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box-header">
                    <i class="fa fa fa-th"></i>
                    <div class="pull-right box-tools">
                        <a href="{!!url(ADMIN_PREFIX.'/team/basic/0')!!}" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Add New Team</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="panel-body">
                    @if(isset($list) && count($list))

                    <div class="sortable-wrapper" id="sortable-wrapper">
                    <table class="table table-striped">
                        <ul class="table">
                            <li>
                                <div class="col-xs-1">#</div>
                                <div class="col-xs-2">Image</div>
                                <div class="col-xs-2">Type</div>
                                <div class="col-xs-2">Name</div>
                                
                                <div class="col-xs-2">Position</div>
                                
                                <div class="col-xs-1">Created Date</div>
                               
                                <div class="col-xs-2">Action</div>
                            </li>
                            <?php 
                                $index=1;
                                $type=json_decode(TEAM_TYPE,true);
                            ?>
                            @foreach($list as $row)
                            <li id="sortOrder_{!! $row->id !!}" class="sortable">
                                <?php 
                                    $file_path=$row->file_name!=''?asset($row->file_path.$row->file_name):asset(DEFAULT_THUMBNAIL);
                                ?>
                                <div class="col-xs-1"><span id="order-no">{!!$index!!}</span></div>
                                <div class="col-xs-2"><img src="{!!$file_path!!}" class="img-thumbnail" style="max-width:120px !important;height:60px;"></div>
                                <div class="col-md-2">
                                    {!!isset($row->type) && $row->type!=''?$type[$row->type]:''!!}
                                </div>
                                <div class="col-xs-2">{!!$row->name!!}</div>
                                <div class="col-xs-2">{!!$row->position!!}</div>
                                
                               
                                <div class="col-xs-1">{!!date('Y-m-d',strtotime($row->created_at))!!}</div>
                                
                                <div class="col-xs-2">
                                    <a href="{!!url(ADMIN_PREFIX.'/team/basic/'.$row->id)!!}" class="btn btn-xs btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <a href="{!!url(ADMIN_PREFIX.'/team/destroy/'.$row->id)!!}" class="btn btn-xs btn-danger dialog" id="delete" onclick="return false;">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </div>
                            </li>
                            <?php $index++;?>
                            @endforeach
                        </ul>
                    </table>
                    </div>
                    <div class="col-md-12 pull-right">
                        @if(isset($search) && $search)
                            &nbsp;
                        @else
                            {!!$list->render()!!}
                        @endif
                    </div>
                    @else
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> There is no Article yet.
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>

<script src="{!!asset('public/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js')!!}"></script>

<link href="{!!asset('public/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.css')!!}" rel="stylesheet">

	<script>
	$(document).on('click', '.dialog', function(e) {

	    var id = this.id;
	    var href = this.href;
	    var $this=$(this).parent().parent();

	    var message,colorCode,button,btnLabel;
	    var modalType;
	            // add dialog tab
	       if(id=="delete"){

	            message = "Are you sure? you want to remove this Article.?";
	            button = "btn btn-danger";
	            btnLabel = "Confirm";
	            modalType = 'type-danger';
	        
	        }else if(id=="publish"){

                message = "Are you sure? you want to publish this article?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="unpublish"){

                message = "Are you sure? you want to unpublish this article?";
                button = "btn btn-danger";
                btnLabel = "Confirm";
                modalType = 'type-danger';
            
            }else if(id=="set_feature"){

                message = "Are you sure? you want to set this record as featured ?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }else if(id=="remove_feature"){

                message = "Are you sure? you want to remove this record from featured ?";
                button = "btn btn-success";
                btnLabel = "Confirm";
                modalType = 'type-success';
            
            }

	        var dialog = new BootstrapDialog({

	            cssClass: modalType,
	            title: '<div id="dia-title">Confirmation</div>',
	            message: '<div id="dia-message">' + message + '</div>',
	            buttons: [{
	                label: 'Cancel',
	                cssClass: 'btn btn-default',
	                action: function(dialog) {
	                    dialog.close();
	                }
	            }, {
	                    label: btnLabel,
	                    cssClass: button,
	                    action: function(dialog) {
	                        $("#dia-message").text("Process is doing now.Please wait for a second");
	                        $('#dia-message').css('color', '#00');
	                        $('#dia-title').text("Wait..");
	                        $.ajax({
	                            type: 'GET',
	                            url: href,
	                            context: this,
	                            dataType: 'json',
	                            success: function(data) {
	                                if (data.success) {
	                                    dialog.close();

	                                    messagebar_toggle(data.message,'success',3000);

	                                    window.setTimeout(function(){location.reload()},2000);

	                                }else{
	                                    dialog.close();
	                                    messagebar_toggle(data.message,'warning',4000);
	                                }
	                            }
	                        });

	                    }
	                }
	                ],
	            closable: true
	        });
	        dialog.realize();
	        dialog.open();
	});


    $(function () {
                
            $_token = "{!! csrf_token() !!}";
            var $id =50;
            $("#sortable-wrapper ul").sortable({

                opacity: 0.6, cursor: 'move', update: function () {

                    var order = $(this).sortable("serialize") + '&_token=' + $_token;
                    $.post("team/sortFeatured", order, function (data) {

                        if (data.status) {

                            $index=1;

                            $("span#order-no").each(function(){

                                $(this).text($index);
                                $index++;

                            });

                            messagebar_toggle("Successfully ordered.",'success',3000);
                           
                        } else {

                            messagebar_toggle("Error! Please try again.",'warning',3000);
                           
                        }
                    });
                }
            });
        });

	</script>
@stop
