@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Download <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->name:'New Download'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Article - ".$info->title:"New Download"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/download/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Download List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/download/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('title',isset($info)?$info->title:null, ['class'=>'form-control','placeholder'=>'Download Title', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Type<span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label class="checkbox-inline"><input name="type" value="1" type="radio" {!!isset($info) && $info->type==1?'checked':''!!}>Free</label>
                                        <label class="checkbox-inline"><input type="radio" name="type" value="2" {!!isset($info) && $info->type==2?'checked':''!!}>Premium</label>
                                    </div>
                                    
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Category<span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <?php 
                                        $category=App\Models\DownloadCategory::get();
                                    ?>

                                     <select class="form-control select2" name="category" >
                                         @foreach($category as $r)
                                         <optgroup label="{!!$r->namepdo!!}">
                                            <?php 
                                            $list=App\Models\Resource::where('category_id',$r->id)->get();
                                            ?>

                                            @if(count($list))
                                                @foreach($list as $row)
                                                    <option value="{!!$row->id!!}" {!!isset($info) && $info->category==$row->id?'selected':''!!}>{!!$row->sub_category!!}</option>
                                                @endforeach
                                            @endif
                                             
                                         </optgroup>
                                         @endforeach
                                     </select>
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Description<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('description',isset($info)?$info->description:null, ['class' => 'form-control','id'=>'editor']) !!}
                                    
                                </div>
                            </div>

                            

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Cover Image <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <!-- file input -->
                                    @if(isset($info) && $info->cover_file_path!='')
                                        <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                        <input type="hidden" name="file_path" value="{!!$info->cover_file_path!!}">
                                        <input type="hidden" name="file_name" value="{!!$info->cover_file_name!!}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img src="{!!asset($info->cover_file_path.$info->cover_file_name)!!}"
                                                     id="edit_image">
                                            </div>
                                            <div>
                                              <span class="btn default btn-file">
                                              <span class="btn btn-xs btn-info fileinput-new">Change Image </span>
                                              <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                          {!! Form::file('cover_photo')!!}
                                              </span>
                                                <a href="javascript:;" class="btn btn-xs btn-danger red fileinput-exists"
                                                   data-dismiss="fileinput" id="edit_remove">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                 <img src="{!!asset('/backend/img/s-thumbnail.png')!!}" class="img-responsive img-lthumb">
                                            </div>
                                            <div>
                                                
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-info btn-xs fileinput-new">Select Image </span>
                                                  <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                        {!! Form::file('cover_photo', array('required'=>'true'))!!}
                                                  </span>
                                                <a href="javascript:;" class="btn btn-danger btn-xs fileinput-exists"
                                                   data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">File <span class="mandatory">*</span></label>
                                <div class="col-sm-9">
                                    <!-- file input -->
                                    @if(isset($info) && $info->file_path!='')
                                        <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                        <input type="hidden" name="file_path" value="{!!$info->file_path!!}">
                                        <input type="hidden" name="file_name" value="{!!$info->file_name!!}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                <img src="{!!asset($info->file_path.$info->file_name)!!}"
                                                     id="edit_image">
                                            </div>
                                            <div>
                                              <span class="btn default btn-file">
                                              <span class="btn btn-xs btn-info fileinput-new">Change File </span>
                                              <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                          {!! Form::file('photo')!!}
                                              </span>
                                                <a href="javascript:;" class="btn btn-xs btn-danger red fileinput-exists"
                                                   data-dismiss="fileinput" id="edit_remove">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                 style="width: 200px; height: 150px;">
                                                 <img src="{!!asset('/backend/img/s-thumbnail.png')!!}" class="img-responsive img-lthumb">
                                            </div>
                                            <div>
                                                
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-info btn-xs fileinput-new">Select File </span>
                                                  <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                        {!! Form::file('photo', array('required'=>'true'))!!}
                                                  </span>
                                                <a href="javascript:;" class="btn btn-danger btn-xs fileinput-exists"
                                                   data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>
    <style type="text/css">
        .select2-container .select2-selection--single{
            height:35px !important
        }
    </style>

    <link rel="stylesheet"  href="{!! asset('/backend/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/backend/js/bootstrap-fileinput.js')!!}"></script>
    <script>
	    $(function(){

	       CKEDITOR.replace('editor',{             
                
            });

            $('.select2').select2({
               
            });

	    });
	</script>
	<script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>
@stop
