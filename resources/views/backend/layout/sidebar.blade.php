<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/bower_components/admin-lte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{!!Auth::user()->name!!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
    
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->


            <li class="treeview {!!isset($parent) && $parent=='news'?'active':''!!}">
                <a href="#"><span>News</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='new-category'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/new-category/')!!}">Category</a>
                    </li>
                    <li class="{!!isset($active) && $active=='new'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/news')!!}"><span>News</span></a>
                    </li>
                    

                </ul>
            </li>
            

            <li class="treeview {!!isset($parent) && $parent=='article'?'active':''!!}">
                <a href="#"><span>Articles</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='category'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/category/')!!}">Category</a></li>
                    <li class="{!!isset($active) && $active=='new-category'?'articles':''!!}"><a href="{!!url(ADMIN_PREFIX.'/article')!!}"><span>Articles</span></a></li>
                    <li class="{!!isset($active) && $active=='featurd_articles'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/featured-articles')!!}"><span>Featured Articles</span></a></li>
                    

                </ul>
            </li>
            

            <li class="treeview {!!isset($parent) && $parent=='event'?'active':''!!}">
                <a href="#"><span>Events</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='event_category'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/event_category/')!!}">Category</a></li>
                    <li class="{!!isset($active) && $active=='events'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/event')!!}">Events</a></li>
                    

                </ul>
            </li>

            <li class="treeview {!!isset($parent) && $parent=='download'?'active':''!!}">
                <a href="#"><span>Download</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='download_category'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/download_category/')!!}">Category</a></li>
                    <li><a href="{!!url(ADMIN_PREFIX.'/download_subcategory')!!}">Sub Category</a></li>
                    <li class="{!!isset($active) && $active=='downloads'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/download')!!}">Downloads</a></li>

                </ul>
            </li>

            <li class="treeview {!!isset($parent) && $parent=='stock'?'active':''!!}">
                <a href="#"><span>Market Data</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='stock'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/stock')!!}"><span>Stock</span></a>
                    </li>
                    <li class="{!!isset($active) && $active=='stock_market'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/stock_market')!!}"><span>Stock Market</span></a>
                    </li>
                    
                    <li class="{!!isset($active) && $active=='stock_photo'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/stock_photo')!!}"><span>Stock Photo</span></a>
                    </li>    

                    <li class="{!!isset($active) && $active=='procedure'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/procedure')!!}"><span>Tax Procedure</span></a>
                    </li>                

                </ul>
            </li>

            <li class="treeview {!!isset($parent) && $parent=='about_us'?'active':''!!}">
                <a href="#"><span>About Us</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li class="{!!isset($active) && $active=='service'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/service')!!}"><span>Services</span></a>
                    </li>
                    <li class="{!!isset($active) && $active=='featured_service'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/feature-service')!!}"><span>Featured Services</span></a>
                    </li>

                    <li class="{!!isset($active) && $active=='team'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/team')!!}"><span>Team</span></a>
                    </li>
                </ul>
            </li>

             <li class="treeview {!!isset($parent) && $parent=='job'?'active':''!!}">
                <a href="#"><span>Jobs</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                        
                    <li class="{!!isset($active) && $active=='job_category'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/job_category')!!}"><span>Category</span></a>
                    </li>

                    <li class="{!!isset($active) && $active=='jobs'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/job')!!}"><span>Jobs</span></a>
                    </li>

                    <li class="{!!isset($active) && $active=='featured_job'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/featured-job')!!}"><span>Featured Jobs</span></a>
                    </li>
                             

                </ul>
            </li>

            <li class="treeview {!!isset($parent) && $parent=='service'?'active':''!!}">
                <a href="#"><span>Links</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                        
                    <li class="{!!isset($active) && $active=='link'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/link')!!}"><span>Website Links</span></a>
                    </li>

                    <li class="{!!isset($active) && $active=='school'?'active':''!!}">
                        <a href="{!!url(ADMIN_PREFIX.'/school')!!}"><span>Traingin Schools</span></a>
                    </li>
                </ul>
            </li>

             <li class="{!!isset($parent) && $parent=='client'?'active':''!!}"><a href="{!!url(ADMIN_PREFIX.'/client')!!}">Clients</a></li>

            

            <li class="treeview">
                <a href="#"><span>Setting</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    <li><a href="{!!url(ADMIN_PREFIX.'/contact-message')!!}">Contact Message</a></li>

                    <li><a href="{!!url(ADMIN_PREFIX.'/user')!!}">Users</a></li>

                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Channge Password</a></li>
                    <li><a href="{!!url('logout')!!}">Logout</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>