<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright © {!!date('Y-m-d')!!} <a href="#">Company</a>.</strong> All rights reserved.
</footer>

<?php
	if (isset($data['jstoload']) && is_array($data['jstoload']) && count($data['jstoload'])){
	$jstoload = $data['jstoload'];
	foreach ($jstoload as $jsview){
?>
		<script src="{{asset('/backend/js/'.$jsview.'.js') }}"></script>
<?php
	}
}
?>