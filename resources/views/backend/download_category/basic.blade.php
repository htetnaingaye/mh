@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Download Category <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->name:'New Category'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Download Category - ".$info->name:"New Category"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url('admin/category/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Category List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>'admin/download_category/basic/'.$id,'class'=>'form-horizontal frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}

                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">


                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Download Type<span class="mandatory">*</span></label>
                                <?php 
                                    $category=json_decode(DOWNLOAD_TYPE,true);
                                ?>
                                <div class="col-sm-9">

                                     {!! Form::select('type',$category,isset($info)?$info->type:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('name',isset($info)?$info->name:null, ['class'=>'form-control','placeholder'=>'Download Category', 'required']) !!}
                                   
                                </div>  
                            </div>
                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!url('admin/download_category')!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@stop
