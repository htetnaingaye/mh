@extends('backend.layout.template')
@section('content')

<section class="content-header">
    <h1>
        Download Category List
    </h1>
    
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@include('flash::message')
		</div>

        

	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box-header">
                    <i class="fa fa fa-th"></i>
                    <div class="pull-right box-tools">
                        <a href="{!!url('admin/download_category/basic/0')!!}" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Add New Category</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="panel-body">
                    @if(isset($list) && count($list))
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Created Date</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                            <?php 
                            	$index=1;
                            	$type=json_decode(DOWNLOAD_TYPE,true);

                            ?>

                            @foreach($list as $row)
                            <tr>
                                
                                <td>{!!$index!!}</td>
                                <td>{!!isset($type[$row->type])?$type[$row->type]:''!!}</td>
                                <td>{!!$row->name!!}</td>
                               
                                <td>{!!date('Y-m-d',strtotime($row->created_at))!!}</td>
                                <td>{!!$row->user->name!!}</td>

                                <td>
                                   	<a href="{!!url('admin/download_category/basic/'.$row->id)!!}" class="btn btn-xs btn-info">
                                    	<i class="fa fa-pencil"></i>
                                	</a>

                                	<a href="{!!url('admin/download_category/destroy/'.$row->id)!!}" class="btn btn-xs btn-danger dialog" id="delete" onclick="return false;">
                                    	<i class="fa fa-trash-o"></i>
                                	</a>

                                </td>
                            </tr>
                            <?php $index++;?>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-md-12 pull-right">
                        @if(isset($search) && $search)
                            &nbsp;
                        @else
                            {!!$list->render()!!}
                        @endif
                    </div>
                    @else
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> There is no Categry yet.
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>

	<script>
	$(document).on('click', '.dialog', function(e) {

	    var id = this.id;
	    var href = this.href;
	    var $this=$(this).parent().parent();

	    var message,colorCode,button,btnLabel;
	    var modalType;
	            // add dialog tab
	       if(id=="delete"){

	            message = "Are you sure? you want to remove this Record?";
	            button = "btn btn-danger";
	            btnLabel = "Confirm";
	            modalType = 'type-danger';
	        
	        }

	        var dialog = new BootstrapDialog({

	            cssClass: modalType,
	            title: '<div id="dia-title">Confirmation</div>',
	            message: '<div id="dia-message">' + message + '</div>',
	            buttons: [{
	                label: 'Cancel',
	                cssClass: 'btn btn-default',
	                action: function(dialog) {
	                    dialog.close();
	                }
	            }, {
	                    label: btnLabel,
	                    cssClass: button,
	                    action: function(dialog) {
	                        $("#dia-message").text("Process is doing now.Please wait for a second");
	                        $('#dia-message').css('color', '#00');
	                        $('#dia-title').text("Wait..");
	                        $.ajax({
	                            type: 'GET',
	                            url: href,
	                            context: this,
	                            dataType: 'json',
	                            success: function(data) {
	                                if (data.success) {
	                                    dialog.close();

	                                    messagebar_toggle(data.message,'success',3000);

	                                    window.setTimeout(function(){location.reload()},2000);

	                                }else{
	                                    dialog.close();
	                                    messagebar_toggle(data.message,'warning',4000);
	                                }
	                            }
	                        });

	                    }
	                }
	                ],
	            closable: true
	        });
	        dialog.realize();
	        dialog.open();
	});

	</script>
@stop
