@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Job <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->title:'New Job'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Job - ".$info->title:"New Job"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url(ADMIN_PREFIX.'/job/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Job List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>ADMIN_PREFIX.'/job/basic/'.$id,'class'=>'form-horizontal  frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company Name<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('company',isset($info)?$info->company:null, ['class'=>'form-control','placeholder'=>'ABC Co.ltd.', 'required']) !!}
                                   
                                </div>  
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('title',isset($info)?$info->title:null, ['class'=>'form-control','placeholder'=>'', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Category<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                     {!! Form::select('category_id',$category,isset($info)?$info->category_id:null,['class' => 'form-control select2']) !!}
                                   
                                </div> 
                                
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Responsibility<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('responsibility',isset($info)?$info->responsibility:null, ['class' => 'form-control','id'=>'editor']) !!}
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Requirements<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('requirements',isset($info)?$info->requirements:null, ['class' => 'form-control','id'=>'editor_1']) !!}
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Salary<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('salary',isset($info)?$info->salary:null, ['class'=>'form-control','placeholder'=>'USD 500 ~ 700', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Location<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('location',isset($info)?$info->location:null, ['class'=>'form-control','placeholder'=>'Yangon', 'required']) !!}
                                   
                                </div>  
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Apply<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::textarea('apply',isset($info)?$info->apply:null, ['class' => 'form-control','id'=>'editor_2']) !!}
                                    
                                </div>
                            </div>


                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!URL::previous()!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>

    <link rel="stylesheet"  href="{!! asset('/css/bootstrap-fileinput.css')!!}"/>
    <script src="{!! asset('/js/bootstrap-fileinput.js')!!}"></script>
    <script>
        $(function(){

            CKEDITOR.replace('editor');
            CKEDITOR.replace('editor_1');
            CKEDITOR.replace('editor_2');

            $('.select2').select2({
               
            });

        });
    </script>
    <script src="{!!asset('/bower_components/admin-lte/plugins/ckeditor/ckeditor.js')!!}"></script>
@stop
