@extends('backend.layout.template')
@section('content')
<section class="content-header">
    <h1>
        Job Category <i class="fa fa-arrow-circle-right route-link"></i> Basic <i class="fa fa-arrow-circle-right route-link"></i> {!!isset($info)?$info->name:'New Job Category'!!}
    </h1>
    
</section>
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                    <div class="box-header">
                        <i class="fa fa fa-th"></i>
                        {!!isset($info)?"Edit Job Category - ".$info->name:"New Job Category"!!}
                        <div class="pull-right box-tools">
                            <a href="{!!url('admin/job_category/')!!}" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Job Category List</a>
                        </div>
                    </div>
                      
                    <hr>
                    {!! Form::open(array('url' =>'admin/job_category/basic/'.$id,'class'=>'form-horizontal frmBasic','files'=>true,'method'=>'post','id'=>'frmBasic')) !!}
                    {!! Form::hidden('id',isset($info)?$info->id:0)!!}
                
                    
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name<span class="mandatory">*</span></label>
                                <div class="col-sm-9">

                                    {!! Form::text('name',isset($info)?$info->name:null, ['class'=>'form-control','placeholder'=>'Category', 'required']) !!}
                                   
                                </div>  
                            </div>
                        </div>
                       
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <a href="{!!url(ADMIN_PREFIX.'/job_category')!!}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@stop
