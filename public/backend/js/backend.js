

function loading_show(){

	$("body").remove(".loading-bar");
	$html='<div class="loading-bar"><div class="pdt-ajax-loader">';
	$html+='<div class="text">';
	$html+='<i class="fa fa-spin fa-refresh"></i>';
	$html+='<span>loading...</span>';       
	$html+='</div></div></div>';

	$("body").append($html);
	$('.loading-bar').animate({'top':'-5px'},100);
	 $("#p_body").addClass('wait');
	$("button[type='submit']").attr('disabled','disabled');
}

function loading_hide(){

    $('.loading-bar').delay(1000).animate({'top':'-200px'},1000);
    $("body").remove(".loading-bar");
    $("#p_body").removeClass('wait');
}

function messagebar_toggle($msg,$type,$time){

    $("body .message-bar").remove();
    var $html='<div class="message-bar fixed-top">';
        $html+='<div class="alert message-alert '+$type+'">';
        $html+='<div class="status-label '+$type+'">';
        $html+='<i class="fa fa-check"></i>';
        $html+='</div>';
        $html+='<span>'+$msg+'</span>';
        $html+='<button type="button" class="close" id="message_bar_close">&times;</button>';
        $html+='</div></div>';

    $("body").append($html);
        $('.message-bar').animate({'top':'0px'},1000);
        $('.message-bar').delay($time).animate({'top':'-200px'},1000);

}

/* Message bar animate fade out */
$(document).on("click","#message_bar_close",function(){

    $("body .message-bar").remove();

});


$(document).on('submit',"form.frmBasic",function(){
	
    loading_show();
});