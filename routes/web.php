<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get("/",'HomeController@getHome');
Route::get('home',['as'=>'home','uses'=>'HomeController@getHome']);

Route::get('landing',['as'=>'home','uses'=>'HomeController@getLanding']);

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::get('fbarticleshare/{id}/{name?}',['as'=>'fbarticleshare/{id}/{name?}','uses'=>'HomeController@getFbArticleShare']);

Route::get('fbnewshare/{id}/{name?}',['as'=>'fbnewshare/{id}/{name?}','uses'=>'HomeController@getFbNewShare']);



Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/index','HomeController@getIndex');

Route::get('/article','HomeController@getArticle');

Route::get('/news','HomeController@getNew');

Route::get('/article-detail/{id}/{one?}/{two?}/{three?}','HomeController@getArticleDetail');

Route::get('/new-detail/{id}/{one?}/{two?}/{three?}','HomeController@getNewDetail');

Route::get('/article-category/{id}/{url?}/{url1?}/{url2?}','HomeController@getArticleCategory');

Route::get('/new-category/{id}/{url?}/{url1?}/{url2?}','HomeController@getNewCategory');

Route::get('/event-detail/{id}/{one?}/{two?}/{three?}','HomeController@getEventDetail');

Route::get('/school','HomeController@getSchool');

Route::get('/school-detail/{id}/{url1?}/{url2?}/{url3?}','HomeController@getSchoolDetail');

Route::get('/school/filter','HomeController@getSchool');

Route::post('/school/filter','HomeController@postSchool');

Route::get('/shop','HomeController@getShop');

Route::get('/faq','HomeController@getFaq');

Route::get('/contact','HomeController@getContact');
Route::post('/contact','HomeController@postContact');

Route::get('/financetools','HomeController@getFinancetools');

Route::get('/service','HomeController@getService');
Route::get('/service-detail/{id}/{url?}/{url1?}/{url2?}','HomeController@getServiceDetail');

Route::get('/event','HomeController@getEvent');

Route::get('/event-category/{id}/{url?}/{url2?}/{url3?}','HomeController@getEventCategory');

Route::get('/seminar','HomeController@getSeminar');

Route::get('/schools','HomeController@getSchools');

Route::get('/job','HomeController@getJob');
Route::get('/job-detail/{id}/{url?}/{url2?}/{url3?}','HomeController@getJobDetail');

Route::get('/aboutus','HomeController@getAboutus');

Route::get('/useful-links/{category?}','HomeController@getUseFulLink');

Route::get('/download','HomeController@getDownload');

Route::get('/resources/premium','HomeController@getPremium');

Route::get('/download-detail/{id}','HomeController@getDownloadDetail');
Route::get('/download-read/{id}/{url?}/{url1?}/{url2?}','HomeController@getDownloadRead');
Route::get('/download-category/{category?}','HomeController@getDownloadCategory');

Route::get('/tax-procedure','HomeController@getTaxProcedure');

Route::get('/tax-calendar','HomeController@getTaxCalendar');

Route::get('/tax-calculatar','HomeController@getTaxCalculatar');


Route::get('/market-data/{para?}','HomeController@getMarketData');

Route::get('/job/{para?}','HomeController@getJob');

Route::get('/team','HomeController@getTeam');

Route::get('/user-login','HomeController@getUserLogin');
Route::post('/user-login','HomeController@postUserLogin');

Route::get('/user-register','HomeController@getUserRegister');
Route::post('/user-register','HomeController@postUserRegister');


Route::get('/dashboard','HomeController@getUserDashboard');

Route::get('autocomplete', 'HomeController@autocomplete');
Route::get('school/autocomplete', 'HomeController@autocomplete');






/* ADMIN */
Route::group(['prefix'=>ADMIN_PREFIX,'middleware' => ['auth']], function () {

	Route::get('/admin','AdminController@index');
	Route::get('/','AdminController@index');

	Route::get('/user','AdminController@getUser');

	Route::get('/user/basic/{id}','AdminController@getUserBasic');
	Route::post('/user/basic/{id}','AdminController@postUserBasic');

	Route::get('/image','ImageController@getIndex');

	Route::get('/image/basic/{id}','ImageController@getBasic');
	Route::post('/image/basic/{id}','ImageController@postBasic');
	Route::get('/image/destroy/{id}', 'ImageController@destroy');

	/* CATEGORY */

    Route::get('/category',['as'=>ADMIN_PREFIX.'/category','uses'=>'CategoryController@index']);
    Route::get('category/basic/{id}',['as'=>ADMIN_PREFIX.'/category/basic/{id}','uses'=>'CategoryController@basic']);
    Route::post('category/basic/{id}','CategoryController@storeBasic');
    Route::get('category/destroy/{id}', 'CategoryController@destroy');

    Route::get('category/search',['as'=>ADMIN_PREFIX.'/category/search','uses'=>'CategoryController@index']);
    Route::post('category/search',['as'=>ADMIN_PREFIX.'/category/search','uses'=>'CategoryController@search']);


    /* DOWNLOAD CATEGORY */

    Route::get('/download_category',['as'=>ADMIN_PREFIX.'/download_category','uses'=>'DownloadCategoryController@index']);
    Route::get('download_category/basic/{id}',['as'=>ADMIN_PREFIX.'/download_category/basic/{id}','uses'=>'DownloadCategoryController@basic']);
    Route::post('download_category/basic/{id}','DownloadCategoryController@storeBasic');
    Route::get('download_category/destroy/{id}', 'DownloadCategoryController@destroy');

    

    /* STOCK */

    Route::get('/stock',['as'=>ADMIN_PREFIX.'/stock','uses'=>'StockController@index']);
    Route::get('stock/basic/{id}',['as'=>ADMIN_PREFIX.'/category/basic/{id}','uses'=>'StockController@basic']);
    Route::post('stock/basic/{id}','StockController@storeBasic');
    Route::get('stock/destroy/{id}', 'StockController@destroy');

   

    /* NEW CATEGORY */

    Route::get('/new-category',['as'=>ADMIN_PREFIX.'/new-category','uses'=>'NewsCategoryController@index']);
    Route::get('new-category/basic/{id}',['as'=>ADMIN_PREFIX.'/new-category/basic/{id}','uses'=>'NewsCategoryController@basic']);
    Route::post('new-category/basic/{id}','NewsCategoryController@storeBasic');
    Route::get('new-category/destroy/{id}', 'NewsCategoryController@destroy');

    Route::get('new-category/search',['as'=>ADMIN_PREFIX.'/new-category/search','uses'=>'NewsCategoryController@index']);
    Route::post('new-category/search',['as'=>ADMIN_PREFIX.'/new-category/search','uses'=>'NewsCategoryController@search']);



	/* EVENT CATEGORY */

    Route::get('/event_category',['as'=>ADMIN_PREFIX.'/event_category','uses'=>'EventCategoryController@index']);

    Route::get('event_category/basic/{id}',['as'=>ADMIN_PREFIX.'/event_category/basic/{id}','uses'=>'EventCategoryController@basic']);
    Route::post('event_category/basic/{id}','EventCategoryController@storeBasic');
    Route::get('event_category/destroy/{id}', 'EventCategoryController@destroy');

    Route::get('event_category/search',['as'=>ADMIN_PREFIX.'/event_category/search','uses'=>'EventCategoryController@index']);

    Route::post('event_category/search',['as'=>ADMIN_PREFIX.'/event_category/search','uses'=>'EventCategoryController@search']);

    /* ============ Article ============== */

    Route::get('/article',['as'=>ADMIN_PREFIX.'/article','uses'=>'ArticleController@index']);

    Route::get('/article/progressive',['as'=>ADMIN_PREFIX.'/article/progressive','uses'=>'ArticleController@getProgressive']);


    Route::get('article/basic/{id}',['as'=>ADMIN_PREFIX.'/article/basic/{id}','uses'=>'ArticleController@basic']);


    Route::post('article/basic/{id}','ArticleController@storeBasic');
    Route::get('article/destroy/{id}', 'ArticleController@destroy');

    Route::get('article/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'ArticleController@index']);
    Route::post('article/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'ArticleController@search']);

    Route::get('article/togglepublish/{id}/{status}', 'ArticleController@togglePublish');
    Route::get('article/toggleFeatured/{id}/{status}', 'ArticleController@toggleFeatured');

    Route::get('/featured-articles',['as'=>ADMIN_PREFIX.'/featured-articles','uses'=>'ArticleController@featuredArticles']);

    Route::post('article/sortFeatured', 'ArticleController@sortFeatured');



    /* ============ EVENTS ============== */

    Route::get('/event',['as'=>ADMIN_PREFIX.'/event','uses'=>'EventController@index']);

    Route::get('/event/progressive',['as'=>ADMIN_PREFIX.'/article/progressive','uses'=>'EventController@getProgressive']);


    Route::get('event/basic/{id}',['as'=>ADMIN_PREFIX.'/article/basic/{id}','uses'=>'EventController@basic']);


    Route::post('event/basic/{id}','EventController@storeBasic');
    Route::get('event/destroy/{id}', 'EventController@destroy');

    Route::get('event/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'EventController@index']);
    Route::post('event/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'EventController@search']);

    Route::get('event/togglepublish/{id}/{status}', 'EventController@togglePublish');
    Route::get('event/toggleFeatured/{id}/{status}', 'EventController@toggleFeatured');

    Route::get('/featured-articles',['as'=>ADMIN_PREFIX.'/featured-articles','uses'=>'EventController@featuredArticles']);

    Route::post('event/sortFeatured', 'EventControllere@sortFeatured');




    /* ============ SERVICES ============== */

    Route::get('/service',['as'=>ADMIN_PREFIX.'/event','uses'=>'ServiceController@index']);

    Route::get('/service/progressive',['as'=>ADMIN_PREFIX.'/article/progressive','uses'=>'ServiceController@getProgressive']);


    Route::get('service/basic/{id}',['as'=>ADMIN_PREFIX.'/article/basic/{id}','uses'=>'ServiceController@basic']);


    Route::post('service/basic/{id}','ServiceController@storeBasic');
    Route::get('service/destroy/{id}', 'ServiceController@destroy');

    Route::get('service/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'ServiceController@index']);
    Route::post('service/search',['as'=>ADMIN_PREFIX.'/article/search','uses'=>'ServiceController@search']);

    Route::get('service/togglepublish/{id}/{status}', 'ServiceController@togglePublish');
    Route::get('service/toggleFeatured/{id}/{status}', 'ServiceController@toggleFeatured');

    Route::get('/feature-service',['as'=>ADMIN_PREFIX.'/featured-articles','uses'=>'ServiceController@featuredServices']);

    Route::post('service/sortFeatured', 'ServiceController@sortFeatured');


    /* ============ Links ============== */

    Route::get('/link',['as'=>ADMIN_PREFIX.'/link','uses'=>'LinkController@index']);
    
    Route::get('link/basic/{id}',['as'=>ADMIN_PREFIX.'/link/basic/{id}','uses'=>'LinkController@basic']);


    Route::post('link/basic/{id}','LinkController@storeBasic');
    Route::get('link/destroy/{id}', 'LinkController@destroy');

    Route::get('service/search',['as'=>ADMIN_PREFIX.'/link/search','uses'=>'LinkController@index']);
    Route::post('service/search',['as'=>ADMIN_PREFIX.'/link/search','uses'=>'LinkController@search']);

    Route::get('link/togglepublish/{id}/{status}', 'LinkController@togglePublish');
    Route::get('link/toggleFeatured/{id}/{status}', 'LinkController@toggleFeatured');


    /* ============ Download ============== */

    Route::get('/download',['as'=>ADMIN_PREFIX.'/download','uses'=>'DownloadController@index']);
    
    Route::get('download/basic/{id}',['as'=>ADMIN_PREFIX.'/download/basic/{id}','uses'=>'DownloadController@basic']);


    Route::post('download/basic/{id}','DownloadController@storeBasic');
    Route::get('download/destroy/{id}', 'DownloadController@destroy');

    Route::get('download/search',['as'=>ADMIN_PREFIX.'/download/search','uses'=>'DownloadController@index']);
    Route::post('download/search',['as'=>ADMIN_PREFIX.'/download/search','uses'=>'DownloadController@search']);

    Route::get('download/togglepublish/{id}/{status}', 'DownloadController@togglePublish');
    Route::get('download/toggleFeatured/{id}/{status}', 'DownloadController@toggleFeatured');



    /* ============ SCHOOLS ============== */

    Route::get('/school',['as'=>ADMIN_PREFIX.'/school','uses'=>'SchoolController@index']);

    
    Route::get('school/basic/{id}',['as'=>ADMIN_PREFIX.'/school/basic/{id}','uses'=>'SchoolController@basic']);


    Route::post('school/basic/{id}','SchoolController@storeBasic');
    Route::get('school/destroy/{id}', 'SchoolController@destroy');

    Route::get('school/search',['as'=>ADMIN_PREFIX.'/school/search','uses'=>'SchoolController@index']);
    Route::post('school/search',['as'=>ADMIN_PREFIX.'/school/search','uses'=>'SchoolController@search']);

    Route::get('school/togglepublish/{id}/{status}', 'SchoolController@togglePublish');
    Route::get('school/toggleFeatured/{id}/{status}', 'SchoolController@toggleFeatured');

    Route::get('/featured-school',['as'=>ADMIN_PREFIX.'/featured-school','uses'=>'SchoolController@featuredArticles']);
    Route::post('school/sortFeatured', 'SchoolController@sortFeatured');




    /* ============ Tax Procedure ============== */

    Route::get('/procedure',['as'=>ADMIN_PREFIX.'/link','uses'=>'TaxProcedureController@index']);
    
    Route::get('procedure/basic/{id}',['as'=>ADMIN_PREFIX.'/procedure/basic/{id}','uses'=>'TaxProcedureController@basic']);

    Route::post('procedure/basic/{id}','TaxProcedureController@storeBasic');
    Route::get('procedure/destroy/{id}', 'TaxProcedureController@destroy');


    Route::get('procedure/togglepublish/{id}/{status}', 'TaxProcedureController@togglePublish');
    Route::get('procedure/toggleFeatured/{id}/{status}', 'TaxProcedureController@toggleFeatured');

    /* ============ Stock Market ============== */

    Route::get('/stock_market',['as'=>ADMIN_PREFIX.'/stock_market','uses'=>'StockMarketController@index']);
    
    Route::get('stock_market/basic/{id}',['as'=>ADMIN_PREFIX.'/stock_market/basic/{id}','uses'=>'StockMarketController@basic']);

    Route::post('stock_market/basic/{id}','StockMarketController@storeBasic');

    Route::get('stock_photo/',['as'=>ADMIN_PREFIX.'/stock_photo/basic/{id}','uses'=>'StockMarketController@getStockPhotoList']);

    Route::get('stock_photo/basic/{id}',['as'=>ADMIN_PREFIX.'/stock_photo/basic/{id}','uses'=>'StockMarketController@getStockPhoto']);

    Route::post('stock_photo/basic/{id}','StockMarketController@postStockPhoto');



    Route::get('stock_market/photo/{id}',['as'=>ADMIN_PREFIX.'/stock_market/basic/{id}','uses'=>'StockMarketController@getPhoto']);

    Route::post('stock_market/photo/{id}','StockMarketController@postPhoto');


    Route::get('stock_market/destroy/{id}', 'StockMarketController@destroy');


    Route::get('stock_market/togglepublish/{id}/{status}', 'StockMarketController@togglePublish');

     Route::get('stock_photo/togglepublish/{id}/{status}', 'StockMarketController@togglePhotoPublish');

    Route::get('stock_market/toggleFeatured/{id}/{status}', 'StockMarketController@toggleFeatured');


    /* ============ News ============== */

    Route::get('/news',['as'=>ADMIN_PREFIX.'/news','uses'=>'NewsController@index']);

    Route::get('/news/progressive',['as'=>ADMIN_PREFIX.'/news/progressive','uses'=>'NewsController@getProgressive']);

    Route::get('news/basic/{id}',['as'=>ADMIN_PREFIX.'/news/basic/{id}','uses'=>'NewsController@basic']);


    Route::post('news/basic/{id}','NewsController@storeBasic');
    Route::get('news/destroy/{id}', 'NewsController@destroy');

    Route::get('news/search',['as'=>ADMIN_PREFIX.'/news/search','uses'=>'NewsController@index']);
    Route::post('news/search',['as'=>ADMIN_PREFIX.'/news/search','uses'=>'NewsController@search']);

    Route::get('news/togglepublish/{id}/{status}', 'NewsController@togglePublish');
    Route::get('news/toggleFeatured/{id}/{status}', 'NewsController@toggleFeatured');

    Route::get('/featured-news',['as'=>ADMIN_PREFIX.'/featured-news','uses'=>'NewsController@featuredArticles']);

    Route::post('news/sortFeatured', 'NewsController@sortFeatured');


     /* ============ Setting ============== */

    Route::get('/contact-message',['as'=>ADMIN_PREFIX.'/contact','uses'=>'SettingController@getContactMessage']);

    Route::get('/news/progressive',['as'=>ADMIN_PREFIX.'/news/progressive','uses'=>'NewsController@getProgressive']);


    Route::get('news/basic/{id}',['as'=>ADMIN_PREFIX.'/news/basic/{id}','uses'=>'NewsController@basic']);


    Route::post('news/basic/{id}','NewsController@storeBasic');
    Route::get('news/destroy/{id}', 'NewsController@destroy');


    /* ============ Team ============== */

    Route::get('/team',['as'=>ADMIN_PREFIX.'/team','uses'=>'TeamController@index']);

    Route::get('team/basic/{id}',['as'=>ADMIN_PREFIX.'/news/basic/{id}','uses'=>'TeamController@basic']);
    Route::post('team/basic/{id}','TeamController@storeBasic');
    Route::get('team/destroy/{id}', 'TeamController@destroy');

    Route::get('team/search',['as'=>ADMIN_PREFIX.'/news/search','uses'=>'TeamController@index']);
    Route::post('team/search',['as'=>ADMIN_PREFIX.'/news/search','uses'=>'TeamController@search']);

    Route::get('team/togglepublish/{id}/{status}', 'TeamController@togglePublish');
    Route::get('team/toggleFeatured/{id}/{status}', 'TeamController@toggleFeatured');


    Route::post('team/sortFeatured', 'TeamController@sortFeatured');


    /* ============ Job ============== */

    Route::get('/job',['as'=>ADMIN_PREFIX.'/job','uses'=>'JobController@index']);

    
    Route::get('job/basic/{id}',['as'=>ADMIN_PREFIX.'/job/basic/{id}','uses'=>'JobController@basic']);


    Route::post('job/basic/{id}','JobController@storeBasic');
    Route::get('job/destroy/{id}', 'JobController@destroy');

    Route::get('job/search',['as'=>ADMIN_PREFIX.'/job/search','uses'=>'JobController@index']);
    Route::post('job/search',['as'=>ADMIN_PREFIX.'/job/search','uses'=>'JobController@search']);

    Route::get('job/togglepublish/{id}/{status}', 'JobController@togglePublish');
    Route::get('job/toggleFeatured/{id}/{status}', 'JobController@toggleFeatured');

    Route::get('/featured-job',['as'=>ADMIN_PREFIX.'/featured-job','uses'=>'JobController@featuredJobs']);

    Route::post('job/sortFeatured', 'JobController@sortFeatured');

    /* ============ Download Sub Category ============== */

    Route::get('/download_subcategory/{id?}',['as'=>ADMIN_PREFIX.'/resources/free','uses'=>'ResourcesController@getFree']);

     Route::post('/download_subcategory/{id?}',['as'=>ADMIN_PREFIX.'/resources/free','uses'=>'ResourcesController@postFree']);


     /* CLIENT */

    Route::get('/client',['as'=>ADMIN_PREFIX.'/client','uses'=>'ClientController@index']);
    Route::get('client/basic/{id}',['as'=>ADMIN_PREFIX.'/category/basic/{id}','uses'=>'ClientController@basic']);
    Route::post('client/basic/{id}','ClientController@storeBasic');
    Route::get('client/destroy/{id}', 'ClientController@destroy');

    Route::get('client/search',['as'=>ADMIN_PREFIX.'/client/search','uses'=>'ClientController@index']);
    Route::post('client/search',['as'=>ADMIN_PREFIX.'/client/search','uses'=>'ClientController@search']);


    /* JOB CATEGORY */

    Route::get('/job_category',['as'=>ADMIN_PREFIX.'/job_category','uses'=>'JobCategoryController@index']);
    Route::get('job_category/basic/{id}',['as'=>ADMIN_PREFIX.'/job_category/basic/{id}','uses'=>'JobCategoryController@basic']);
    Route::post('job_category/basic/{id}','JobCategoryController@storeBasic');
    Route::get('job_category/destroy/{id}', 'JobCategoryController@destroy');

    Route::get('job_category/search',['as'=>ADMIN_PREFIX.'/job_category/search','uses'=>'JobCategoryController@index']);
    Route::post('job_category/search',['as'=>ADMIN_PREFIX.'/job_category/search','uses'=>'JobCategoryController@search']);


});



