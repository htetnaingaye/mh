<?php


define("ADMIN_PREFIX","admin");

define("DEST_PATH",'uploads/destination/');
define("TOUR_PATH",'uploads/tour/');
define("HOTEL_PATH",'uploads/hotel/');
define("PHOTO_PATH",'uploads/photo/');

define("GUIDE_PATH",'uploads/guide/');

define("LOGIN_REDIRECT",ADMIN_PREFIX);

define("SITE_ADMIN",10);
define("MEMBER",20);

define("MEME",1);
define("GUM9",2);
define("MMTROLLER",3);

define('LINKS',json_encode(array(

	101=>'Link One',
	102=>'Link Two',
	103=>'Link Three'

)));

define('TEAM_TYPE',json_encode(array(

	'bod'=>'BOD Team',
	'operation'=>'Operation Team',
	

)));

define('DOWNLOAD_TYPE',json_encode(array(
	'100'=>'Free',
	'101'=>'Premium',

)));

define('ORDINARY_LINK',json_encode(array(
	'law'=>'Law',
	'standard'=>'Standard',
	'ebooks'=>'Ebooks',
	'template'=>'Template'



)));

define('JOB_CATEGORY',json_encode(array(
	'vacancy'=>'Vacancy',
	'cv-submit'=>'CV Submit',

)));


define('WEBSITE_LINK',json_encode(array(

	'ministry'=>'Ministry',
	'international-organization'=>'International Organization',
	'professional-body'=>'Professional Body',

)));


define('PREMIUM_LINK',json_encode(array(
	
	

)));


define('DOWNLOAD_LINK',json_encode(array(
	'law'=>'Law',
	'standard'=>'Standard',
	'ebooks'=>'Ebooks'

)));

define('MARKET_DATA',json_encode(array(

	'ex-rate'=>'Ex Rate',
	'stock-market'=>'Stock Market',
	'inflation'=>'Inflation'

)));


define("SOURCE",json_encode(array(
	''=>'None',
	''=>'Credit',
	MEME=>'Myanmar MeMe Group',
	GUM9=>'GUM-9',
	MMTROLLER=>'MM Troller'
	)));

define("NORMAL_WIDTH",'750');
define("NORMAL_HIGHT",'308');


define("SERVICE_NORMAL_WIDTH",'750');
define("SERVICE_NORMAL_HIGHT",'308');

define("THUMB_WIDTH",'177');
define("THUMB_HIGHT",'100');

define("DEFAULT_PAGE_COUNT",10);

define("DEST",10);
define("HOTEL",20);
define("TOUR",30);


define("NORMAL_PREFIX",'normal__');
define("THUMB_PREFIX",'thumb__');

define("DEFAULT_THUMBNAIL",'');

define("INACTIVE",0);
define("ACTIVE",1);

define("SITE_DESC","သင္႕ အတြက္ ေပ်ာ္ရႊင္စရာ အခိုုက္အတန္႕ ေလးေတြကိုု ယူေဆာင္လာေပးမယ္႕  meme တိုု႕ စုုစည္းရာေနရာ Todaymeme.com");

define('ARTICLE_PATH','uploads/articles');
define('EVENT_PATH','uploads/events');
define('SERVICE_PATH','uploads/services');
define('DOWNLOAD_PATH','uploads/downloads');

define("FACEBOOK_LINK",'https://www.facebook.com/mysec2015/');
define("GOOGLE_LINK",'#');
define("TWITTER_LINK",'#');
define("LINKIN_LINK",'#');
define("SKYPE_NAME",'#');



